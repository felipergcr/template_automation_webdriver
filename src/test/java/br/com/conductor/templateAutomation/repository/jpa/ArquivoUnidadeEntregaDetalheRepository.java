package br.com.conductor.templateAutomation.repository.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.templateAutomation.domain.custom.ArquivoUnidadeEntregaDetalhe;

@Repository
public interface ArquivoUnidadeEntregaDetalheRepository extends JpaRepository<ArquivoUnidadeEntregaDetalhe, Long> {

	Optional<List<ArquivoUnidadeEntregaDetalhe>> findByIdArquivoUE(Long idArquivoUE);
}
