
package br.com.conductor.templateAutomation.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.templateAutomation.domain.custom.ArquivoUnidadeEntrega;

@Repository
public interface ArquivoUnidadeEntregaRepository extends JpaRepository<ArquivoUnidadeEntrega, Long> {

}
