
package br.com.conductor.templateAutomation.repository.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.templateAutomation.domain.custom.UnidadeEntrega;

public interface UnidadeEntregaRepository extends JpaRepository<UnidadeEntrega, Long> {

	Optional<List<UnidadeEntrega>> findByIdGrupoEmpresaAndStatus(Long idGrupoEmpresa, Integer status);

}
