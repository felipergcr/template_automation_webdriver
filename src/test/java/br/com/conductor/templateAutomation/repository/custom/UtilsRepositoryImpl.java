package br.com.conductor.templateAutomation.repository.custom;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.conductor.templateAutomation.integrado.report.LogReport;
import br.com.conductor.templateAutomation.utils.DataUtils;

@Repository
public class UtilsRepositoryImpl implements UtilsRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	@Transactional
	public void inserirEventoExternosPagamentos(BigDecimal valor) {

		StringBuilder sql = new StringBuilder();

		sql.append("INSERT INTO eventosexternospagamentos ")
				.append("(Banco, DataMovimento, DataPagamento, NossoNumero, ArquivoCobrancaBancaria, ")
				.append("DataVencimento, ValorPago, NumeroDocumento, Cartao, Id_MovimentoPagamentoPendente, Lote, ")
				.append("DefasagemCobranca, MeioLiquidacao, Id_Estabelecimento, Id_Autorizacao, Id_Conta,  ")
				.append("Id_Operacao, CodigoAutenticacao, DataEntradaPagamento, DataVencimentoPadrao, ")
				.append("DataVencimentoReal, Origem, OrigemResolucao, RestituicaoSaque, Status, DataProcessamentoLojista, ")
				.append("Id_Borderaux, DataProcessamentoLojista2, CodigoAutorizacao, StatusLojista, MCC, CodigoMoeda, ")
				.append("Nome_Estabelecimento_VISA, Id_Estabelecimento_VISA, Id_Incoming, ValorDolar, Id_Cartao, ")
				.append("Id_EventoExternoRedeCompartilhada, Id_EmissorRedeCompartilhada, Id_Estabelecimento_Externo, CodAutRedeCompartilhada) ")
				.append("values ").append("(33, GETDATE(), GETDATE(), '8000000001', NULL, NULL,").append("" + valor)
				.append(" ,'8000000001', '', NULL, NULL, 0, NULL, NULL, NULL, 4, 99, NULL, GETDATE(), '01/01/2079', '2079-01-01 00:00:00', ")
				.append("NULL, NULL, 0.00, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ")
				.append("NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)");
		
		LogReport.info("Query utilizada: <br/>" + sql.toString());

		this.entityManager.createNativeQuery(sql.toString()).executeUpdate();
	}
	
	@Override
    public Long getUltimoEventosExternosPagamentos() {
   	 
   	 StringBuilder sql = new StringBuilder();
   	 
   	 sql.append("SELECT TOP 1 id_eventopagamento FROM eventosexternospagamentos ORDER BY 1 DESC");
   	 
   	 LogReport.info("Query utilizada: <br/>" + sql.toString());
   	 
   	 Integer id_evento  = (Integer) this.entityManager.createNativeQuery(sql.toString()).getSingleResult();
   	 
   	 return Long.valueOf(id_evento.toString());
   	 
    }
	
	@Override
	@Transactional
	public void pagamentoBoletoEmitidos(Long id_boleto, Long id_eventopagamento) {

		StringBuilder sql = new StringBuilder();

		sql.append("UPDATE boletosemitidos ")
		.append("SET Id_EventoPagamento = " + id_eventopagamento)
		.append(" WHERE Id_Boleto = " + id_boleto);

		LogReport.info("Query utilizada: <br/>" + sql.toString());

		this.entityManager.createNativeQuery(sql.toString()).executeUpdate();
	}
	
	@Override
	@Transactional
	public void inserirExcecoesLimiteDisponivel(Long idGrupoEmpresa, Long idUsuario) {
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("INSERT INTO ExcecoesLimiteDisponivel ")
		.append("(Id_GrupoEmpresa, Descricao, Status, DataAlteracao, Id_UsuarioRegistro) ")
		.append("values ")
		.append("(" + idGrupoEmpresa)
		.append(", 'Automacao', 1, GETDATE(), " + idUsuario + ")");
		
		LogReport.info("Query utilizada: <br/>" + sql.toString());
		
		this.entityManager.createNativeQuery(sql.toString()).executeUpdate();
	}
	
	@Override
	@Transactional
	public void inserirDataVigenciaVencida(Long idGrupoEmpresa, Long dias) {
		
		StringBuilder sql = new StringBuilder();
		
		String data = DataUtils.localDateParaStringyyyyMMdd(LocalDate.now().minusDays(dias));
		
		sql.append("UPDATE ParametrosGruposEmpresas ")
		.append("SET valor = '")
		.append(data)
		.append("' ")
		.append(" WHERE Id_GrupoEmpresa = " + idGrupoEmpresa)
		.append(" AND id_parametro = (select id_parametro from parametros where codigo = 'RH_VigenciaLimite')");
		
		LogReport.info("Query utilizada: <br/>" + sql.toString());
		
		this.entityManager.createNativeQuery(sql.toString()).executeUpdate();
	}
}
