package br.com.conductor.templateAutomation.repository.jpa;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.templateAutomation.domain.custom.CargaControleFinanceiro;

@Repository
public interface CargaControleFinanceiroRepository extends JpaRepository<CargaControleFinanceiro, Long> {

	Optional<CargaControleFinanceiro> findById(Long id);
}
