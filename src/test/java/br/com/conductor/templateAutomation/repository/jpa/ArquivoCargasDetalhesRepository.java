
package br.com.conductor.templateAutomation.repository.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.templateAutomation.domain.custom.ArquivosCargasDetalhes;

@Repository
public interface ArquivoCargasDetalhesRepository extends JpaRepository<ArquivosCargasDetalhes, Long> {

	Optional<List<ArquivosCargasDetalhes>> findByIdArquivoCargas(Long idArquivoCargas);

}
