
package br.com.conductor.templateAutomation.repository.jpa;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.templateAutomation.domain.custom.GrupoEmpresa;

@Repository
public interface GrupoEmpresaRepository extends JpaRepository<GrupoEmpresa, Long> {

	Optional<GrupoEmpresa> findById(Long idGrupoEmpresa);

}
