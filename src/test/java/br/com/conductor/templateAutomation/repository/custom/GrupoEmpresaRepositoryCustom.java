package br.com.conductor.templateAutomation.repository.custom;

import java.util.List;

import br.com.conductor.templateAutomation.domain.custom.HierarquiaOrganizacionalGrupo;

public interface GrupoEmpresaRepositoryCustom {

	List<HierarquiaOrganizacionalGrupo> consultaHierarquiaOrganizacional(Long id);

	String consultarCNPJ(Integer idPessoa);
}
