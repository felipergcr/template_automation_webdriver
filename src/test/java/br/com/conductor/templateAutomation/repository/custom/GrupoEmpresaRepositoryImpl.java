package br.com.conductor.templateAutomation.repository.custom;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.conductor.templateAutomation.domain.custom.HierarquiaOrganizacionalGrupo;

@Repository
public class GrupoEmpresaRepositoryImpl implements GrupoEmpresaRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<HierarquiaOrganizacionalGrupo> consultaHierarquiaOrganizacional(Long id) {

		Query query;

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT ").append("ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS ID, ")
				.append("g.Id_GrupoEmpresa AS ID_GRUPOEMPRESA, ").append("g.Nome AS NOME_GRUPOEMPRESA, ")
				.append("sg.Id_GrupoEmpresa AS ID_SUBGRUPOEMPRESA, ").append("sg.Nome AS NOME_SUBGRUPOEMPRESA, ")
				.append("e.Id_Empresa AS ID_EMPRESA, ").append("e.NomeExibicao AS NOME_EMPRESA, ")
				.append("p.CPF AS CNPJ  ").append("FROM Empresas E ")
				.append("INNER JOIN Pessoas P ON E.Id_Pessoa = P.Id_PessoaFisica ")
				.append("INNER JOIN GruposEmpresas SG ON SG.Id_GrupoEmpresa = E.Id_GrupoEmpresa and SG.Id_GrupoEmpresaPai IS NOT NULL ")
				.append("INNER JOIN GruposEmpresas G ON G.Id_GrupoEmpresa = SG.Id_GrupoEmpresaPai and G.Id_GrupoEmpresaPai IS NULL ")
				.append("WHERE E.Id_GrupoEmpresa IN (SELECT id_grupoEmpresa FROM gruposEmpresas WHERE Id_GrupoEmpresa= :id OR Id_GrupoEmpresaPai= :id) ");

		query = entityManager.createNativeQuery(sql.toString(), HierarquiaOrganizacionalGrupo.class);

		query.setParameter("id", id);

		List<HierarquiaOrganizacionalGrupo> hierarquiaOrganizacionalGrupo = query.getResultList();

		return hierarquiaOrganizacionalGrupo;
	}

	@Override
	public String consultarCNPJ(Integer idPessoa) {

		Query query;

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT PF.CPF ").append("FROM empresas E ")
				.append("INNER JOIN PessoasFisicas PF ON (PF.Id_PessoaFisica = E.Id_Pessoa) ")
				.append("WHERE PF.Id_PessoaFisica = :idPessoa");

		query = entityManager.createNativeQuery(sql.toString());

		query.setParameter("idPessoa", idPessoa);

		return query.getSingleResult().toString();
	}

}
