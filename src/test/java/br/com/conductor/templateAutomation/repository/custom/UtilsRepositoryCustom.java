package br.com.conductor.templateAutomation.repository.custom;

import java.math.BigDecimal;

public interface UtilsRepositoryCustom {
	
	void inserirEventoExternosPagamentos(BigDecimal valor);
	
	Long getUltimoEventosExternosPagamentos();
	
	void pagamentoBoletoEmitidos(Long id_boleto, Long id_eventopagamento);
	
	void inserirExcecoesLimiteDisponivel(Long idGrupoEmpresa, Long idUsuario);
	
	void inserirDataVigenciaVencida(Long idGrupoEmpresa, Long dias); 

}
