package br.com.conductor.templateAutomation.repository.jpa;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.conductor.templateAutomation.domain.custom.EmpresasCargas;

@Repository
public interface EmpresasCargasRepository extends JpaRepository<EmpresasCargas, Long> {

	Optional<EmpresasCargas> findByIdEmpresa(Long idEmpresa);
	
	Optional<EmpresasCargas> findByIdCargaBeneficio(Long idCargaBeneficio);
}
