
package br.com.conductor.templateAutomation.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conductor.templateAutomation.domain.custom.CargaBeneficio;

public interface CargaBeneficioRepository extends JpaRepository<CargaBeneficio, Long> {

	List<CargaBeneficio> findByArquivoCargasId(Long idArquivoCargas);

}