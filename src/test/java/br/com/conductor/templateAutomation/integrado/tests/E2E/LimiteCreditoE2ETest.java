package br.com.conductor.templateAutomation.integrado.tests.E2E;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.templateAutomation.config.Property;
import br.com.conductor.templateAutomation.config.Webdriver;
import br.com.conductor.templateAutomation.controleAcesso.domain.Usuarios;
import br.com.conductor.templateAutomation.domain.custom.ArquivoCargas;
import br.com.conductor.templateAutomation.domain.custom.CargaBeneficio;
import br.com.conductor.templateAutomation.domain.custom.CargaControleFinanceiro;
import br.com.conductor.templateAutomation.domain.custom.EmpresasCargas;
import br.com.conductor.templateAutomation.domain.custom.GrupoEmpresa;
import br.com.conductor.templateAutomation.domain.response.EmpresaResponse;
import br.com.conductor.templateAutomation.domain.response.GrupoEmpresaResponse;
import br.com.conductor.templateAutomation.domain.response.LimiteCreditoResponse;
import br.com.conductor.templateAutomation.domain.response.PermissoesRhResponse;
import br.com.conductor.templateAutomation.domain.response.SubgrupoEmpresaResponse;
import br.com.conductor.templateAutomation.domain.response.UsuarioResponse;
import br.com.conductor.templateAutomation.enums.ModeloDeCartao;
import br.com.conductor.templateAutomation.enums.ModeloDePedido;
import br.com.conductor.templateAutomation.enums.SegmentoEmpresaEnum;
import br.com.conductor.templateAutomation.enums.StatusArquivoEnum;
import br.com.conductor.templateAutomation.enums.StatusCargaBeneficioEnum;
import br.com.conductor.templateAutomation.enums.StatusPagamentoEnum;
import br.com.conductor.templateAutomation.enums.StatusPagamentoFrontEnum;
import br.com.conductor.templateAutomation.enums.StatusPedidoEnum;
import br.com.conductor.templateAutomation.enums.TipoContratoEnum;
import br.com.conductor.templateAutomation.enums.TipoEntregaCartao;
import br.com.conductor.templateAutomation.enums.TipoFaturamentoEnum;
import br.com.conductor.templateAutomation.enums.TipoPagamento;
import br.com.conductor.templateAutomation.enums.TipoPedido;
import br.com.conductor.templateAutomation.enums.TipoProduto;
import br.com.conductor.templateAutomation.enums.TransferenciaProdutos;
import br.com.conductor.templateAutomation.integrado.actions.GrupoEmpresaActions;
import br.com.conductor.templateAutomation.integrado.actions.PedidosActions;
import br.com.conductor.templateAutomation.integrado.actions.UsuarioActions;
import br.com.conductor.templateAutomation.integrado.pages.HomePage;
import br.com.conductor.templateAutomation.integrado.pages.LoginPage;
import br.com.conductor.templateAutomation.integrado.pages.PedidoPage;
import br.com.conductor.templateAutomation.integrado.tests.BaseTest;
import br.com.conductor.templateAutomation.service.ArquivoCargasService;
import br.com.conductor.templateAutomation.service.CargaBeneficioService;
import br.com.conductor.templateAutomation.service.CargaControleFinanceiroService;
import br.com.conductor.templateAutomation.service.EmpresasCargasService;
import br.com.conductor.templateAutomation.service.GrupoEmpresaService;
import br.com.conductor.templateAutomation.service.UtilsService;
import br.com.conductor.templateAutomation.utils.CadastroGenerico;

public class LimiteCreditoE2ETest extends BaseTest {
	
	@Autowired
	CadastroGenerico cadastroGenerico;
	
	@Autowired
	UsuarioActions usuarioActions;
	
	@Autowired
	GrupoEmpresaActions grupoEmpresaActions;
	
	@Autowired
	PedidosActions pedidosActions;
	
	@Autowired
	ArquivoCargasService arquivoCargasService;
	
	@Autowired
	EmpresasCargasService empresasCargasService;
	
	@Autowired
	CargaBeneficioService cargaBeneficioService;
	
	@Autowired
	CargaControleFinanceiroService cargaControleFinanceiroService;

	@Autowired
	GrupoEmpresaService grupoEmpresaService;
	
	@Autowired
	UtilsService utilsService;
	
	private JSONArray tipoProduto = new JSONArray();
	private LocalDate dataAtual = LocalDate.now();
	private Random random = new Random();
	
	private static Webdriver driver = new Webdriver();
	private static LoginPage loginPage = new LoginPage(Webdriver.getDriver());
	private static PedidoPage pedidoPage = new PedidoPage(Webdriver.getDriver());
	private static HomePage homePage = new HomePage(Webdriver.getDriver());
	
	@Before
	public void before() {
		driver.navegateTo(Property.API_FRONT);
	}
	
	@Test
	public void consultarLimiteVencidoGrupoPosPagoCentralizado() throws Throwable {
		JSONObject parametros = new JSONObject();

		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.CENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.CENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3)); 
		
		GrupoEmpresaResponse grupoEmpresaResponse = grupoEmpresaActions.cadastrarGrupoEmpresa(parametros);
		
		utilsService.inserirDataVigenciaVencida(grupoEmpresaResponse.getId(), 10L);
		
		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();
		
		list_cnpjEmpresas.put(grupoEmpresaService.consultarCNPJ(grupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(7));
		
		JSONArray detalhesPedido = pedidosActions.gerarDetalhesPedidosArquivoLoteOuPortaPorta(list_cnpjEmpresas, 10,
				null);

		XSSFWorkbook workbook = pedidosActions
				.getPlanilhaDefaultPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name());

		pedidosActions.setDetalhesPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name(), detalhesPedido,
				workbook);

		MultipartFile xlsPedido = pedidosActions.gerarNovaPlanilhaPedidoLoteOuPortaPorta(workbook);

		usuarioActions.atualizarSenhausuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
		Long numero_pedido = pedidoPage.realizarUploadPlanilha(xlsPedido.getName());
		
		pedidosActions.validarStatusArquivoCargaSTD(numero_pedido, StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO.getStatus(), StatusPagamentoEnum.LIMITE_CREDITO_VENCIDO.getStatus());
		
		pedidosActions.validarArquivoCargasDetalhes(TipoEntregaCartao.PORTA_A_PORTA.name(),
				numero_pedido, detalhesPedido);

		pedidosActions.validarCargasBeneficios(numero_pedido,
				StatusCargaBeneficioEnum.PENDENCIA_LIMITE.getStatus());
		
		Optional<ArquivoCargas> arquivoCargaSTD = arquivoCargasService.obterPorId(numero_pedido);

		pedidoPage.validarDadosUploadArquivo(arquivoCargaSTD.get().getValor(), StatusPagamentoFrontEnum.LIMITE_CREDITO_VENCIDO.getDescricao(), StatusPedidoEnum.EFETUADO.getDescricao());
		
		limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarVisualizacaoDadosLimitePrePagoOuSemPermissao(limiteCreditoResponse);
		
	}
	
	@Test
	public void consultarLimiteGrupoPrePagoCentralizado() throws Throwable {
		JSONObject parametros = new JSONObject();

		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.CENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.PRE.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.CENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3)); 

		GrupoEmpresaResponse grupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);

		usuarioActions.atualizarSenhausuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarVisualizacaoDadosLimitePrePagoOuSemPermissao(limiteCreditoResponse);
		
	}
	
	@Test
	public void consultarLimiteGrupoPrePagoDescentralizado() throws Throwable {
		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.DESCENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.PRE.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.DESCENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		
		GrupoEmpresaResponse grupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);
		
		usuarioActions.atualizarSenhausuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarVisualizacaoDadosLimitePrePagoOuSemPermissao(limiteCreditoResponse);
		
	}
	
	@Test
	public void consultarLimiteGrupoPosPagoComExcecoesLimiteDisponivel() throws Throwable {
		
		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.DESCENTRALIZADO.name()); 
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pos
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.DESCENTRALIZADO.name()); 
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3));
		
		GrupoEmpresaResponse grupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);
		
		utilsService.inserirExcecoesLimiteDisponivel(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		usuarioActions.atualizarSenhausuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarVisualizacaoDadosLimitePrePagoOuSemPermissao(limiteCreditoResponse);
		
	}
	
	@Test
	public void consultarLimiteGrupoPrePagoComExcecoesLimiteDisponivel() throws Throwable {
		
		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.DESCENTRALIZADO.name()); 
		parametros.put("tipoContrato", TipoContratoEnum.PRE.name()); // Pos
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.DESCENTRALIZADO.name()); 
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		
		GrupoEmpresaResponse grupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);
		
		utilsService.inserirExcecoesLimiteDisponivel(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		usuarioActions.atualizarSenhausuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarVisualizacaoDadosLimitePrePagoOuSemPermissao(limiteCreditoResponse);
	}

	@Test
	public void consultarLimiteGrupoPosPagoCentralizadoComLimiteZerado() throws Throwable {
		JSONObject parametros = new JSONObject();

		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.CENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 0.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.CENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3));

		GrupoEmpresaResponse grupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);

		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();
		
		
		list_cnpjEmpresas.put(grupoEmpresaService.consultarCNPJ(grupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(7));

		JSONArray detalhesPedido = pedidosActions.gerarDetalhesPedidosArquivoLoteOuPortaPorta(list_cnpjEmpresas, 10,
				null);

		XSSFWorkbook workbook = pedidosActions
				.getPlanilhaDefaultPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name());

		pedidosActions.setDetalhesPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name(), detalhesPedido,
				workbook);

		MultipartFile xlsPedido = pedidosActions.gerarNovaPlanilhaPedidoLoteOuPortaPorta(workbook);

		usuarioActions.atualizarSenhausuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
		Long numero_pedido = pedidoPage.realizarUploadPlanilha(xlsPedido.getName());
		
		pedidosActions.validarStatusArquivoCargaSTD(numero_pedido, StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO.getStatus(), StatusPagamentoEnum.LIMITE_CREDITO_INSUFICIENTE.getStatus());
		
		pedidosActions.validarArquivoCargasDetalhes(TipoEntregaCartao.PORTA_A_PORTA.name(),
				numero_pedido, detalhesPedido);

		pedidosActions.validarCargasBeneficios(numero_pedido,
				StatusCargaBeneficioEnum.PENDENCIA_LIMITE.getStatus());
		
		Optional<ArquivoCargas> arquivoCargaSTD = arquivoCargasService.obterPorId(numero_pedido);
		
		pedidoPage.validarDadosUploadArquivo(arquivoCargaSTD.get().getValor(), StatusPagamentoFrontEnum.LIMITE_CREDITO_INSUFICIENTE.getDescricao(), StatusPedidoEnum.EFETUADO.getDescricao());

		limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
	
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
	}
	
	@Test
	public void consultarLimiteGrupoPosPagoCentralizadoComLimiteNegativo() throws Throwable {
		JSONObject parametros = new JSONObject();

		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.CENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 10.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.CENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3));

		GrupoEmpresaResponse grupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);

		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();
		
		
		list_cnpjEmpresas.put(grupoEmpresaService.consultarCNPJ(grupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(7));

		JSONArray detalhesPedido = pedidosActions.gerarDetalhesPedidosArquivoLoteOuPortaPorta(list_cnpjEmpresas, 10,
				null);

		XSSFWorkbook workbook = pedidosActions
				.getPlanilhaDefaultPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name());

		pedidosActions.setDetalhesPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name(), detalhesPedido,
				workbook);

		MultipartFile xlsPedido = pedidosActions.gerarNovaPlanilhaPedidoLoteOuPortaPorta(workbook);

		usuarioActions.atualizarSenhausuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
		Long numero_pedido = pedidoPage.realizarUploadPlanilha(xlsPedido.getName());
		
		pedidosActions.validarStatusArquivoCargaSTD(numero_pedido, StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO.getStatus(), StatusPagamentoEnum.LIMITE_CREDITO_INSUFICIENTE.getStatus());
		
		pedidosActions.validarArquivoCargasDetalhes(TipoEntregaCartao.PORTA_A_PORTA.name(),
				numero_pedido, detalhesPedido);

		pedidosActions.validarCargasBeneficios(numero_pedido,
				StatusCargaBeneficioEnum.PENDENCIA_LIMITE.getStatus());
		
		Optional<ArquivoCargas> arquivoCargaSTD = arquivoCargasService.obterPorId(numero_pedido);
		
		pedidoPage.validarDadosUploadArquivo(arquivoCargaSTD.get().getValor(), StatusPagamentoFrontEnum.LIMITE_CREDITO_INSUFICIENTE.getDescricao(), StatusPedidoEnum.EFETUADO.getDescricao());

		limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
	
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
	}
	
	@Test
	public void consultarLimiteGrupoPosPagoCentralizadoCorLimiteVerde() throws JSONException, JsonParseException, JsonMappingException, IOException, ParseException, InterruptedException {
		JSONObject parametros = new JSONObject();

		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.CENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.CENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3));

		GrupoEmpresaResponse grupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);

		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();
		
		
		list_cnpjEmpresas.put(grupoEmpresaService.consultarCNPJ(grupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(7));

		JSONArray detalhesPedido = pedidosActions.gerarDetalhesPedidosArquivoLoteOuPortaPorta(list_cnpjEmpresas, 10,
				null);

		XSSFWorkbook workbook = pedidosActions
				.getPlanilhaDefaultPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name());

		pedidosActions.setDetalhesPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name(), detalhesPedido,
				workbook);

		MultipartFile xlsPedido = pedidosActions.gerarNovaPlanilhaPedidoLoteOuPortaPorta(workbook);

		usuarioActions.atualizarSenhausuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
		Long numero_pedido = pedidoPage.realizarUploadPlanilha(xlsPedido.getName());
		
		pedidosActions.validarStatusArquivoCargaSTD(numero_pedido, StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO.getStatus(), StatusPagamentoEnum.AGUARDANDO_PAGAMENTO.getStatus());
		
		pedidosActions.validarArquivoCargasDetalhes(TipoEntregaCartao.PORTA_A_PORTA.name(),
				numero_pedido, detalhesPedido);

		pedidosActions.validarCargasBeneficios(numero_pedido,
				StatusCargaBeneficioEnum.PEDIDO_CONFIRMADO.getStatus());
		
		Optional<ArquivoCargas> arquivoCargaSTD = arquivoCargasService.obterPorId(numero_pedido);
		
		pedidoPage.validarDadosUploadArquivo(arquivoCargaSTD.get().getValor(), StatusPagamentoFrontEnum.AGUARDANDO_PAGAMENTO.getDescricao(), StatusPedidoEnum.EFETUADO.getDescricao());

		limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
	
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
	}
	
	@Test
	public void consultarLimiteGrupoPosPagoDescantralizadoCorLimiteCinza() throws JSONException, JsonParseException, JsonMappingException, IOException, ParseException, InterruptedException {
		JSONObject parametros = new JSONObject();

		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.DESCENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 120.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.DESCENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3));
		
		GrupoEmpresaResponse grupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);

		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();
		
		
		list_cnpjEmpresas.put(grupoEmpresaService.consultarCNPJ(grupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(7));

		JSONArray detalhesPedido = pedidosActions.gerarDetalhesPedidosArquivoLoteOuPortaPorta(list_cnpjEmpresas, 10,
				null);

		XSSFWorkbook workbook = pedidosActions
				.getPlanilhaDefaultPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name());

		pedidosActions.setDetalhesPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name(), detalhesPedido,
				workbook);

		MultipartFile xlsPedido = pedidosActions.gerarNovaPlanilhaPedidoLoteOuPortaPorta(workbook);
		
		usuarioActions.atualizarSenhausuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
		Long numero_pedido = pedidoPage.realizarUploadPlanilha(xlsPedido.getName());
		
		pedidosActions.validarStatusArquivoCargaSTD(numero_pedido, StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO.getStatus(), StatusPagamentoEnum.AGUARDANDO_PAGAMENTO.getStatus());
		
		pedidosActions.validarArquivoCargasDetalhes(TipoEntregaCartao.PORTA_A_PORTA.name(),
				numero_pedido, detalhesPedido);

		pedidosActions.validarCargasBeneficios(numero_pedido,
				StatusCargaBeneficioEnum.PEDIDO_CONFIRMADO.getStatus());
		
		Optional<ArquivoCargas> arquivoCargaSTD = arquivoCargasService.obterPorId(numero_pedido);
		
		pedidoPage.validarDadosUploadArquivo(arquivoCargaSTD.get().getValor(), StatusPagamentoFrontEnum.AGUARDANDO_PAGAMENTO.getDescricao(), StatusPedidoEnum.EFETUADO.getDescricao());

		limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
	
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
	}
	
	@Test
	public void consultarLimiteGrupoPosPagoDescantralizadoCorLimiteVermelho() throws JSONException, JsonParseException, JsonMappingException, IOException, ParseException, InterruptedException {
		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.DESCENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 80.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.DESCENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3));
		
		GrupoEmpresaResponse grupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);
		
		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();
		
		
		list_cnpjEmpresas.put(grupoEmpresaService.consultarCNPJ(grupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(7));
		
		JSONArray detalhesPedido = pedidosActions.gerarDetalhesPedidosArquivoLoteOuPortaPorta(list_cnpjEmpresas, 10,
				null);
		
		XSSFWorkbook workbook = pedidosActions
				.getPlanilhaDefaultPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name());
		
		pedidosActions.setDetalhesPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name(), detalhesPedido,
				workbook);
		
		MultipartFile xlsPedido = pedidosActions.gerarNovaPlanilhaPedidoLoteOuPortaPorta(workbook);
		
		usuarioActions.atualizarSenhausuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
		Long numero_pedido = pedidoPage.realizarUploadPlanilha(xlsPedido.getName());
		
		pedidosActions.validarStatusArquivoCargaSTD(numero_pedido, StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO.getStatus(), StatusPagamentoEnum.AGUARDANDO_PAGAMENTO.getStatus());
		
		pedidosActions.validarArquivoCargasDetalhes(TipoEntregaCartao.PORTA_A_PORTA.name(),
				numero_pedido, detalhesPedido);
		
		pedidosActions.validarCargasBeneficios(numero_pedido,
				StatusCargaBeneficioEnum.PEDIDO_CONFIRMADO.getStatus());
		
		Optional<ArquivoCargas> arquivoCargaSTD = arquivoCargasService.obterPorId(numero_pedido);
		
		pedidoPage.validarDadosUploadArquivo(arquivoCargaSTD.get().getValor(), StatusPagamentoFrontEnum.AGUARDANDO_PAGAMENTO.getDescricao(), StatusPedidoEnum.EFETUADO.getDescricao());
		
		limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
	}
	
	@Test
	public void consultarLimiteGrupoPosPagoDescantralizadoComCancelamentoPedidoParcial() throws Throwable {
		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.DESCENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.DESCENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3));
		
		List cnpjsCancelamento = new ArrayList<String>();
		
		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();
		
		GrupoEmpresaResponse grupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);
		
		list_cnpjEmpresas.put(grupoEmpresaService.consultarCNPJ(grupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(7));
		
		cnpjsCancelamento.add(grupoEmpresaService.consultarCNPJ(grupoEmpresaResponse.getIdPessoa().intValue()));
		
		SubgrupoEmpresaResponse subgrupoEmpresaResponse = grupoEmpresaActions.cadastrarSubgrupoEmpresa(parametros, grupoEmpresaResponse.getId());
				
		list_cnpjEmpresas.put(grupoEmpresaService.consultarCNPJ(subgrupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(10));
		
		JSONArray detalhesPedido = pedidosActions.gerarDetalhesPedidosArquivoLoteOuPortaPorta(list_cnpjEmpresas, 10,
				null);
		
		XSSFWorkbook workbook = pedidosActions
				.getPlanilhaDefaultPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name());
		
		pedidosActions.setDetalhesPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name(), detalhesPedido,
				workbook);
		
		MultipartFile xlsPedido = pedidosActions.gerarNovaPlanilhaPedidoLoteOuPortaPorta(workbook);
		
		usuarioActions.atualizarSenhausuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
		Long numero_pedido = pedidoPage.realizarUploadPlanilha(xlsPedido.getName());
		
		pedidosActions.validarStatusArquivoCargaSTD(numero_pedido, StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO.getStatus(), StatusPagamentoEnum.AGUARDANDO_PAGAMENTO.getStatus());
		
		pedidosActions.validarArquivoCargasDetalhes(TipoEntregaCartao.PORTA_A_PORTA.name(),
				numero_pedido, detalhesPedido);
		
		pedidosActions.validarCargasBeneficios(numero_pedido,
				StatusCargaBeneficioEnum.PEDIDO_CONFIRMADO.getStatus());
		
		Optional<ArquivoCargas> arquivoCargaSTD = arquivoCargasService.obterPorId(numero_pedido);
		
		pedidoPage.validarDadosUploadArquivo(arquivoCargaSTD.get().getValor(), StatusPagamentoFrontEnum.AGUARDANDO_PAGAMENTO.getDescricao(), StatusPedidoEnum.EFETUADO.getDescricao());
		
		limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
		pedidoPage.cancelarPedidoDescentralizado(cnpjsCancelamento);
		
		limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
		pedidoPage.validarDadosUploadArquivo(arquivoCargaSTD.get().getValor(), StatusPagamentoFrontEnum.AGUARDANDO_PAGAMENTO.getDescricao(), StatusPedidoEnum.PARCIALMENTE_CANCELADO.getDescricao());

	}
	
	@Test
	public void consultarLimiteGrupoPosPagoDescantralizadoComCancelamentoPedidoTotal() throws Throwable {
		
		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.DESCENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.DESCENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3));
		
		List cnpjsCancelamento = new ArrayList<String>();
		
		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();
		
		GrupoEmpresaResponse grupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);
		
		list_cnpjEmpresas.put(grupoEmpresaService.consultarCNPJ(grupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(7));
		
		cnpjsCancelamento.add(grupoEmpresaService.consultarCNPJ(grupoEmpresaResponse.getIdPessoa().intValue()));
		
		SubgrupoEmpresaResponse subgrupoEmpresaResponse = grupoEmpresaActions.cadastrarSubgrupoEmpresa(parametros, grupoEmpresaResponse.getId());
				
		list_cnpjEmpresas.put(grupoEmpresaService.consultarCNPJ(subgrupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(10));
		
		cnpjsCancelamento.add(grupoEmpresaService.consultarCNPJ(subgrupoEmpresaResponse.getIdPessoa().intValue()));
		
		JSONArray detalhesPedido = pedidosActions.gerarDetalhesPedidosArquivoLoteOuPortaPorta(list_cnpjEmpresas, 10,
				null);
		
		XSSFWorkbook workbook = pedidosActions
				.getPlanilhaDefaultPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name());
		
		pedidosActions.setDetalhesPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name(), detalhesPedido,
				workbook);
		
		MultipartFile xlsPedido = pedidosActions.gerarNovaPlanilhaPedidoLoteOuPortaPorta(workbook);
		
		usuarioActions.atualizarSenhausuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
		Long numero_pedido = pedidoPage.realizarUploadPlanilha(xlsPedido.getName());
		
		pedidosActions.validarStatusArquivoCargaSTD(numero_pedido, StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO.getStatus(), StatusPagamentoEnum.AGUARDANDO_PAGAMENTO.getStatus());
		
		pedidosActions.validarArquivoCargasDetalhes(TipoEntregaCartao.PORTA_A_PORTA.name(),
				numero_pedido, detalhesPedido);
		
		pedidosActions.validarCargasBeneficios(numero_pedido,
				StatusCargaBeneficioEnum.PEDIDO_CONFIRMADO.getStatus());
		
		Optional<ArquivoCargas> arquivoCargaSTD = arquivoCargasService.obterPorId(numero_pedido);
		
		pedidoPage.validarDadosUploadArquivo(arquivoCargaSTD.get().getValor(), StatusPagamentoFrontEnum.AGUARDANDO_PAGAMENTO.getDescricao(), StatusPedidoEnum.EFETUADO.getDescricao());
		
		limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
		pedidoPage.cancelarPedidoDescentralizado(cnpjsCancelamento);
		
		pedidoPage.validarDadosUploadArquivo(arquivoCargaSTD.get().getValor(), StatusPagamentoFrontEnum.AGUARDANDO_PAGAMENTO.getDescricao(), StatusPedidoEnum.CANCELADO.getDescricao());
		
		limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
	}
	
	@Test
	public void consultarLimiteGrupoPosPagoDescantralizadoComPagamentoParcialPedido() throws Throwable {
		
		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.DESCENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.DESCENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3));
		
		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();
		
		GrupoEmpresaResponse grupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);
		
		list_cnpjEmpresas.put(grupoEmpresaService.consultarCNPJ(grupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(7));
		
		SubgrupoEmpresaResponse subgrupoEmpresaResponse = grupoEmpresaActions.cadastrarSubgrupoEmpresa(parametros, grupoEmpresaResponse.getId());
				
		list_cnpjEmpresas.put(grupoEmpresaService.consultarCNPJ(subgrupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(10));
		
		JSONArray detalhesPedido = pedidosActions.gerarDetalhesPedidosArquivoLoteOuPortaPorta(list_cnpjEmpresas, 10,
				null);
		
		XSSFWorkbook workbook = pedidosActions
				.getPlanilhaDefaultPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name());
		
		pedidosActions.setDetalhesPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name(), detalhesPedido,
				workbook);
		
		MultipartFile xlsPedido = pedidosActions.gerarNovaPlanilhaPedidoLoteOuPortaPorta(workbook);
		
		usuarioActions.atualizarSenhausuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
		Long numero_pedido = pedidoPage.realizarUploadPlanilha(xlsPedido.getName());
		
		pedidosActions.validarStatusArquivoCargaSTD(numero_pedido, StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO.getStatus(), StatusPagamentoEnum.AGUARDANDO_PAGAMENTO.getStatus());
		
		pedidosActions.validarArquivoCargasDetalhes(TipoEntregaCartao.PORTA_A_PORTA.name(),
				numero_pedido, detalhesPedido);
		
		pedidosActions.validarCargasBeneficios(numero_pedido,
				StatusCargaBeneficioEnum.PEDIDO_CONFIRMADO.getStatus());
		
		Optional<ArquivoCargas> arquivoCargaSTD = arquivoCargasService.obterPorId(numero_pedido);
		
		pedidoPage.validarDadosUploadArquivo(arquivoCargaSTD.get().getValor(), StatusPagamentoFrontEnum.AGUARDANDO_PAGAMENTO.getDescricao(), StatusPedidoEnum.EFETUADO.getDescricao());
		
		List<CargaBeneficio> cargasBeneficios = cargaBeneficioService.obterPorIdArquivoCargas(numero_pedido);
		
		CargaBeneficio cargaBeneficio = cargasBeneficios.get(random.nextInt(cargasBeneficios.size() - 1));
		
		Optional<EmpresasCargas> empresaCargas = empresasCargasService.obterPorIdCargaBeneficio(cargaBeneficio.getIdCargaBeneficio());

		Optional<CargaControleFinanceiro> cargaControleFinanceiro = cargaControleFinanceiroService.obterPorId(cargaBeneficio.getCargaControleFinanceiro().getId());
	
		utilsService.inserirEventoExternosPagamentos(empresaCargas.get().getValor());
		
		Long id_eventopagamento = utilsService.getUltimoEventosExternosPagamentos();
		
		utilsService.pagamentoBoletoEmitidos(cargaControleFinanceiro.get().getIdBoleto(), id_eventopagamento);
		
		pedidoPage.validarDadosUploadArquivo(arquivoCargaSTD.get().getValor(), StatusPagamentoFrontEnum.AGUARDANDO_PAGAMENTO.getDescricao(), StatusPedidoEnum.EFETUADO.getDescricao());
		
		limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
	}
	
	@Test
	public void consultarLimiteGrupoPosPagoDescantralizadoComPagamentoTotalPedido() throws Throwable {
		
		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.DESCENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.DESCENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3));
		
		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();
		
		GrupoEmpresaResponse grupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);
		
		list_cnpjEmpresas.put(grupoEmpresaService.consultarCNPJ(grupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(7));
		
		SubgrupoEmpresaResponse subgrupoEmpresaResponse = grupoEmpresaActions.cadastrarSubgrupoEmpresa(parametros, grupoEmpresaResponse.getId());
				
		list_cnpjEmpresas.put(grupoEmpresaService.consultarCNPJ(subgrupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(10));
		
		JSONArray detalhesPedido = pedidosActions.gerarDetalhesPedidosArquivoLoteOuPortaPorta(list_cnpjEmpresas, 10,
				null);
		
		XSSFWorkbook workbook = pedidosActions
				.getPlanilhaDefaultPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name());
		
		pedidosActions.setDetalhesPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name(), detalhesPedido,
				workbook);
		
		MultipartFile xlsPedido = pedidosActions.gerarNovaPlanilhaPedidoLoteOuPortaPorta(workbook);
		
		usuarioActions.atualizarSenhausuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
		Long numero_pedido = pedidoPage.realizarUploadPlanilha(xlsPedido.getName());
		
		pedidosActions.validarStatusArquivoCargaSTD(numero_pedido, StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO.getStatus(), StatusPagamentoEnum.AGUARDANDO_PAGAMENTO.getStatus());
		
		pedidosActions.validarArquivoCargasDetalhes(TipoEntregaCartao.PORTA_A_PORTA.name(),
				numero_pedido, detalhesPedido);
		
		pedidosActions.validarCargasBeneficios(numero_pedido,
				StatusCargaBeneficioEnum.PEDIDO_CONFIRMADO.getStatus());
		
		Optional<ArquivoCargas> arquivoCargaSTD = arquivoCargasService.obterPorId(numero_pedido);
		
		pedidoPage.validarDadosUploadArquivo(arquivoCargaSTD.get().getValor(), StatusPagamentoFrontEnum.AGUARDANDO_PAGAMENTO.getDescricao(), StatusPedidoEnum.EFETUADO.getDescricao());
		
		List<CargaBeneficio> cargasBeneficios = cargaBeneficioService.obterPorIdArquivoCargas(numero_pedido);
	
		BigDecimal valorTotalEmpresasCargas = new BigDecimal("0.0");
		
		for (CargaBeneficio carga : cargasBeneficios) {
			
			Optional<EmpresasCargas> empresaCargas = empresasCargasService.obterPorIdCargaBeneficio(carga.getIdCargaBeneficio());
			
			Optional<CargaControleFinanceiro> cargaControleFinanceiro = cargaControleFinanceiroService.obterPorId(carga.getCargaControleFinanceiro().getId());
			
			valorTotalEmpresasCargas = valorTotalEmpresasCargas.add(empresaCargas.get().getValor());
			
			utilsService.inserirEventoExternosPagamentos(empresaCargas.get().getValor());
			
			Long id_eventopagamento = utilsService.getUltimoEventosExternosPagamentos();
			
			utilsService.pagamentoBoletoEmitidos(cargaControleFinanceiro.get().getIdBoleto(), id_eventopagamento);
		}
		
		pedidoPage.validarDadosUploadArquivo(arquivoCargaSTD.get().getValor(), StatusPagamentoFrontEnum.AGUARDANDO_PAGAMENTO.getDescricao(), StatusPedidoEnum.EFETUADO.getDescricao());
		
		limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
	}
	
	@Test
	public void consultarLimiteGrupoPosPagoCentralizadoComCancelamentoPedidoTotal() throws Throwable {
		
		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.CENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.CENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3));
		
		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();
		
		GrupoEmpresaResponse grupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);
		
		list_cnpjEmpresas.put(grupoEmpresaService.consultarCNPJ(grupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(7));
		
		SubgrupoEmpresaResponse subgrupoEmpresaResponse = grupoEmpresaActions.cadastrarSubgrupoEmpresa(parametros, grupoEmpresaResponse.getId());
				
		list_cnpjEmpresas.put(grupoEmpresaService.consultarCNPJ(subgrupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(10));
		
		JSONArray detalhesPedido = pedidosActions.gerarDetalhesPedidosArquivoLoteOuPortaPorta(list_cnpjEmpresas, 10,
				null);
		
		XSSFWorkbook workbook = pedidosActions
				.getPlanilhaDefaultPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name());
		
		pedidosActions.setDetalhesPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name(), detalhesPedido,
				workbook);
		
		MultipartFile xlsPedido = pedidosActions.gerarNovaPlanilhaPedidoLoteOuPortaPorta(workbook);
		
		usuarioActions.atualizarSenhausuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
		Long numero_pedido = pedidoPage.realizarUploadPlanilha(xlsPedido.getName());
		
		pedidosActions.validarStatusArquivoCargaSTD(numero_pedido, StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO.getStatus(), StatusPagamentoEnum.AGUARDANDO_PAGAMENTO.getStatus());
		
		pedidosActions.validarArquivoCargasDetalhes(TipoEntregaCartao.PORTA_A_PORTA.name(),
				numero_pedido, detalhesPedido);
		
		pedidosActions.validarCargasBeneficios(numero_pedido,
				StatusCargaBeneficioEnum.PEDIDO_CONFIRMADO.getStatus());
		
		Optional<ArquivoCargas> arquivoCargaSTD = arquivoCargasService.obterPorId(numero_pedido);
		
		pedidoPage.validarDadosUploadArquivo(arquivoCargaSTD.get().getValor(), StatusPagamentoFrontEnum.AGUARDANDO_PAGAMENTO.getDescricao(), StatusPedidoEnum.EFETUADO.getDescricao());
		
		limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
		pedidoPage.cancelarPedidoCentralizado();
		
		pedidoPage.validarDadosUploadArquivo(arquivoCargaSTD.get().getValor(), StatusPagamentoFrontEnum.AGUARDANDO_PAGAMENTO.getDescricao(), StatusPedidoEnum.CANCELADO.getDescricao());
		
		limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
	}
	
	@Test
	public void consultarLimiteGrupoPosPagoCentralizadoComUsuarioNivelSubgrupo() throws Throwable { 
		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.CENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.CENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3));
		
		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();
		
		GrupoEmpresaResponse grupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);
		
		SubgrupoEmpresaResponse subgrupoEmpresaResponse = grupoEmpresaActions.cadastrarSubgrupoEmpresa(parametros, grupoEmpresaResponse.getId());
		
		usuarioActions.atualizarSenhausuario(subgrupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(subgrupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), subgrupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
	}
	
	@Test
	public void consultarLimiteGrupoPosPagoCentralizadoComUsuarioNivelEmpresa() throws Throwable { 
		
		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.CENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.CENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3));
		
		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();
		
		GrupoEmpresaResponse grupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);
		
		SubgrupoEmpresaResponse subgrupoEmpresaResponse = grupoEmpresaActions.cadastrarSubgrupoEmpresa(parametros, grupoEmpresaResponse.getId());
		
		EmpresaResponse empresaReponse = grupoEmpresaActions.cadastrarEmpresa(subgrupoEmpresaResponse.getId());
		
		usuarioActions.atualizarSenhausuario(empresaReponse.getUsuarios().get(0).getId());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(empresaReponse.getUsuarios().get(0).getId());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), empresaReponse.getUsuarios().get(0).getId());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
	}
	
	@Test
	public void consultarLimiteDeGruposPosPagoCentralizadoComUsuarioComPermissaoDeGrupos() throws Throwable { 
		
		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "samuel.severino.ext@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.CENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.CENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3));
		
		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();
		
		GrupoEmpresaResponse grupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);
		
		GrupoEmpresaResponse newGrupoEmpresaResponse = cadastroGenerico.grupoCentralizadoUtils(parametros);
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		JSONArray niveisAcessos = new JSONArray();
		niveisAcessos.put(newGrupoEmpresaResponse.getId());
		
		JSONObject body = new JSONObject();
		body.put("nome", usuario.get().getNome());
		body.put("cpf", usuario.get().getCpf().trim());
		body.put("email", "felipe.roque@conductor.com.br");
		body.put("idsNivelAcesso", niveisAcessos);
		body.put("idUsuarioLogado", newGrupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		body.put("idGrupoEmpresa", newGrupoEmpresaResponse.getId());
		body.put("nivelAcesso", "GRUPO");
		
		usuarioActions.hierarquiaAcessosUsuarios(body);
		
		usuarioActions.atualizarSenhausuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		Optional<GrupoEmpresa> grupo = grupoEmpresaService.obterPorId(grupoEmpresaResponse.getId());
		
		pedidoPage.selecionarGrupo(true, grupo.get().getNomeGrupo());
		
		LimiteCreditoResponse limiteCreditoResponse = pedidosActions.consultarLimiteCredito(grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
		grupo = grupoEmpresaService.obterPorId(newGrupoEmpresaResponse.getId());
		
		pedidoPage.selecionarGrupo(false, grupo.get().getNomeGrupo());
		
		limiteCreditoResponse = pedidosActions.consultarLimiteCredito(newGrupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidoPage.validarDadosLimiteGrupo(limiteCreditoResponse);
		
	}
	
	@After
	public void after() {
		homePage.logout();
		Webdriver.loadPage(Property.API_FRONT);
	}
	
	@AfterClass
	public static void afterClass() {
		Webdriver.closeNavegator();
	}
	
}
