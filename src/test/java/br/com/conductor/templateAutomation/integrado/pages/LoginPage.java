package br.com.conductor.templateAutomation.integrado.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import br.com.conductor.templateAutomation.config.Webdriver;
import br.com.conductor.templateAutomation.integrado.report.LogReport;

public class LoginPage extends Webdriver {

	// *********Web Elements*********
	private By CAMPO_CPF = By.id("cpf");
	private By CAMPO_SENHA = By.id("password");
	private By BOTAO_CONTINUAR = By.id("lg_btn_submit");
	
	public LoginPage(WebDriver driver) {
		super(driver);
	}

	public void login(String cpf, String senha) {
		LogReport.info("METODO DE REALIZAR LOGIN: </br> CPF: " + cpf + "</br> Senha: " + senha);
		clear(CAMPO_CPF);
		sendKeys(CAMPO_CPF, cpf);
		clear(CAMPO_SENHA);
		sendKeys(CAMPO_SENHA, senha);
		click(BOTAO_CONTINUAR);
	}
	
	public void errorMessage(String message) {
		assertEquals(BOTAO_CONTINUAR, message);
	}
}
