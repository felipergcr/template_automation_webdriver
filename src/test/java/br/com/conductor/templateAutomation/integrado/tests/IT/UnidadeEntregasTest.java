package br.com.conductor.templateAutomation.integrado.tests.IT;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.templateAutomation.domain.response.GrupoEmpresaResponse;
import br.com.conductor.templateAutomation.domain.response.UnidadeEntregaResponse;
import br.com.conductor.templateAutomation.enums.ModeloDeCartao;
import br.com.conductor.templateAutomation.enums.ModeloDePedido;
import br.com.conductor.templateAutomation.enums.SegmentoEmpresaEnum;
import br.com.conductor.templateAutomation.enums.StatusArquivoUnidadeEntregaEnum;
import br.com.conductor.templateAutomation.enums.TipoContratoEnum;
import br.com.conductor.templateAutomation.enums.TipoEntregaCartao;
import br.com.conductor.templateAutomation.enums.TipoFaturamentoEnum;
import br.com.conductor.templateAutomation.enums.TipoPagamento;
import br.com.conductor.templateAutomation.enums.TipoPedido;
import br.com.conductor.templateAutomation.enums.TipoProduto;
import br.com.conductor.templateAutomation.enums.TransferenciaProdutos;
import br.com.conductor.templateAutomation.enums.UnidadeEntregaEnum;
import br.com.conductor.templateAutomation.integrado.actions.GrupoEmpresaActions;
import br.com.conductor.templateAutomation.integrado.actions.UnidadeEntregaActions;
import br.com.conductor.templateAutomation.integrado.tests.BaseTest;

public class UnidadeEntregasTest extends BaseTest {

	@Autowired
	private GrupoEmpresaActions grupoEmpresaActions;

	@Autowired
	private UnidadeEntregaActions unidadeEntregaActions;

	private JSONArray tipoProduto = new JSONArray();
	private LocalDate dataAtual = LocalDate.now();

	@Test
	public void cadastrarUnidadesEntregasGrupoPortaPorta()
			throws JSONException, JsonParseException, JsonMappingException, IOException, InterruptedException {
		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "felipe.roque@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.CENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.CENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3)); 
		
		GrupoEmpresaResponse grupoEmpresaResponse = grupoEmpresaActions.cadastrarGrupoEmpresa(parametros);

		ArrayList<JSONObject> unidadesEntregas = unidadeEntregaActions
				.gerarUnidadesEntregas(grupoEmpresaResponse.getId(), 15, null);

		XSSFWorkbook workbook = unidadeEntregaActions.getPlanilhaDefaultUnidadesEntregas();

		unidadeEntregaActions.setUnidadesEntregasPlanilhaDefault(unidadesEntregas, workbook);

		MultipartFile xlsUnidadesEntregas = unidadeEntregaActions.gerarNovaPlanilhaUnidadesEntregas(workbook);

		UnidadeEntregaResponse unidadeEntregaResponse = unidadeEntregaActions.cadastrarUnidadesEntrega(
				grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario(), xlsUnidadesEntregas);

		unidadeEntregaActions.validarProcessamentoArquivoUnidadeEntrega(
				StatusArquivoUnidadeEntregaEnum.PROCESSAMENTO_CONCLUIDO.status,
				unidadeEntregaResponse.getIdArquivoUnidadeEntrega());

		unidadeEntregaActions.validarDetalhesArquivoUnidadeEntregas(unidadeEntregaResponse.getIdArquivoUnidadeEntrega(),
				unidadesEntregas);

		unidadeEntregaActions.validarQuantidadeUnidadesEntregasGrupoEStatus(grupoEmpresaResponse.getId(),
				unidadesEntregas.size(), UnidadeEntregaEnum.ATIVA.status);

	}

	@Test
	public void cadastrarUnidadesEntregasGrupoLote()
			throws JSONException, JsonParseException, JsonMappingException, IOException, InterruptedException {
		JSONObject parametros = new JSONObject();

		parametros.put("emailNotaFiscal", "felipe.roque@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.CENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.CENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.RH.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3)); 
		
		GrupoEmpresaResponse grupoEmpresaResponse = grupoEmpresaActions.cadastrarGrupoEmpresa(parametros);

		ArrayList<JSONObject> unidadesEntregas = unidadeEntregaActions
				.gerarUnidadesEntregas(grupoEmpresaResponse.getId(), 15, null);

		XSSFWorkbook workbook = unidadeEntregaActions.getPlanilhaDefaultUnidadesEntregas();

		unidadeEntregaActions.setUnidadesEntregasPlanilhaDefault(unidadesEntregas, workbook);

		MultipartFile xlsUnidadesEntregas = unidadeEntregaActions.gerarNovaPlanilhaUnidadesEntregas(workbook);

		UnidadeEntregaResponse unidadeEntregaResponse = unidadeEntregaActions.cadastrarUnidadesEntrega(
				grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario(), xlsUnidadesEntregas);

		unidadeEntregaActions.validarProcessamentoArquivoUnidadeEntrega(
				StatusArquivoUnidadeEntregaEnum.PROCESSAMENTO_CONCLUIDO.status,
				unidadeEntregaResponse.getIdArquivoUnidadeEntrega());

		unidadeEntregaActions.validarDetalhesArquivoUnidadeEntregas(unidadeEntregaResponse.getIdArquivoUnidadeEntrega(),
				unidadesEntregas);

		unidadeEntregaActions.validarQuantidadeUnidadesEntregasGrupoEStatus(grupoEmpresaResponse.getId(),
				unidadesEntregas.size(), UnidadeEntregaEnum.ATIVA.status);
	}

	@Test
	public void atualizarunidadesEntregasGrupoPortaPorta()
			throws JSONException, JsonParseException, JsonMappingException, IOException, InterruptedException {
		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "felipe.roque@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.CENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.CENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3)); 
		
		GrupoEmpresaResponse grupoEmpresaResponse = grupoEmpresaActions.cadastrarGrupoEmpresa(parametros);

		ArrayList<JSONObject> unidadesEntregas = unidadeEntregaActions
				.gerarUnidadesEntregas(grupoEmpresaResponse.getId(), 15, null);

		XSSFWorkbook workbook = unidadeEntregaActions.getPlanilhaDefaultUnidadesEntregas();

		unidadeEntregaActions.setUnidadesEntregasPlanilhaDefault(unidadesEntregas, workbook);

		MultipartFile xlsUnidadesEntregas = unidadeEntregaActions.gerarNovaPlanilhaUnidadesEntregas(workbook);

		UnidadeEntregaResponse unidadeEntregaResponse = unidadeEntregaActions.cadastrarUnidadesEntrega(
				grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario(), xlsUnidadesEntregas);

		unidadeEntregaActions.validarProcessamentoArquivoUnidadeEntrega(
				StatusArquivoUnidadeEntregaEnum.PROCESSAMENTO_CONCLUIDO.status,
				unidadeEntregaResponse.getIdArquivoUnidadeEntrega());

		unidadeEntregaActions.validarDetalhesArquivoUnidadeEntregas(unidadeEntregaResponse.getIdArquivoUnidadeEntrega(),
				unidadesEntregas);

		unidadeEntregaActions.validarQuantidadeUnidadesEntregasGrupoEStatus(grupoEmpresaResponse.getId(),
				unidadesEntregas.size(), UnidadeEntregaEnum.ATIVA.status);

		ArrayList<JSONObject> unidadesAtualizadas = unidadeEntregaActions.atualizarUnidadesEntregas(
				grupoEmpresaResponse.getId(), unidadeEntregaResponse.getIdArquivoUnidadeEntrega());

		workbook = unidadeEntregaActions.getPlanilhaDefaultUnidadesEntregas();

		unidadeEntregaActions.setUnidadesEntregasPlanilhaDefault(unidadesAtualizadas, workbook);

		xlsUnidadesEntregas = unidadeEntregaActions.gerarNovaPlanilhaUnidadesEntregas(workbook);

		unidadeEntregaResponse = unidadeEntregaActions.cadastrarUnidadesEntrega(grupoEmpresaResponse.getId(),
				grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario(), xlsUnidadesEntregas);

		unidadeEntregaActions.validarProcessamentoArquivoUnidadeEntrega(
				StatusArquivoUnidadeEntregaEnum.PROCESSAMENTO_CONCLUIDO.status,
				unidadeEntregaResponse.getIdArquivoUnidadeEntrega());

		unidadeEntregaActions.validarDetalhesArquivoUnidadeEntregas(unidadeEntregaResponse.getIdArquivoUnidadeEntrega(),
				unidadesAtualizadas);

		unidadeEntregaActions.validarQuantidadeUnidadesEntregasGrupoEStatus(grupoEmpresaResponse.getId(),
				unidadesEntregas.size(), UnidadeEntregaEnum.DESATIVADA.status);
	}
}
