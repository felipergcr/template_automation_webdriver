package br.com.conductor.templateAutomation.integrado.pages;

import java.io.File;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.boot.env.SystemEnvironmentPropertySourceEnvironmentPostProcessor;

import br.com.conductor.templateAutomation.config.Webdriver;
import br.com.conductor.templateAutomation.domain.response.LimiteCreditoResponse;
import br.com.conductor.templateAutomation.enums.CoresGraficoLimitesEnum;
import br.com.conductor.templateAutomation.integrado.report.LogReport;
import br.com.conductor.templateAutomation.utils.Mensagens;

public class PedidoPage extends Webdriver {
	
	private By MEU_PERFIL = By.id("nav_link_userData");
	
	private By BOTAO_NOVO_PEDIDO = By.id("add_button_order");
	
	private By NOME_GRUPO = By.id("selected_group_name");
	
	private By BOTAO_MEU_PERFIL_TROCAR_GRUPO = By.id("btn_change_group");
	
	private By MENU_PEDIDOS = By.id("nav_link_RhFront.OrdersDashboard");
	
	private By BOTAO_ANEXAR_PLANILHA = By.id("file_button");
	
	private By BOTAO_ENVIAR_PEDIDO = By.id("lg_btn_submit");
	
	private By ARQUIVO = By.id("file");
	
	private By TITULO_ARQUIVO = By.id("on_feedback_file_title");
	
	private By NUMERO_PEDIDO = By.id("order_card_order_id_0");
	
	private By BOTAO_STATUS_PEDIDO = By.id("action_0");
	
	private By BOTAO_CANCELAR_PEDIDO = By.xpath("//div[contains(.,'Cancelar Pedido')]/button");
	
	private By VALOR_PEDIDO = By.id("order_card_value_0");
	
	private By LIMITE_PERCENTUAL = By.id("lmc_percent");
	
	private By MENSAGEM_UTILIZANDO_DO_SEU_LIMITE = By.id("lmc_desc");
	
	private By MENSAGEM_LIMITE_DISPONIVEL = By.xpath("//*[@id='lmc_container']/div[2]/span[1]");
	
	private By STATUS_PAGAMENTO = By.id("order_card_payment_status_0");
	
	private By COR_GRAFICO_LIMITE = By.id("lmc_container");
	
	private By BOTAO_DETALHAR_PEDIDO = By.id("action_icon_0");
	
	private By LISTA_DETALHES_PEDIDOS_CNPJS = By.xpath("//*[contains(@id, 'bo_header_cnpj')]");
	
	private By MENSAGEM_CANCELAMENTO_PEDIDO_PARCIAL = By.xpath("//*[@id='overlay']/div/div[2]/p[1]");
	
	private By MENSAGEM_ESCOLHER_GRUPO_DESEJA_ACESSAR = By.xpath("//div[contains(.,'Escolha o grupo que deseja acessar:')]/div[2]/div");
	
	private By MENSAGEM_CANCELAMENTO_PEDIDO_CENTRALIZADO = By.xpath("//*[@id='overlay']/div/div[2]/p[1]");
	
	private By MENSAGEM_ALERTA_CANCELAMENTO_PEDIDO_PARCIAL = By.xpath("//*[@id='overlay']/div/div[2]/p[2]");
	
	private By MENSAGEM_PEDIDO_NEGATIVO = By.id("lmc_desc_mailto");
	
	private By BOTAO_MODAL_CONFIRMAR = By.xpath("//button[contains(.,'confirmar')]/span");
	
	private By BOTAO_MODAL_CONTINUAR = By.xpath("//button[contains(.,'continuar')]/span");
	
	private By BOTAO_VOLTAR_DETALHES_PEDIDO = By.id("header_back_button");
	
	public PedidoPage(WebDriver driver) {
		super(driver);
	}
		
	public Long realizarUploadPlanilha(String name) {
		
		String temp = "/tmp/";
		File file = new File(temp + name);
		
		click(MENU_PEDIDOS);
		click(BOTAO_NOVO_PEDIDO);
		waitVisibility(BOTAO_ANEXAR_PLANILHA);
		waitPresenceOfElementLocate(ARQUIVO);
		sendKeys(ARQUIVO, file.getAbsolutePath());
		waitVisibility(TITULO_ARQUIVO);
		click(BOTAO_ENVIAR_PEDIDO);
		
		LogReport.info("REALIZANDO UPLADO DE ARQUIVO DO DIRETORIO: " + file.getAbsolutePath());
		LogReport.info("NUMERO DO PEDIDO: " + getText(NUMERO_PEDIDO));
		
		return Long.parseLong(getText(NUMERO_PEDIDO));
	}
	
	public void validarDadosUploadArquivo(BigDecimal valor, String statusPagamento, String statusPedido) {
		
		refresh();
		
		waitVisibility(By.xpath("//*[contains(@title, '"+statusPedido+"')]"));
		
		visibilityOfElementLocated(VALOR_PEDIDO);
		
		String valor_tela  = getText(VALOR_PEDIDO);
		
		Locale ptBr = new Locale("pt", "BR");
		
		NumberFormat nf = NumberFormat.getCurrencyInstance(ptBr); 
		
		String valorPedidoFormatado = nf.format(valor);
		
		String statusPedidoTela = getAttributeValue(By.xpath("//*[contains(@title, '"+statusPedido+"')]"), "title");
		
		LogReport.passFail(statusPedido.equals(statusPedidoTela), 
				"STATUS PEDIDO ESPERADO: " + statusPedido + 
				"<br/> STATUS PEDIDO EXIBIDO: " + statusPedidoTela);
		
		LogReport.passFail(valorPedidoFormatado.equals(valor_tela), 
				"VALOR PEDIDO ESPERADO: " + valorPedidoFormatado
				+ "</br> VALOR PEDIDO EXIBIDO: " + valor_tela);
		
		LogReport.passFail(statusPagamento.equals(getText(STATUS_PAGAMENTO)), 
				"STATUS PAGAMENTO ESPERADO: " + statusPagamento +
				"</br> STATUS PAGAMENTO EXIBIDO: " + getText(STATUS_PAGAMENTO));
	}
	
	public void selecionarGrupo(Boolean aposLogin, String nome) throws InterruptedException {
		
		if (aposLogin) {
			
			waitVisibility(MENSAGEM_ESCOLHER_GRUPO_DESEJA_ACESSAR);
			
			LogReport.passFail(Mensagens.MENSAGEM_ESCOLHER_GRUPO_DESEJA_ACESSAR.equals(getText(MENSAGEM_ESCOLHER_GRUPO_DESEJA_ACESSAR)),
					"MENSAGEM ESCOLHA DE GRUPO ESPERADA: " + Mensagens.MENSAGEM_ESCOLHER_GRUPO_DESEJA_ACESSAR +
					"</br> MENSAGEM ESCOLHA DE GRUPO RETORNADA: " + getText(MENSAGEM_ESCOLHER_GRUPO_DESEJA_ACESSAR));
			
			LogReport.passFail(ElementIsPresent(By.xpath("//div[contains(text(), '"+nome+"')]")), 
					"NOME DO GRUPO EMPRESA ESTEJA VISIVEL: " + true +
					"</br> NOME DO GRUPO EMPRESA ESTA VISIVEL ? " + ElementIsPresent(By.xpath("//div[contains(text(), '"+nome+"')]")));
			
			click(By.xpath("//div[contains(text(), '"+nome+"')]"));
			
			System.err.println(nome);
			
		} else {
			
			click(MEU_PERFIL);
			
			click(BOTAO_MEU_PERFIL_TROCAR_GRUPO);
			
			waitVisibility(MENSAGEM_ESCOLHER_GRUPO_DESEJA_ACESSAR);
			
			LogReport.passFail(Mensagens.MENSAGEM_ESCOLHER_GRUPO_DESEJA_ACESSAR.equals(getText(MENSAGEM_ESCOLHER_GRUPO_DESEJA_ACESSAR)),
					"MENSAGEM ESCOLHA DE GRUPO ESPERADA: " + Mensagens.MENSAGEM_ESCOLHER_GRUPO_DESEJA_ACESSAR +
					"</br> MENSAGEM ESCOLHA DE GRUPO RETORNADA: " + getText(MENSAGEM_ESCOLHER_GRUPO_DESEJA_ACESSAR));
			
			LogReport.passFail(ElementIsPresent(By.xpath("//div[contains(text(), '"+nome+"')]")), 
					"NOME DO GRUPO EMPRESA ESTEJA VISIVEL: " + true +
					"</br> NOME DO GRUPO EMPRESA ESTA VISIVEL ? " + ElementIsPresent(By.xpath("//div[contains(text(), '"+nome+"')]")));
			
			click(By.xpath("//div[contains(text(), '"+nome+"')]"));
			
		}
		
		Thread.sleep(2000);
		
		visibilityOfElementLocated(NOME_GRUPO);
		
		LogReport.passFail(nome.equals(getText(NOME_GRUPO)),
				"NOME DO GRUPO ESPERADO QUE SEJA EXIBIDO: " + nome +
				"</br> NOME DO GRUPO EXIBIDO: " + getText(NOME_GRUPO));
	}
	
	public void validarVisualizacaoDadosLimitePrePagoOuSemPermissao(LimiteCreditoResponse limiteCreditoResponse) {
				
        LogReport.passFail(ElementIsPresent(COR_GRAFICO_LIMITE) == limiteCreditoResponse.isValidaLimite(), 
        		"GRAFICO DE LIMITES NÃO DEVE SER EXIBIDO: " + limiteCreditoResponse.isValidaLimite() +
        		"</br> GRAFICO DE LIMITES RETORNADO ? " + ElementIsPresent(COR_GRAFICO_LIMITE));
	}
	
	public void validarDadosLimiteGrupo(LimiteCreditoResponse limiteCreditoResponse) {
		
		waitVisibility(LIMITE_PERCENTUAL);
		
		refresh();
		
		waitVisibility(LIMITE_PERCENTUAL);

		BigDecimal valorUtilizado = new BigDecimal(limiteCreditoResponse.getValorUtilizado().toString()); 
		
		BigDecimal valorDisponivel = new BigDecimal(limiteCreditoResponse.getValorLimiteDisponivel().toString());  
		
		Locale ptBr = new Locale("pt", "BR");
		
		NumberFormat nf = NumberFormat.getCurrencyInstance(ptBr);  
		
		String valorUtilizadoFormatado = nf.format(valorUtilizado);
		
		String valorDisponivelFormatado = nf.format(valorDisponivel);
		
		if (Integer.parseInt(limiteCreditoResponse.getPorcentagemUtilizado().toString()) < 25) {
			
			String corCssVerde = getAttributeValue(COR_GRAFICO_LIMITE, "aria-used-color");
			
			LogReport.passFail(corCssVerde.equals(CoresGraficoLimitesEnum.VERDE.getCor()), 
					"COR DO GRÁFICO DE LIMITES ESPERADA: " + CoresGraficoLimitesEnum.VERDE.getCor() +
					"</br> COR DO GRÁFICO DE LIMITES EXIBIDA: " + corCssVerde);
		}
		
		if (Integer.parseInt(limiteCreditoResponse.getPorcentagemUtilizado().toString()) >= 25 
				&& Integer.parseInt(limiteCreditoResponse.getPorcentagemUtilizado().toString()) < 50) {
			
			String corCssCinza = Webdriver.getDriver().findElement(COR_GRAFICO_LIMITE).getAttribute("aria-used-color");
			
			LogReport.passFail(corCssCinza.equals(CoresGraficoLimitesEnum.CINZA.getCor()), 
					"COR DO GRÁFICO DE LIMITES ESPERADA: " + CoresGraficoLimitesEnum.CINZA.getCor() +
							"</br> COR DO GRÁFICO DE LIMITES EXIBIDA: " + corCssCinza);
		}
		
		if (Integer.parseInt(limiteCreditoResponse.getPorcentagemUtilizado().toString()) >= 50 
				&& Integer.parseInt(limiteCreditoResponse.getPorcentagemUtilizado().toString()) < 75) {
			
			String corCssLaranja = Webdriver.getDriver().findElement(COR_GRAFICO_LIMITE).getAttribute("aria-used-color");
			
			LogReport.passFail(corCssLaranja.equals(CoresGraficoLimitesEnum.LARANJA.getCor()), 
					"COR DO GRÁFICO DE LIMITES ESPERADA: " + CoresGraficoLimitesEnum.LARANJA.getCor() +
							"</br> COR DO GRÁFICO DE LIMITES EXIBIDA: " + corCssLaranja);
		}
		
		if (Integer.parseInt(limiteCreditoResponse.getPorcentagemUtilizado().toString()) >= 75 ) {
			
			String corCssVermelho = Webdriver.getDriver().findElement(COR_GRAFICO_LIMITE).getAttribute("aria-used-color");
			
			LogReport.passFail(corCssVermelho.equals(CoresGraficoLimitesEnum.VERMELHO.getCor()), 
					"COR DO GRÁFICO DE LIMITES ESPERADA: " + CoresGraficoLimitesEnum.VERMELHO.getCor() +
							"</br> COR DO GRÁFICO DE LIMITES EXIBIDA: " + corCssVermelho);
		}
		
		LogReport.passFail(Mensagens.LIMITE_DISPONIVEL_PEDIDO.equals(getText(MENSAGEM_LIMITE_DISPONIVEL)), 
				"MENSAGEM ESPERADA: " + Mensagens.LIMITE_DISPONIVEL_PEDIDO + 
				"</br> MENSAGEM EXIBIDA: " + getText(MENSAGEM_LIMITE_DISPONIVEL));
		
		LogReport.passFail(Integer.parseInt(limiteCreditoResponse.getPorcentagemUtilizado().toString()) >= 100 
				? Mensagens.MENSAGEM_PEDIDO_NEGATIVO.equals(getText(MENSAGEM_PEDIDO_NEGATIVO))
				: String.format(Mensagens.LIMITE_UTILIZADO_DO_LIMITE_TOTAL, valorUtilizadoFormatado, valorDisponivelFormatado).equals(getText(MENSAGEM_UTILIZANDO_DO_SEU_LIMITE)), 
				Integer.parseInt(limiteCreditoResponse.getPorcentagemUtilizado().toString()) >= 100 
				? "MENSAGEM ESPERADA: " + Mensagens.MENSAGEM_PEDIDO_NEGATIVO +
				  "</br> MENSAGEM EXIBIDA: " + getText(MENSAGEM_PEDIDO_NEGATIVO)
				: "MENSAGEM ESPERADA: " + String.format(Mensagens.LIMITE_UTILIZADO_DO_LIMITE_TOTAL, valorUtilizadoFormatado, valorDisponivelFormatado) +
				  "</br> MENSAGEM EXIBIDA: " + getText(MENSAGEM_UTILIZANDO_DO_SEU_LIMITE));
		
		LogReport.passFail(limiteCreditoResponse.getPorcentagemUtilizado().toString().equals(getText(LIMITE_PERCENTUAL).replace("%", "")), 
				"VALOR PERCENTUAL ESPERADO: " + limiteCreditoResponse.getPorcentagemUtilizado() + "%" +
				"</br> VALOR PERCENTUAL EXIBIDO: " + getText(LIMITE_PERCENTUAL));
	}
	
	public void cancelarPedidoCentralizado() {
		
		click(BOTAO_STATUS_PEDIDO);
		
		click(BOTAO_CANCELAR_PEDIDO);
		
		LogReport.passFail(Mensagens.MENSAGEM_CANCELAMENTO_PEDIDO_CENTRALIZADO.equals(getText(MENSAGEM_CANCELAMENTO_PEDIDO_CENTRALIZADO)), 
				"MENSAGEM CANCELAMENTO ESPERADA: " + Mensagens.MENSAGEM_CANCELAMENTO_PEDIDO_CENTRALIZADO + 
				"</br> MENSAGEM CANCELAMENTO EXIBIDA: " + getText(MENSAGEM_CANCELAMENTO_PEDIDO_CENTRALIZADO));
		
		LogReport.passFail(Mensagens.MENSAGEM_ALERTA_HORARIO_CANCELAMENTO_PEDIDO_PARCIAL.equals(getText(MENSAGEM_ALERTA_CANCELAMENTO_PEDIDO_PARCIAL)), 
				"MENSAGEM CANCELAMENTO ESPERADA: " + Mensagens.MENSAGEM_ALERTA_HORARIO_CANCELAMENTO_PEDIDO_PARCIAL + 
				"</br> MENSAGEM CANCELAMENTO EXIBIDA: " + getText(MENSAGEM_ALERTA_CANCELAMENTO_PEDIDO_PARCIAL));
		
		click(BOTAO_MODAL_CONTINUAR);
	}
	
	
	public BigDecimal cancelarPedidoDescentralizado(List cnpjs) {
		
		BigDecimal valorCancelado = new BigDecimal("0.00");
		
		click(BOTAO_DETALHAR_PEDIDO);
		
		for(Object cnpj : cnpjs) {
			
			Integer indice = getIndiceElement(LISTA_DETALHES_PEDIDOS_CNPJS, cnpj.toString());
			
			String valor = getText(By.id("bo_header_amount_" + indice)).replace("R$ ", "").replace(",", ".");
			
			BigDecimal valorBig = new BigDecimal(valor);
			
			valorCancelado = valorCancelado.add(valorBig);
			
			click(By.xpath("//*[@id='action_"+indice+"']"));
			
			click(By.xpath("//*[@id='action_button_"+indice+"']"));
			
			LogReport.passFail(Mensagens.MENSAGEM_CANCELAMENTO_PEDIDO_PARCIAL.equals(getText(MENSAGEM_CANCELAMENTO_PEDIDO_PARCIAL)), 
					"MENSAGEM CANCELAMENTO ESPERADA: " + Mensagens.MENSAGEM_CANCELAMENTO_PEDIDO_PARCIAL + 
					"</br> MENSAGEM CANCELAMENTO EXIBIDA: " + getText(MENSAGEM_CANCELAMENTO_PEDIDO_PARCIAL));
			
			LogReport.passFail(Mensagens.MENSAGEM_ALERTA_HORARIO_CANCELAMENTO_PEDIDO_PARCIAL.equals(getText(MENSAGEM_ALERTA_CANCELAMENTO_PEDIDO_PARCIAL)), 
					"MENSAGEM CANCELAMENTO ESPERADA: " + Mensagens.MENSAGEM_ALERTA_HORARIO_CANCELAMENTO_PEDIDO_PARCIAL + 
					"</br> MENSAGEM CANCELAMENTO EXIBIDA: " + getText(MENSAGEM_ALERTA_CANCELAMENTO_PEDIDO_PARCIAL));
		
			click(BOTAO_MODAL_CONFIRMAR);
			
			LogReport.passFail(Mensagens.MENSAGEM_PEDIDO_CANCELADO.equals(getText(By.xpath("//*[@id='bo_header_"+indice+"']/div[6]/span"))), 
					"MENSAGEM PEDIDO CANCELADO ESPERADA: " + Mensagens.MENSAGEM_PEDIDO_CANCELADO +
					"<br/> MENSAGEM PEDIDO CANCELADO EXIBIDA: " + getText(By.xpath("//*[@id='bo_header_"+indice+"']/div[6]/span")));
		}
		
		click(BOTAO_VOLTAR_DETALHES_PEDIDO);
		
		waitVisibility(NUMERO_PEDIDO);

		return valorCancelado;
	}
	
}
