package br.com.conductor.templateAutomation.integrado.report;

import org.junit.Assert;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import br.com.conductor.templateAutomation.integrado.tests.BaseTest;

public class LogReport {

	private static String[][] tabela;
	private static String[] coluna;
	private static String[] linha;

	public static void passFail(boolean passouFalhou, String mensagem) {
		if (passouFalhou) {
			pass(mensagem + " - PASSED");
		} else {
			fail(mensagem + " - FAIL");
		}
	}

	public static void pass(String mensagem) {
		if (BaseTest.isUsingExtent) {
			BaseReport.fetchTest().log(Status.PASS, mensagem);
			Log.info(mensagem);
			Assert.assertTrue(true);
		}

	}

	public static void fail(String mensagem) {
		if (BaseTest.isUsingExtent) {
			BaseReport.fetchTest().log(Status.FAIL, mensagem);
			Assert.assertTrue(false);
		}
		Log.erro(mensagem);
	}

	public static void skip(String mensagem) {
		if (BaseTest.isUsingExtent) {
			Log.info(mensagem);
			BaseReport.fetchTest().log(Status.SKIP, mensagem);
		}
	}

	public static void warning(String mensagem) {
		if (BaseTest.isUsingExtent) {
			Log.info(mensagem);
			BaseReport.fetchTest().log(Status.WARNING, mensagem);
		}
	}

	public static void fatal(String mensagem) {
		if (BaseTest.isUsingExtent) {
			Log.erro(mensagem);
			BaseReport.fetchTest().log(Status.FATAL, mensagem);
		}
	}

	public static void info(String mensagem) {
		if (BaseTest.isUsingExtent) {
			BaseReport.fetchTest().log(Status.INFO, mensagem);
			Log.info(mensagem);
		}
	}

	public static void info(String mensagem, String requisicao) {
		if (BaseTest.isUsingExtent) {
			BaseReport.fetchTest().log(Status.INFO, mensagem);
			BaseReport.fetchTest().log(Status.INFO, requisicao);
		}
		Log.info(mensagem);
		Log.info(requisicao);
	}

	public static void extentPass(String mensagem) {
		if (BaseTest.isUsingExtent) {
			BaseReport.fetchTest().log(Status.PASS, mensagem);
		}
	}

	public static void extentFail(String mensagem) {
		if (BaseTest.isUsingExtent) {
			BaseReport.fetchTest().log(Status.FAIL, mensagem);
		}
	}

	public static void extentFatal(String mensagem) {
		if (BaseTest.isUsingExtent) {
			BaseReport.fetchTest().log(Status.FATAL, mensagem);
		}
	}

	public static void extentInfo(String mensagem) {
		if (BaseTest.isUsingExtent) {
			BaseReport.fetchTest().log(Status.INFO, mensagem);
		}
	}

	public static void extentWarning(String mensagem) {
		if (BaseTest.isUsingExtent) {
			BaseReport.fetchTest().log(Status.WARNING, mensagem);
		}
	}

	public static void addTableLog(String[] coluna, String[] linha) {
		if (BaseTest.isUsingExtent) {
			tabela = new String[coluna.length][2];
			for (int i = 0; i < coluna.length; i++) {
				tabela[i][0] = coluna[i];
				tabela[i][1] = linha[i];

			}
			BaseReport.fetchTest().log(Status.INFO, MarkupHelper.createTable(tabela));
		}
	}

	public static void addTableLog(String... strings) {
		if (BaseTest.isUsingExtent) {
			coluna = new String[strings.length / 2];
			linha = new String[strings.length / 2];
			int size = strings.length;
			for (int i = 0; i < (size / 2); i++) {
				coluna[i] = strings[i];
			}

			for (int i = (size / 2); i < size; i++) {
				linha[i - size / 2] = strings[i];
			}

			tabela = new String[size / 2][2];
			for (int i = 0; i < linha.length; i++) {
				tabela[i][0] = coluna[i];
				tabela[i][1] = linha[i];
			}
			BaseReport.fetchTest().log(Status.INFO, MarkupHelper.createTable(tabela));
		}
	}
}
