package br.com.conductor.templateAutomation.integrado.actions;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;

import br.com.conductor.templateAutomation.config.Request;
import br.com.conductor.templateAutomation.domain.custom.ArquivoCargas;
import br.com.conductor.templateAutomation.domain.custom.ArquivosCargasDetalhes;
import br.com.conductor.templateAutomation.domain.custom.CargaBeneficio;
import br.com.conductor.templateAutomation.domain.response.LimiteCreditoResponse;
import br.com.conductor.templateAutomation.domain.response.PedidoResponse;
import br.com.conductor.templateAutomation.enums.StatusArquivoEnum;
import br.com.conductor.templateAutomation.enums.TipoEntregaCartao;
import br.com.conductor.templateAutomation.integrado.report.LogReport;
import br.com.conductor.templateAutomation.service.ArquivoCargasDetalhesService;
import br.com.conductor.templateAutomation.service.ArquivoCargasService;
import br.com.conductor.templateAutomation.service.CargaBeneficioService;
import br.com.conductor.templateAutomation.utils.ConstantesPaths;
import br.com.conductor.templateAutomation.utils.ConvertGenericUtil;
import br.com.conductor.templateAutomation.utils.EntityGenericUtil;
import br.com.conductor.templateAutomation.utils.LimparDiretorio;
import br.com.twsoftware.alfred.cpf.CPF;

@Component
public class PedidosActions {

	@Autowired
	ArquivoCargasService arquivoCargasService;

	@Autowired
	ArquivoCargasDetalhesService arquivoCargasDetalhesService;

	@Autowired
	CargaBeneficioService cargaBeneficioService;

	private static File planilha;

	private Faker faker = new Faker();

	private Random random = new Random();

	private List<Integer> TIPO_PRODUTO = Arrays.asList(1, 2);

	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	private SimpleDateFormat dataFormatada = new SimpleDateFormat("dd/MM/yyyy");

	public JSONArray gerarDetalhesPedidosArquivoLoteOuPortaPorta(HashMap<String, LocalDate> listaEmpresas,
			Integer qtdBeneficiarios, ArrayList<JSONObject> unidadesEntregas) throws JSONException {

		JSONArray dados = new JSONArray();

		List<HashMap<String, LocalDate>> cnpjs = new ArrayList<HashMap<String, LocalDate>>();

		for (int i = 0; i < qtdBeneficiarios; i++) {
			Integer indice = random.nextInt(listaEmpresas.size());

			HashMap<String, LocalDate> hm = new HashMap<String, LocalDate>();

			hm.put(listaEmpresas.keySet().toArray()[indice].toString(),
					(LocalDate) listaEmpresas.values().toArray()[indice]);

			cnpjs.add(hm);
		}

		for (HashMap<String, LocalDate> cnpj : cnpjs) {

			LocalDate data = (LocalDate) Arrays.asList(cnpj.values().toArray()).get(0);

			dados.put(Objects.isNull(unidadesEntregas)
					? gerarDadosBeneficiariosArquivoPortaPorta(Arrays.asList(cnpj.keySet().toArray()).get(0).toString(),
							data)
					: gerarDadosBeneficiariosArquivoLote(Arrays.asList(cnpj.keySet().toArray()).get(0).toString(), data,
							unidadesEntregas));
		}

		return dados;

	}

	private JSONObject gerarDadosBeneficiariosArquivoPortaPorta(String cnpj, LocalDate data) throws JSONException {

		JSONObject beneficiarios = new JSONObject();

		beneficiarios.put("CNPJ", cnpj);
		beneficiarios.put("CÓDIGO DA UNIDADE", "UE" + faker.number().numberBetween(1, 999999999));
		beneficiarios.put("MATRÍCULA", RandomStringUtils.randomNumeric(5));
		beneficiarios.put("NOME COMPLETO",
				EntityGenericUtil.converter((faker.name().firstName().replaceAll("'", "") + " Beneficiario")));
		beneficiarios.put("CPF", CPF.gerar());
		beneficiarios.put("DATA DE NASCIMENTO", LocalDate.now().plusYears(-10).format(formatter));
		beneficiarios.put("PRODUTO", TIPO_PRODUTO.get(random.nextInt(TIPO_PRODUTO.size())));
		beneficiarios.put("VALOR DO CRÉDITO", faker.number().randomDouble(2, 1, 10));
		beneficiarios.put("DATA DO CRÉDITO", data.format(formatter));
		beneficiarios.put("LOGRADOURO (TIPO/AV/RUA)", "AV");
		beneficiarios.put("ENDEREÇO", ConvertGenericUtil.getAlphabetic(15));
		beneficiarios.put("NÚMERO", RandomStringUtils.randomNumeric(4));
		beneficiarios.put("COMPLEMENTO", ConvertGenericUtil.getAlphabetic(6));
		beneficiarios.put("BAIRRO", ConvertGenericUtil.getAlphabetic(15));
		beneficiarios.put("CIDADE", ConvertGenericUtil.getAlphabetic(10));
		beneficiarios.put("UF", "SP");
		beneficiarios.put("CEP", String.valueOf(EntityGenericUtil.getLong(8)));

		return beneficiarios;
	}

	private JSONObject gerarDadosBeneficiariosArquivoLote(String cnpj, LocalDate data,
			ArrayList<JSONObject> unidadesEntregas) throws JSONException {

		JSONObject beneficiarios = new JSONObject();

		beneficiarios.put("CNPJ", cnpj);
		beneficiarios.put("CÓDIGO DO LOCAL DE ENTREGA",
				unidadesEntregas.get(random.nextInt(unidadesEntregas.size() - 1)).getString("codigoUnidadeEntrega"));
		beneficiarios.put("MATRÍCULA", RandomStringUtils.randomNumeric(5));
		beneficiarios.put("NOME COMPLETO", RandomStringUtils.randomAlphabetic(5) + " Responsavel Lote");
		beneficiarios.put("CPF", CPF.gerar());
		beneficiarios.put("DATA DE NASCIMENTO", LocalDate.now().format(formatter));
		beneficiarios.put("PRODUTO", TIPO_PRODUTO.get(random.nextInt(TIPO_PRODUTO.size())));
		beneficiarios.put("VALOR DO CRÉDITO", faker.number().randomDouble(2, 1, 10));
		beneficiarios.put("DATA DO CREDITO", data.format(formatter));

		return beneficiarios;
	}

	public XSSFWorkbook getPlanilhaDefaultPedidoLoteOuPortaPorta(String tipoPedido) throws IOException {

		if (TipoEntregaCartao.PORTA_A_PORTA.name().equals(tipoPedido))

			planilha = new File(ConstantesPaths.PATH_PLANILHA_DEFAULT_PEDIDO_PORTA_PORTA);

		if (TipoEntregaCartao.RH.name().equals(tipoPedido))

			planilha = new File(ConstantesPaths.PATH_PLANILHA_DEFAULT_PEDIDO_LOTE);

		FileInputStream fileInputStream = new FileInputStream(planilha);

		XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);

		return workbook;
	}

	public void setDetalhesPedidoLoteOuPortaPorta(String tipoPedido, JSONArray detalhesPedido, XSSFWorkbook workbook)
			throws ParseException {

		if (TipoEntregaCartao.RH.name().equals(tipoPedido))

			setDetalhesPlanilhaPedidoLoteDefault(detalhesPedido, workbook);

		if (TipoEntregaCartao.PORTA_A_PORTA.name().equals(tipoPedido))

			setDetalhesPlanilhaPedidoPortaPortaDefault(detalhesPedido, workbook);
	}

	private void setDetalhesPlanilhaPedidoLoteDefault(JSONArray detalhesPedido, XSSFWorkbook workbook) {

		XSSFSheet sheet = workbook.getSheet("BEN _ Beneficiários");

		XSSFRow row = sheet.createRow(6);

		XSSFCell cell;

		CreationHelper createHelper = workbook.getCreationHelper();

		XSSFCellStyle cellStyle = workbook.createCellStyle();

		cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy"));

		for (int i = 0; i < detalhesPedido.length(); i++) {

			row = sheet.createRow(6 + i);

			try {

				cell = row.createCell(0);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("CNPJ"));

				cell = row.createCell(1);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("CÓDIGO DO LOCAL DE ENTREGA"));

				cell = row.createCell(2);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("MATRÍCULA"));

				cell = row.createCell(3);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("NOME COMPLETO"));

				cell = row.createCell(4);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("CPF"));

				cell = row.createCell(5);
				cell.setCellValue(
						dataFormatada.parse(detalhesPedido.getJSONObject(i).get("DATA DE NASCIMENTO").toString()));
				cell.setCellStyle(cellStyle);

				cell = row.createCell(6);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("PRODUTO"));

				cell = row.createCell(7);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("VALOR DO CRÉDITO"));

				cell = row.createCell(8);
				cell.setCellValue(
						dataFormatada.parse(detalhesPedido.getJSONObject(i).get("DATA DO CREDITO").toString()));
				cell.setCellStyle(cellStyle);

			} catch (Exception e) {

				LogReport.fail("ERRO AO PREENCHER A PLANILHA EXCEL: <br/>" + e.getMessage());
			}
		}
	}

	private void setDetalhesPlanilhaPedidoPortaPortaDefault(JSONArray detalhesPedido, XSSFWorkbook workbook)
			throws ParseException {

		XSSFSheet sheet = workbook.getSheet("BEN _ Beneficiários");

		XSSFRow row = sheet.createRow(6);

		XSSFCell cell;

		CreationHelper createHelper = workbook.getCreationHelper();

		XSSFCellStyle cellStyle = workbook.createCellStyle();

		cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy"));

		for (int i = 0; i < detalhesPedido.length(); i++) {

			row = sheet.createRow(6 + i);

			try {

				cell = row.createCell(0);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("CNPJ"));

				cell = row.createCell(1);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("CÓDIGO DA UNIDADE"));

				cell = row.createCell(2);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("MATRÍCULA"));

				cell = row.createCell(3);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("NOME COMPLETO"));

				cell = row.createCell(4);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("CPF"));

				cell = row.createCell(5);
				cell.setCellValue(
						dataFormatada.parse(detalhesPedido.getJSONObject(i).get("DATA DE NASCIMENTO").toString()));
				cell.setCellStyle(cellStyle);

				cell = row.createCell(6);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("PRODUTO"));

				cell = row.createCell(7);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("VALOR DO CRÉDITO"));

				cell = row.createCell(8);
				cell.setCellValue(
						dataFormatada.parse(detalhesPedido.getJSONObject(i).get("DATA DO CRÉDITO").toString()));
				cell.setCellStyle(cellStyle);

				cell = row.createCell(9);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("LOGRADOURO (TIPO/AV/RUA)"));

				cell = row.createCell(10);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("ENDEREÇO"));

				cell = row.createCell(11);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getInt("NÚMERO"));

				cell = row.createCell(12);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("COMPLEMENTO"));

				cell = row.createCell(13);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("BAIRRO"));

				cell = row.createCell(14);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("CIDADE"));

				cell = row.createCell(15);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("UF"));

				cell = row.createCell(16);
				cell.setCellValue(detalhesPedido.getJSONObject(i).getString("CEP"));

			} catch (JSONException e) {

				LogReport.fail("ERRO AO PREENCHER A PLANILHA EXCEL: <br/>" + e.getMessage());
			}
		}
	}

	public MockMultipartFile gerarNovaPlanilhaPedidoLoteOuPortaPorta(XSSFWorkbook workbook) throws IOException {

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		workbook.write(outputStream);

		outputStream.close();

		workbook.close();

		File file = gravarNovoArquivoPedidoLoteOuPortaPorta(outputStream,
				"carga_APIROQUE_PedidoLoteOuPortaPortaRoque.xlsx");

		InputStream fromoutputstream = new ByteArrayInputStream(outputStream.toByteArray());

		return new MockMultipartFile(file.getName(), "carga_APIROQUE_PedidoLoteOuPortaPortaRoque.xlsx",
				"multipart/form-data", fromoutputstream);
	}

	public File gravarNovoArquivoPedidoLoteOuPortaPorta(ByteArrayOutputStream bytes, String nomeArquivo) {

		File arquivo = null;

		try {

			LimparDiretorio.limparDiretorio("/tmp");

			String temp = "/tmp/";

			byte[] b = bytes.toByteArray();
			Path path = Paths.get(temp + nomeArquivo);
			Files.write(path, b);

			arquivo = new File(temp + nomeArquivo);

		} catch (Exception e) {

			LogReport.fail("ERRO AO GRAVAR PLANILHA EXCEL: <br/>" + e.getMessage());
		}

		return arquivo;
	}

	public PedidoResponse cadastrarPedido(MultipartFile xlsPedido, Long idGrupoEmpresa, Long idUsuario)
			throws JSONException {

		PedidoResponse pedidoResponse = new PedidoResponse();

		try {

			String temp = "/tmp/";

			File file = new File(temp + xlsPedido.getName());

			MultiValueMap<String, String> params = new LinkedMultiValueMap<>();

			params.add("idGrupoEmpresa", idGrupoEmpresa.toString());
			params.add("idUsuario", idUsuario.toString());

			Request request = new Request();

			ResponseEntity<?> t = request.postFileApi(ConstantesPaths.PATH_UPLOAD, params, file, "ARQUIVO DE PEDIDO");

			if (HttpStatus.CREATED.equals(t.getStatusCode())) {

				ObjectMapper mapper = new ObjectMapper();

				pedidoResponse = mapper.readValue(t.getBody().toString(), PedidoResponse.class);
			}

		} catch (Exception e) {

			LogReport.fail("ERRO AO CADASTRAR PEDIDO: <br/>" + e.getMessage());
		}

		return pedidoResponse;
	}

	public void validarStatusArquivoCargaSTD(Long id_arquivocargastd, Integer statusPedido, Integer statusPagamento) throws InterruptedException {

		final long startTime = System.currentTimeMillis();
	    final long maxLoadTime = TimeUnit.SECONDS.toMillis(30000L);
	    
		Optional<ArquivoCargas> arquivoCargaSTD = arquivoCargasService.obterPorId(id_arquivocargastd);

		while (arquivoCargaSTD.get().getStatus().equals(StatusArquivoEnum.RECEBIDO.getStatus())
				|| arquivoCargaSTD.get().getStatus().equals(StatusArquivoEnum.IMPORTADO.getStatus())
				&& (System.currentTimeMillis() - startTime < maxLoadTime)) {

			arquivoCargaSTD = arquivoCargasService.obterPorId(id_arquivocargastd);
		}

		LogReport.passFail(
				arquivoCargaSTD.get().getStatus().equals(statusPedido),
				"STATUS DO PEDIDO ARQUIVO CARGAS ESPERADO: " + statusPedido
						+ "<br/> STATUS DO PEDIDO ARQUIVO CARGAS RETORNADO: " + arquivoCargaSTD.get().getStatus());
		
		LogReport.passFail(
				arquivoCargaSTD.get().getStatusPagamento().equals(statusPagamento),
				"STATUS DO PAGAMENTO ARQUIVO CARGAS ESPERADO: " + statusPagamento
				+ "<br/> STATUS DO PAGAMENTO ARQUIVO CARGAS RETORNADO: " + arquivoCargaSTD.get().getStatusPagamento());
	}

	public void validarArquivoCargasDetalhes(String tipo, Long id_arquivocargastd, JSONArray detalhesPedido)
			throws JSONException {

		Optional<List<ArquivosCargasDetalhes>> arquivosCargasDetalhesBase = arquivoCargasDetalhesService
				.obterPorId(id_arquivocargastd);

		Integer totalDetalhesBase = arquivosCargasDetalhesBase.get().size();

		LogReport.passFail(totalDetalhesBase.equals(detalhesPedido.length()), "QUANTIDADE DE DETALHES ESPERADO: "
				+ detalhesPedido.length() + "<br/> QUANTIDADE DE DETALHES RETORNADO: " + totalDetalhesBase);

		for (int i = 0; i < detalhesPedido.length(); i++) {

			for (ArquivosCargasDetalhes detalhe : arquivosCargasDetalhesBase.get()) {

				if (detalhe.getCpf().equals(detalhesPedido.getJSONObject(i).getString("CPF"))) {

					LogReport
							.passFail(
									tipo.equals(TipoEntregaCartao.PORTA_A_PORTA.name()) ? detalhe.getBairro()
											.equals(detalhesPedido.getJSONObject(i).getString("BAIRRO")) : true,
									tipo.equals(TipoEntregaCartao.PORTA_A_PORTA.name())
											? "BAIRRO ESPERADO: " + detalhesPedido.getJSONObject(i).getString("BAIRRO")
													+ "<br/> BAIRRO RETORNADO: " + detalhe.getBairro()
											: "GRUPO LOTE NÃO CONTÉM BAIRRO NOS DETALHES");
					LogReport
							.passFail(
									tipo.equals(TipoEntregaCartao.PORTA_A_PORTA.name()) ? detalhe.getCidade()
											.equals(detalhesPedido.getJSONObject(i).getString("CIDADE")) : true,
									tipo.equals(TipoEntregaCartao.PORTA_A_PORTA.name())
											? "CIDADE ESPERADO: " + detalhesPedido.getJSONObject(i).getString("CIDADE")
													+ "<br/> CIDADE RETORNADO: " + detalhe.getCidade()
											: "GRUPO LOTE NÃO CONTÉM CIDADE NOS DETALHES");
					LogReport
							.passFail(
									tipo.equals(TipoEntregaCartao.PORTA_A_PORTA.name())
											? detalhe.getCodUnidade().equals(
													detalhesPedido.getJSONObject(i).getString("CÓDIGO DA UNIDADE"))
											: detalhe.getCodUnidade()
													.equals(detalhesPedido.getJSONObject(i)
															.getString("CÓDIGO DO LOCAL DE ENTREGA")),
									tipo.equals(TipoEntregaCartao.PORTA_A_PORTA.name()) ? "CODIGO DA UNIDADE ESPERADO: "
											+ detalhesPedido.getJSONObject(i).getString("CÓDIGO DA UNIDADE")
											+ "<br/> CODIGO DA UNIDADE RETORNADO: " + detalhe.getCodUnidade()
											: "CODIGO DA UNIDADE ESPERADO: "
													+ detalhesPedido.getJSONObject(i)
															.getString("CÓDIGO DO LOCAL DE ENTREGA")
													+ "<br/> CODIGO DA UNIDADE RETORNADO: " + detalhe.getCodUnidade());
					LogReport.passFail(
							tipo.equals(TipoEntregaCartao.PORTA_A_PORTA.name()) ? detalhe.getComplemento()
									.equals(detalhesPedido.getJSONObject(i).getString("COMPLEMENTO")) : true,
							tipo.equals(TipoEntregaCartao.PORTA_A_PORTA.name())
									? "COMPLEMENTO ESPERADO: "
											+ detalhesPedido.getJSONObject(i).getString("COMPLEMENTO")
											+ "<br/> COMPLEMENTO RETORNADO: " + detalhe.getComplemento()
									: "GRUPO LOTE NÃO CONTÉM COMPLEMENTO NOS DETALHES");
					LogReport.passFail(
							tipo.equals(TipoEntregaCartao.PORTA_A_PORTA.name())
									? detalhe.getDataCredito().equals(
											detalhesPedido.getJSONObject(i).getString("DATA DO CRÉDITO"))
									: detalhe.getDataCredito()
											.equals(detalhesPedido.getJSONObject(i).getString("DATA DO CREDITO")),
							tipo.equals(TipoEntregaCartao.PORTA_A_PORTA.name())
									? "DATA DE CREDITO ESPERADO: "
											+ detalhesPedido.getJSONObject(i).getString("DATA DO CRÉDITO")
											+ "<br/> DATA DE CREDITO RETORNADO: " + detalhe.getDataCredito()
									: "DATA DE CREDITO ESPERADO: "
											+ detalhesPedido.getJSONObject(i).getString("DATA DO CREDITO")
											+ "<br/> DATA DE CREDITO RETORNADO: " + detalhe.getDataCredito());
					LogReport.passFail(
							detalhe.getDataNascimento()
									.equals(detalhesPedido.getJSONObject(i).getString("DATA DE NASCIMENTO")),
							"DATA DE NASCIMENTO ESPERADO: "
									+ detalhesPedido.getJSONObject(i).getString("DATA DE NASCIMENTO")
									+ "<br/> DATA DE NASCIMENTO RETORNADO: " + detalhe.getDataNascimento());
					LogReport.passFail(
							tipo.equals(TipoEntregaCartao.PORTA_A_PORTA.name()) ? detalhe.getEndereco()
									.equals(detalhesPedido.getJSONObject(i).getString("ENDEREÇO")) : true,
							tipo.equals(TipoEntregaCartao.PORTA_A_PORTA.name())
									? "ENDERECO ESPERADO: " + detalhesPedido.getJSONObject(i).getString("ENDEREÇO")
											+ "<br/> ENDERECO RETORNADO: " + detalhe.getEndereco()
									: "GRUPO LOTE NÃO CONTÉM ENDERECO NOS DETALHES");
					LogReport
							.passFail(
									tipo.equals(TipoEntregaCartao.PORTA_A_PORTA.name()) ? detalhe.getLogradouro()
											.equals(detalhesPedido.getJSONObject(i)
													.getString("LOGRADOURO (TIPO/AV/RUA)"))
											: true,
									tipo.equals(TipoEntregaCartao.PORTA_A_PORTA.name())
											? "LOGRADOURO ESPERADO: "
													+ detalhesPedido.getJSONObject(i)
															.getString("LOGRADOURO (TIPO/AV/RUA)")
													+ "<br/> LOGRADOURO RETORNADO: " + detalhe.getLogradouro()
											: "GRUPO LOTE NÃO CONTÉM LOGRADOURO NOS DETALHES");
					LogReport
							.passFail(
									detalhe.getMatricula()
											.equals(StringUtils.stripStart(
													detalhesPedido.getJSONObject(i).getString("MATRÍCULA"), "0")),
									"MATRICULA ESPERADO: "
											+ StringUtils.stripStart(
													detalhesPedido.getJSONObject(i).getString("MATRÍCULA"), "0")
											+ "<br/> MATRICULA RETORNADO: " + detalhe.getMatricula());
					LogReport.passFail(
							tipo.equals(TipoEntregaCartao.PORTA_A_PORTA.name())
									? detalhe.getUf().equals(detalhesPedido.getJSONObject(i).getString("UF"))
									: true,
							tipo.equals(TipoEntregaCartao.PORTA_A_PORTA.name())
									? "UF ESPERADO: " + detalhesPedido.getJSONObject(i).getString("UF")
											+ "<br/> UF RETORNADO: " + detalhe.getUf()
									: "GRUPO LOTE NÃO CONTÉM UF NOS DETALHES");
					LogReport.passFail(
							detalhe.getValorBeneficio()
									.equals(detalhesPedido.getJSONObject(i).getString("VALOR DO CRÉDITO")),
							"VALOR BENEFICIO ESPERADO: " + detalhesPedido.getJSONObject(i).getString("VALOR DO CRÉDITO")
									+ "<br/> VALOR BENEFICIO RETORNADO: " + detalhe.getValorBeneficio());
					LogReport.passFail(
							detalhe.getNome().equals(detalhesPedido.getJSONObject(i).getString("NOME COMPLETO")),
							"NOME ESPERADO: " + detalhesPedido.getJSONObject(i).getString("NOME COMPLETO")
									+ "<br/> NOME RETORNADO: " + detalhe.getNome());
				}

			}
		}
	}

	public void validarCargasBeneficios(Long id_arquivocargastd, Integer status) throws JSONException {

		List<CargaBeneficio> arquivosCargasBeneficios = cargaBeneficioService
				.obterPorIdArquivoCargas(id_arquivocargastd);

		arquivosCargasBeneficios.stream().forEach(item -> {

			LogReport.passFail(item.getStatus().equals(status), "STATUS DA CARGAS BENEFICIO ESPERADO: " + status
					+ "<br/> STATUS DA CARGAS BENEFICIO RETORNADO: " + item.getStatus());

		});
	}
	
	public void cancelarPedidoCentralizado(Long id_arquivoCargas, Long id_usuario) {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();

		params.add("idUsuario", id_usuario.toString());

		Request request = new Request();

		ResponseEntity<?> t = request.putApi(ConstantesPaths.PATH_PEDIDOS + "/" + id_arquivoCargas + ConstantesPaths.PATH_CANCELAR, params, null, "CANCELAMENTO PEDIDO CENTRALIZADO");

	}
	
	
	public LimiteCreditoResponse consultarLimiteCredito(Long id_grupoEmpresa, Long id_usuario) throws JsonParseException, JsonMappingException, IOException, InterruptedException {
		
		LimiteCreditoResponse limiteCreditoResponse = new LimiteCreditoResponse();
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		
		params.add("idGrupoEmpresa", id_grupoEmpresa.toString());
		params.add("idUsuario", id_usuario.toString());
		
		Request request = new Request();
		
		ResponseEntity<?> t = request.getApi(ConstantesPaths.PATH_LIMITE_CREDITO, params, "LIMITE DE CREDITO");
				
		if(HttpStatus.OK.equals(t.getStatusCode()))	{
			
			ObjectMapper mapper = new ObjectMapper();
			
			limiteCreditoResponse = mapper.readValue(t.getBody().toString(), LimiteCreditoResponse.class);
		}
			
		return limiteCreditoResponse;
	}
	
}
