package br.com.conductor.templateAutomation.integrado.tests.E2E;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Optional;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.templateAutomation.config.Property;
import br.com.conductor.templateAutomation.config.Webdriver;
import br.com.conductor.templateAutomation.controleAcesso.domain.Usuarios;
import br.com.conductor.templateAutomation.domain.custom.ArquivoCargas;
import br.com.conductor.templateAutomation.domain.response.GrupoEmpresaResponse;
import br.com.conductor.templateAutomation.enums.ModeloDeCartao;
import br.com.conductor.templateAutomation.enums.ModeloDePedido;
import br.com.conductor.templateAutomation.enums.SegmentoEmpresaEnum;
import br.com.conductor.templateAutomation.enums.StatusArquivoEnum;
import br.com.conductor.templateAutomation.enums.StatusCargaBeneficioEnum;
import br.com.conductor.templateAutomation.enums.StatusPagamentoEnum;
import br.com.conductor.templateAutomation.enums.StatusPagamentoFrontEnum;
import br.com.conductor.templateAutomation.enums.StatusPedidoEnum;
import br.com.conductor.templateAutomation.enums.TipoContratoEnum;
import br.com.conductor.templateAutomation.enums.TipoEntregaCartao;
import br.com.conductor.templateAutomation.enums.TipoFaturamentoEnum;
import br.com.conductor.templateAutomation.enums.TipoPagamento;
import br.com.conductor.templateAutomation.enums.TipoPedido;
import br.com.conductor.templateAutomation.enums.TipoProduto;
import br.com.conductor.templateAutomation.enums.TransferenciaProdutos;
import br.com.conductor.templateAutomation.integrado.actions.GrupoEmpresaActions;
import br.com.conductor.templateAutomation.integrado.actions.PedidosActions;
import br.com.conductor.templateAutomation.integrado.actions.UsuarioActions;
import br.com.conductor.templateAutomation.integrado.pages.HomePage;
import br.com.conductor.templateAutomation.integrado.pages.LoginPage;
import br.com.conductor.templateAutomation.integrado.pages.PedidoPage;
import br.com.conductor.templateAutomation.integrado.tests.BaseTest;
import br.com.conductor.templateAutomation.repository.custom.ArquivoUnidadeEntregaRepositoryImpl;
import br.com.conductor.templateAutomation.repository.custom.GrupoEmpresaRepositoryCustom;
import br.com.conductor.templateAutomation.service.ArquivoCargasService;
import br.com.conductor.templateAutomation.utils.CadastroGenerico;

public class PedidoE2ETest extends BaseTest {

	@Autowired
	CadastroGenerico cadastroGenerico;
	
	@Autowired
	ArquivoCargasService arquivoCargasService;
	
	@Autowired
	UsuarioActions usuarioActions;
	
	@Autowired
	private GrupoEmpresaActions grupoEmpresaActions;
	
	@Autowired
	PedidosActions pedidosActions;
	
	@Autowired
	GrupoEmpresaRepositoryCustom grupoEmpresaRepositoryCustom;
	
	private JSONArray tipoProduto = new JSONArray();
	private LocalDate dataAtual = LocalDate.now();
	
	private static Webdriver driver = new Webdriver();
	private static LoginPage loginPage = new LoginPage(Webdriver.getDriver());
	private static HomePage homePage = new HomePage(Webdriver.getDriver());
	private static PedidoPage pedidoPage = new PedidoPage(Webdriver.getDriver());
	
	@Before
	public void before() {
		driver.navegateTo(Property.API_FRONT);
	}
	
	@Test
	public void cancelamentoPedidoCentralizadoE2E() throws JSONException, JsonParseException, JsonMappingException, IOException, ParseException, InterruptedException {
		
		JSONObject parametros = new JSONObject();

		parametros.put("emailNotaFiscal", "felipe.roque@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.CENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.PRE.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.1);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.CENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3));

		GrupoEmpresaResponse grupoEmpresaResponse = grupoEmpresaActions.cadastrarGrupoEmpresa(parametros);

		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();

		list_cnpjEmpresas.put(grupoEmpresaRepositoryCustom.consultarCNPJ(grupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(7));

		JSONArray detalhesPedido = pedidosActions.gerarDetalhesPedidosArquivoLoteOuPortaPorta(list_cnpjEmpresas, 10,
				null);

		XSSFWorkbook workbook = pedidosActions
				.getPlanilhaDefaultPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name());

		pedidosActions.setDetalhesPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name(), detalhesPedido,
				workbook);

		MultipartFile xlsPedido = pedidosActions.gerarNovaPlanilhaPedidoLoteOuPortaPorta(workbook);
		
		usuarioActions.atualizarSenhausuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		Optional<Usuarios> usuario = usuarioActions.consultarUsuario(grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		loginPage.login(usuario.get().getCpf().toString(), "Teste@123");
		
		Long numero_pedido = pedidoPage.realizarUploadPlanilha(xlsPedido.getName());
		
		pedidosActions.validarStatusArquivoCargaSTD(numero_pedido, StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO.getStatus(), StatusPagamentoEnum.AGUARDANDO_PAGAMENTO.getStatus());
		
		pedidosActions.validarArquivoCargasDetalhes(TipoEntregaCartao.PORTA_A_PORTA.name(),
				numero_pedido, detalhesPedido);

		pedidosActions.validarCargasBeneficios(numero_pedido,
				StatusCargaBeneficioEnum.PEDIDO_CONFIRMADO.getStatus());
		
		Optional<ArquivoCargas> arquivoCargaSTD = arquivoCargasService.obterPorId(numero_pedido);
		
		pedidoPage.validarDadosUploadArquivo(arquivoCargaSTD.get().getValor(), StatusPagamentoFrontEnum.AGUARDANDO_PAGAMENTO.getDescricao(), StatusPedidoEnum.EFETUADO.getDescricao());
		
	}
	
	@After
	public void after() {
		
		Webdriver.closeNavegator();
	}
}
