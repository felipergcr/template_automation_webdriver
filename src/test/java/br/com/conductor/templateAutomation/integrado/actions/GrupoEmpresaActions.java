package br.com.conductor.templateAutomation.integrado.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;

import br.com.conductor.templateAutomation.config.Request;
import br.com.conductor.templateAutomation.domain.custom.HierarquiaOrganizacionalGrupo;
import br.com.conductor.templateAutomation.domain.response.EmpresaResponse;
import br.com.conductor.templateAutomation.domain.response.GrupoEmpresaResponse;
import br.com.conductor.templateAutomation.domain.response.HierarquiaOrganizacionalGrupoResponse;
import br.com.conductor.templateAutomation.domain.response.SubgrupoEmpresaResponse;
import br.com.conductor.templateAutomation.integrado.report.LogReport;
import br.com.conductor.templateAutomation.service.GrupoEmpresaService;
import br.com.conductor.templateAutomation.utils.ConstantesPaths;
import br.com.conductor.templateAutomation.utils.ConvertGenericUtil;
import br.com.conductor.templateAutomation.utils.GeradorMassas;
import net.minidev.json.JSONArray;

@Component
public class GrupoEmpresaActions {

	@Autowired
	GrupoEmpresaService grupoEmpresaService;

	private Faker faker = new Faker();
	private ObjectMapper mapper = new ObjectMapper();

	public GrupoEmpresaResponse cadastrarGrupoEmpresa(JSONObject parametros)
			throws JsonParseException, JsonMappingException, IOException, JSONException {

		JSONObject grupo = new JSONObject();
		GrupoEmpresaResponse grupoEmpresaResponse = new GrupoEmpresaResponse();
		GeradorMassas gerar = new GeradorMassas();
		
		try {
			
			grupo.put("nomeGrupo", faker.name().lastName() + " Grupo");
			grupo.put("nomeSubgrupo", faker.name().lastName() + " Subgrupo");
			grupo.put("parametros", parametros);
			grupo.put("condicoes", gerar.condicoesGrupoEmpresa());
			grupo.put("empresaMatriz", gerar.empresaMatrizGrupo());

			Request request = new Request();

			ResponseEntity<?> t = request.postApi(ConstantesPaths.PATH_GRUPO_EMPRESAS, null, grupo, "GRUPO EMPRESA");

			if (HttpStatus.CREATED.equals(t.getStatusCode())) {

				grupoEmpresaResponse = mapper.readValue(t.getBody().toString(), GrupoEmpresaResponse.class);
			}

		} catch (JSONException e) {
			LogReport.fail("ERRO AO CADASTRAR GRUPO EMPRESA: <br/>" + e.getMessage());
		}

		return grupoEmpresaResponse;
	}

	public SubgrupoEmpresaResponse cadastrarSubgrupoEmpresa(JSONObject parametros, Long idGrupo) throws Throwable {

		SubgrupoEmpresaResponse subgrupoEmpresaResponse = new SubgrupoEmpresaResponse();
		JSONObject subgrupo = new JSONObject();
		GeradorMassas gerar = new GeradorMassas();

		try {

			subgrupo.put("parametros", parametros);
			subgrupo.put("nomeSubGrupo", faker.name().firstName() + " SUBGRUPO");
			subgrupo.put("empresaMatriz", gerar.empresaMatrizGrupo());
			subgrupo.put("condicoes", gerar.condicoesGrupoEmpresa());

			Request request = new Request();

			ResponseEntity<?> t = request.postApi(
					ConstantesPaths.PATH_GRUPO_EMPRESAS + "/" + idGrupo + ConstantesPaths.PATH_SUBGRUPO_EMPRESAS, null,
					subgrupo, "SUBGRUPO EMPRESAS");

			if (HttpStatus.CREATED.equals(t.getStatusCode())) {

				ObjectMapper mapper = new ObjectMapper();
				subgrupoEmpresaResponse = mapper.readValue(t.getBody().toString(), SubgrupoEmpresaResponse.class);
			}

		} catch (IOException e) {
			LogReport.fail("ERRO AO CADASTRAR SUBGRUPO EMPRESA: <br/>" + e.getMessage());
		}

		return subgrupoEmpresaResponse;
	}

	public EmpresaResponse cadastrarEmpresa(Long idSubgrupo) throws Throwable {

		EmpresaResponse empresaResponse = new EmpresaResponse();

		GeradorMassas gerar = new GeradorMassas();

		try {

			Request request = new Request();

			ResponseEntity<?> t = request.postApi(
					ConstantesPaths.PATH_SUBGRUPO_EMPRESAS + "/" + idSubgrupo + ConstantesPaths.PATH_EMPRESAS, null,
					gerar.empresaMatrizGrupo(), "EMPRESA");

			if (HttpStatus.CREATED.equals(t.getStatusCode())) {

				ObjectMapper mapper = new ObjectMapper();
				empresaResponse = mapper.readValue(t.getBody().toString(), EmpresaResponse.class);
			}

		} catch (IOException e) {
			LogReport.fail("ERRO AO CADASTRAR EMPRESA: <br/>" + e.getMessage());
		}

		return empresaResponse;
	}

	public HierarquiaOrganizacionalGrupoResponse consultarHierarquiaOrganizalcionalGrupo(Long idGrupoEmpresa,
			Long IdUsuarioSessao) {

		HierarquiaOrganizacionalGrupoResponse hierarquiaOrganizacionalGrupoResponse = null;

		try {

			MultiValueMap<String, String> params = new LinkedMultiValueMap<>();

			params.add("idUsuarioSessao", IdUsuarioSessao.toString());

			Request request = new Request();

			ResponseEntity<?> t = request.getApi(
					ConstantesPaths.PATH_GRUPO_EMPRESAS + "/" + idGrupoEmpresa
							+ ConstantesPaths.PATH_HIERARQUIA_ORGANIZACIONAL,
					params, "HIERARQUIA ORGANIZACIONAL GRUPO");

			ObjectMapper mapper = new ObjectMapper();
			hierarquiaOrganizacionalGrupoResponse = mapper.readValue(t.getBody().toString(),
					HierarquiaOrganizacionalGrupoResponse.class);

		} catch (Exception e) {
			LogReport.fail("ERRO AO CONSULTAR HIERARQUIA ORGANIZACIONAL: <br/>" + e.getMessage());
		}

		return hierarquiaOrganizacionalGrupoResponse;
	}

	public void validarHierarquiaOrganizacional(HierarquiaOrganizacionalGrupoResponse hierarquiaOrganizalcionalResponse,
			Long id) {

		List<HierarquiaOrganizacionalGrupo> hierarquiaOrganizacionalGrupo = grupoEmpresaService
				.consultaHierarquiaOrganizacional(id);

		for (int i = 0; i < hierarquiaOrganizalcionalResponse.getSubGrupos().size();) {

			for (int h = 0; h < hierarquiaOrganizacionalGrupo.size();) {

				if (!hierarquiaOrganizacionalGrupo.get(h).getIdGrupoEmpresa()
						.equals(hierarquiaOrganizalcionalResponse.getIdGrupoEmpresa())
						|| !hierarquiaOrganizacionalGrupo.get(h).getNomeGrupoEmpresa()
								.equals(hierarquiaOrganizalcionalResponse.getNome())) {

					LogReport.fail(
							"NOME DO GRUPO ESPERADO: " + hierarquiaOrganizacionalGrupo.get(h).getNomeGrupoEmpresa()
									+ " <br/> NOME DO GRUPO RETORNADO: " + hierarquiaOrganizalcionalResponse.getNome());
				}

				if (!hierarquiaOrganizalcionalResponse.getSubGrupos().get(i).getId()
						.equals(hierarquiaOrganizacionalGrupo.get(h).getIdSubGrupoEmpresa())
						|| !hierarquiaOrganizalcionalResponse.getSubGrupos().get(i).getNome()
								.equals(hierarquiaOrganizacionalGrupo.get(h).getNomeSubGrupoEmpresa())) {

					LogReport.fail("NOME DO SUBGRUPO ESPERADO: "
							+ hierarquiaOrganizacionalGrupo.get(h).getNomeSubGrupoEmpresa()
							+ " <br/> NOME DO GRUPO RETORNADO: "
							+ hierarquiaOrganizalcionalResponse.getSubGrupos().get(i).getNome());
				}

				for (int j = 0; j < hierarquiaOrganizalcionalResponse.getSubGrupos().get(i).getEmpresas().size(); j++) {

					if (!hierarquiaOrganizalcionalResponse.getSubGrupos().get(i).getEmpresas().get(j).getId()
							.equals(hierarquiaOrganizacionalGrupo.get(h).getIdEmpresa())
							|| !hierarquiaOrganizalcionalResponse.getSubGrupos().get(i).getEmpresas().get(j).getNome()
									.equals(hierarquiaOrganizacionalGrupo.get(h).getNomeEmpresa())
							|| !hierarquiaOrganizalcionalResponse.getSubGrupos().get(i).getEmpresas().get(j).getCnpj()
									.equals(hierarquiaOrganizacionalGrupo.get(h).getCnpj())) {

						LogReport.fail(
								"NOME DA EMPRESA ESPERADO: " + hierarquiaOrganizacionalGrupo.get(h).getNomeEmpresa()
										+ " <br/> NOME DA EMPRESA RETORNADO: " + hierarquiaOrganizalcionalResponse
												.getSubGrupos().get(i).getEmpresas().get(j).getNome());

						LogReport.fail("CNPJ DO EMPRESA ESPERADO: " + hierarquiaOrganizacionalGrupo.get(h).getCnpj()
								+ " <br/> NOME DO GRUPO RETORNADO: " + hierarquiaOrganizalcionalResponse.getSubGrupos()
										.get(i).getEmpresas().get(j).getCnpj());
					}
					h++;
				}
				i++;
			}
		}

	}
}
