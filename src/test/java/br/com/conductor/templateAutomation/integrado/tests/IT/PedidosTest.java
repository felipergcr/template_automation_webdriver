package br.com.conductor.templateAutomation.integrado.tests.IT;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.templateAutomation.domain.response.GrupoEmpresaResponse;
import br.com.conductor.templateAutomation.domain.response.PedidoResponse;
import br.com.conductor.templateAutomation.domain.response.UnidadeEntregaResponse;
import br.com.conductor.templateAutomation.enums.ModeloDeCartao;
import br.com.conductor.templateAutomation.enums.ModeloDePedido;
import br.com.conductor.templateAutomation.enums.SegmentoEmpresaEnum;
import br.com.conductor.templateAutomation.enums.StatusArquivoEnum;
import br.com.conductor.templateAutomation.enums.StatusArquivoUnidadeEntregaEnum;
import br.com.conductor.templateAutomation.enums.StatusCargaBeneficioEnum;
import br.com.conductor.templateAutomation.enums.StatusPagamentoEnum;
import br.com.conductor.templateAutomation.enums.TipoContratoEnum;
import br.com.conductor.templateAutomation.enums.TipoEntregaCartao;
import br.com.conductor.templateAutomation.enums.TipoFaturamentoEnum;
import br.com.conductor.templateAutomation.enums.TipoPagamento;
import br.com.conductor.templateAutomation.enums.TipoPedido;
import br.com.conductor.templateAutomation.enums.TipoProduto;
import br.com.conductor.templateAutomation.enums.TransferenciaProdutos;
import br.com.conductor.templateAutomation.enums.UnidadeEntregaEnum;
import br.com.conductor.templateAutomation.integrado.actions.GrupoEmpresaActions;
import br.com.conductor.templateAutomation.integrado.actions.PedidosActions;
import br.com.conductor.templateAutomation.integrado.actions.UnidadeEntregaActions;
import br.com.conductor.templateAutomation.integrado.tests.BaseTest;
import br.com.conductor.templateAutomation.repository.custom.GrupoEmpresaRepositoryCustom;

public class PedidosTest extends BaseTest {

	@Autowired
	PedidosActions pedidosActions;

	@Autowired
	private UnidadeEntregaActions unidadeEntregaActions;

	@Autowired
	private GrupoEmpresaActions grupoEmpresaActions;

	@Autowired
	GrupoEmpresaRepositoryCustom grupoEmpresaRepositoryCustom;

	private LocalDate dataAtual = LocalDate.now();

	private JSONArray tipoProduto = new JSONArray();

	@Test
	public void cadastrarPedidoGrupoEmpresaPortaPorta() throws JSONException, JsonParseException, JsonMappingException,
			IOException, ParseException, InterruptedException {

		JSONObject parametros = new JSONObject();

		parametros.put("emailNotaFiscal", "felipe.roque@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.CENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.CENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3)); 

		GrupoEmpresaResponse grupoEmpresaResponse = grupoEmpresaActions.cadastrarGrupoEmpresa(parametros);

		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();

		list_cnpjEmpresas.put(grupoEmpresaRepositoryCustom.consultarCNPJ(grupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(10));

		JSONArray detalhesPedido = pedidosActions.gerarDetalhesPedidosArquivoLoteOuPortaPorta(list_cnpjEmpresas, 30,
				null);

		XSSFWorkbook workbook = pedidosActions
				.getPlanilhaDefaultPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name());

		pedidosActions.setDetalhesPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name(), detalhesPedido,
				workbook);

		MultipartFile xlsPedido = pedidosActions.gerarNovaPlanilhaPedidoLoteOuPortaPorta(workbook);

		PedidoResponse pedidoResponse = pedidosActions.cadastrarPedido(xlsPedido, grupoEmpresaResponse.getId(),
				grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());

		pedidosActions.validarStatusArquivoCargaSTD(pedidoResponse.getNumeroPedido(), StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO.getStatus(), StatusPagamentoEnum.AGUARDANDO_PAGAMENTO.getStatus());

		pedidosActions.validarArquivoCargasDetalhes(TipoEntregaCartao.PORTA_A_PORTA.name(),
				pedidoResponse.getNumeroPedido(), detalhesPedido);

		pedidosActions.validarCargasBeneficios(pedidoResponse.getNumeroPedido(),
				StatusCargaBeneficioEnum.PEDIDO_CONFIRMADO.getStatus());

	}

	@Test
	public void cadastrarPedidoGrupoEmpresaLote() throws JSONException, JsonParseException, JsonMappingException,
			IOException, ParseException, InterruptedException {

		JSONObject parametros = new JSONObject();

		parametros.put("emailNotaFiscal", "felipe.roque@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.CENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 20.1);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.CENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.RH.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3)); 

		GrupoEmpresaResponse grupoEmpresaResponse = grupoEmpresaActions.cadastrarGrupoEmpresa(parametros);

		ArrayList<JSONObject> unidadesEntregas = unidadeEntregaActions
				.gerarUnidadesEntregas(grupoEmpresaResponse.getId(), 2, null);

		XSSFWorkbook workbookUnidades = unidadeEntregaActions.getPlanilhaDefaultUnidadesEntregas();

		unidadeEntregaActions.setUnidadesEntregasPlanilhaDefault(unidadesEntregas, workbookUnidades);

		MultipartFile xlsUnidadesEntregas = unidadeEntregaActions.gerarNovaPlanilhaUnidadesEntregas(workbookUnidades);

		UnidadeEntregaResponse unidadeEntregaResponse = unidadeEntregaActions.cadastrarUnidadesEntrega(
				grupoEmpresaResponse.getId(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario(), xlsUnidadesEntregas);

		unidadeEntregaActions.validarProcessamentoArquivoUnidadeEntrega(
				StatusArquivoUnidadeEntregaEnum.PROCESSAMENTO_CONCLUIDO.status,
				unidadeEntregaResponse.getIdArquivoUnidadeEntrega());

		unidadeEntregaActions.validarDetalhesArquivoUnidadeEntregas(unidadeEntregaResponse.getIdArquivoUnidadeEntrega(),
				unidadesEntregas);

		unidadeEntregaActions.validarQuantidadeUnidadesEntregasGrupoEStatus(grupoEmpresaResponse.getId(),
				unidadesEntregas.size(), UnidadeEntregaEnum.ATIVA.status);

		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();

		list_cnpjEmpresas.put(grupoEmpresaRepositoryCustom.consultarCNPJ(grupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(7));

		JSONArray detalhesPedido = pedidosActions.gerarDetalhesPedidosArquivoLoteOuPortaPorta(list_cnpjEmpresas, 30,
				unidadesEntregas);

		XSSFWorkbook workbookPedido = pedidosActions
				.getPlanilhaDefaultPedidoLoteOuPortaPorta(TipoEntregaCartao.RH.name());

		pedidosActions.setDetalhesPedidoLoteOuPortaPorta(TipoEntregaCartao.RH.name(), detalhesPedido, workbookPedido);

		MultipartFile xlsPedido = pedidosActions.gerarNovaPlanilhaPedidoLoteOuPortaPorta(workbookPedido);

		PedidoResponse pedidoResponse = pedidosActions.cadastrarPedido(xlsPedido, grupoEmpresaResponse.getId(),
				grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());

		pedidosActions.validarStatusArquivoCargaSTD(pedidoResponse.getNumeroPedido(), StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO.getStatus(), StatusPagamentoEnum.AGUARDANDO_PAGAMENTO.getStatus());

		pedidosActions.validarArquivoCargasDetalhes(TipoEntregaCartao.RH.name(), pedidoResponse.getNumeroPedido(),
				detalhesPedido);

		pedidosActions.validarCargasBeneficios(pedidoResponse.getNumeroPedido(),
				StatusCargaBeneficioEnum.PEDIDO_CONFIRMADO.getStatus());

	}
	
	@Test
	public void cancelarPedidoGrupoCentralizado() throws JSONException, JsonParseException, JsonMappingException, IOException, ParseException, InterruptedException {
		
		JSONObject parametros = new JSONObject();

		parametros.put("emailNotaFiscal", "felipe.roque@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.CENTRALIZADO.name()); // Pré-pago
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); // Pré-pago
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 20.1);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.CENTRALIZADO.name()); // centralizado
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); // com nome
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); // web
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); // porta-porta
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); // habilitada
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); // boleto
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); // vale-refeicao
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3)); 

		GrupoEmpresaResponse grupoEmpresaResponse = grupoEmpresaActions.cadastrarGrupoEmpresa(parametros);

		HashMap<String, LocalDate> list_cnpjEmpresas = new HashMap<String, LocalDate>();

		list_cnpjEmpresas.put(grupoEmpresaRepositoryCustom.consultarCNPJ(grupoEmpresaResponse.getIdPessoa().intValue()),
				dataAtual.plusDays(7));

		JSONArray detalhesPedido = pedidosActions.gerarDetalhesPedidosArquivoLoteOuPortaPorta(list_cnpjEmpresas, 10,
				null);

		XSSFWorkbook workbook = pedidosActions
				.getPlanilhaDefaultPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name());

		pedidosActions.setDetalhesPedidoLoteOuPortaPorta(TipoEntregaCartao.PORTA_A_PORTA.name(), detalhesPedido,
				workbook);

		MultipartFile xlsPedido = pedidosActions.gerarNovaPlanilhaPedidoLoteOuPortaPorta(workbook);

		PedidoResponse pedidoResponse = pedidosActions.cadastrarPedido(xlsPedido, grupoEmpresaResponse.getId(),
				grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());

		pedidosActions.validarStatusArquivoCargaSTD(pedidoResponse.getNumeroPedido(), StatusArquivoEnum.PROCESSAMENTO_CONCLUIDO.getStatus(), StatusPagamentoEnum.AGUARDANDO_PAGAMENTO.getStatus());

		pedidosActions.validarArquivoCargasDetalhes(TipoEntregaCartao.PORTA_A_PORTA.name(),
				pedidoResponse.getNumeroPedido(), detalhesPedido);

		pedidosActions.validarCargasBeneficios(pedidoResponse.getNumeroPedido(),
				StatusCargaBeneficioEnum.PEDIDO_CONFIRMADO.getStatus());
		
		pedidosActions.cancelarPedidoCentralizado(pedidoResponse.getNumeroPedido(), grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());
		
		pedidosActions.validarStatusArquivoCargaSTD(pedidoResponse.getNumeroPedido(), StatusArquivoEnum.CANCELADO.getStatus(), StatusPagamentoEnum.AGUARDANDO_PAGAMENTO.getStatus());
		
		pedidosActions.validarCargasBeneficios(pedidoResponse.getNumeroPedido(),
				StatusCargaBeneficioEnum.PEDIDO_CANCELADO.getStatus());
		
	}
}
