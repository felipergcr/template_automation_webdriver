package br.com.conductor.templateAutomation.integrado.report;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.log4j.Logger;

import br.com.conductor.templateAutomation.config.Property;

public class Log {

	private static final Logger aLogger = Logger.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());
	private static final SimpleDateFormat aFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	private static void mensagem(String pMensagemFinal) {
		aLogger.info(pMensagemFinal);
	}

	private static void mensagemDebug(String pMensagemFinal) {
		aLogger.debug(pMensagemFinal);
	}

	public static void erro(String pMensagem) {
		try {
			String vMensagemFinal = getCabecalho("ERRO") + pMensagem;
			mensagem(vMensagemFinal);
		} catch (Exception e) {
		}
	}

	public static void debug(String pMensagem) {
		try {
			String vMensagemFinal = getCabecalho("DEBUG") + pMensagem;
			mensagemDebug(vMensagemFinal);
		} catch (Exception e) {
		}
	}

	public static void info(String pMensagem) {
		try {
			mensagem(getCabecalho("INFO") + pMensagem);
		} catch (Exception e) {
		}
	}

	public static void alerta(String pMensagem) {
		try {
			mensagem(getCabecalho("ALERTA") + pMensagem);
		} catch (Exception e) {
		}
	}

	private static String getCabecalho(String pTipo) {
		return String.format("[%s] [%s] [%s] ", pTipo, aFormat.format(Calendar.getInstance().getTime()),
				Property.EMISSOR);
	}

	public static void excecao(Exception vExcecao) {
		if (vExcecao != null) {
			StringBuilder vBuilder = new StringBuilder();
			vBuilder.append("[EXCECAO OCORRIDA]: " + vExcecao.getMessage());
			vBuilder.append('\n');
			vBuilder.append('\n');
			vBuilder.append('\n' + LogError.extrairStackTrace(vExcecao));
			vBuilder.append('\n');
			erro(vBuilder.toString());
		}
	}

}
