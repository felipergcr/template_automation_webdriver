package br.com.conductor.templateAutomation.integrado.tests.E2E;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.conductor.templateAutomation.integrado.tests.BaseTest;
import br.com.conductor.templateAutomation.integrado.tests.IT.GrupoEmpresaTest;
import br.com.conductor.templateAutomation.integrado.tests.IT.PedidosTest;
import br.com.conductor.templateAutomation.integrado.tests.IT.UnidadeEntregasTest;

@RunWith(Suite.class)
@Suite.SuiteClasses(
		
		{ 	
			LimiteCreditoE2ETest.class,
			PedidoE2ETest.class,
			GrupoEmpresaTest.class,
			PedidosTest.class,
			UnidadeEntregasTest.class
		})

public class SuitesE2ETest extends BaseTest {
}
