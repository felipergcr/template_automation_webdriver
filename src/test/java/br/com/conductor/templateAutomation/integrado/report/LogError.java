package br.com.conductor.templateAutomation.integrado.report;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

import org.junit.Assert;

public class LogError {

	private static String erro;

	public static String extrairStackTrace(Exception e) {
		StackTraceElement[] stack = e.getStackTrace();
		String exception = "";
		for (StackTraceElement s : stack) {
			exception = exception + s.toString() + "\n\t\t";
		}
		return exception;
	}

	public static String getErro() {
		return erro;
	}

	public static void setErro(String erro) {
		LogError.erro = erro;
	}

	public static void assertFail(String message) {
		LogError.setErro(message);
		Assert.fail(message);
	}

	public static String encodeFileToBase64Binary(String fileName) {
		File file = null;
		byte[] bytes = null;
		byte[] encoded = null;
		String encodedString = "";
		try {
			file = new File(fileName);
			bytes = loadFile(file);
			encoded = Base64.getEncoder().encode(bytes);
			encodedString = new String(encoded);
			return encodedString;
		} catch (Exception ex) {
			ex.printStackTrace();
			LogReport.fail("Erro ao capturar documento.");
			return "Erro ao capturar documento";
		}
	}

	@SuppressWarnings("resource")
	public static byte[] loadFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);

		long length = file.length();
		if (length > Integer.MAX_VALUE) {
			// File is too large
		}
		byte[] bytes = new byte[(int) length];

		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}

		if (offset < bytes.length) {
			throw new IOException("Could not completely read file " + file.getName());
		}

		is.close();
		return bytes;
	}

}
