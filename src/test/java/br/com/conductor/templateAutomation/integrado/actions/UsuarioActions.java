package br.com.conductor.templateAutomation.integrado.actions;

import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;

import br.com.conductor.templateAutomation.config.Request;
import br.com.conductor.templateAutomation.controleAcesso.domain.Usuarios;
import br.com.conductor.templateAutomation.domain.response.GrupoEmpresaResponse;
import br.com.conductor.templateAutomation.domain.response.PermissoesRhResponse;
import br.com.conductor.templateAutomation.domain.response.SubgrupoEmpresaResponse;
import br.com.conductor.templateAutomation.domain.response.UsuarioResponse;
import br.com.conductor.templateAutomation.enums.PlataformaEnum;
import br.com.conductor.templateAutomation.integrado.report.LogReport;
import br.com.conductor.templateAutomation.service.UsuariosControleAcessoService;
import br.com.conductor.templateAutomation.utils.ConstantesPaths;
import br.com.twsoftware.alfred.cnpj.CNPJ;
import br.com.twsoftware.alfred.cpf.CPF;

@Component
public class UsuarioActions {
	
	@Autowired
	UsuariosControleAcessoService usuariosControleAcessoService;
	
	private Faker faker = new Faker();
	
	public void atualizarSenhausuario(Long id_usuario) {
		
		usuariosControleAcessoService.updateSenha(id_usuario);
	}
	
	public Optional<Usuarios> consultarUsuario(Long id_usuario) {
		
		Optional<Usuarios> usuarios = usuariosControleAcessoService.obterPorId(id_usuario);
		
		return usuarios;
	}
	
	public void hierarquiaAcessosUsuarios(JSONObject body) {
		
		try {
			
			Request request = new Request();

			ResponseEntity<?> t = request.postApi(ConstantesPaths.PATH_USUARIOS + ConstantesPaths.PATH_HIERARQUIA_ACESSOS, null, body, "HIERARQUIA ACESSOS USUARIOS");
			
		} catch (Exception ex) {
			LogReport.fail("ERRO AO CONCEDER PERMISSAO AO USUARIO: <br/>" + ex.getMessage());
		}
		
	}
	
	public UsuarioResponse cadastroUsuarioRH() {
		
		UsuarioResponse usuarioResponse = new UsuarioResponse();
		JSONObject usuario = new JSONObject();
		
		try {
			
			usuario.put("nome", faker.name().firstName());
			usuario.put("cpf", CPF.gerar());
			usuario.put("cnpj", CNPJ.gerar());
			usuario.put("email", "felipe.roque@conductor.com.br");
			usuario.put("plataforma", PlataformaEnum.RH.getTipo());
			
			Request request = new Request();

			ResponseEntity<?> t = request.postApi(ConstantesPaths.PATH_USUARIOS, null, usuario, "CADASTRO USUARIOS");
			
			if (HttpStatus.CREATED.equals(t.getStatusCode())) {

				ObjectMapper mapper = new ObjectMapper();
				usuarioResponse = mapper.readValue(t.getBody().toString(), UsuarioResponse.class);
			}
			
		} catch (Exception ex) {
			LogReport.fail("Message Error: <br/>" + ex.getMessage());
		}
		return usuarioResponse;
	}
	
}
