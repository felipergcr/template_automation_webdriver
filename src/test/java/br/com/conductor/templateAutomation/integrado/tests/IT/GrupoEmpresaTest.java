package br.com.conductor.templateAutomation.integrado.tests.IT;

import java.time.LocalDate;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.conductor.templateAutomation.domain.response.GrupoEmpresaResponse;
import br.com.conductor.templateAutomation.domain.response.HierarquiaOrganizacionalGrupoResponse;
import br.com.conductor.templateAutomation.domain.response.SubgrupoEmpresaResponse;
import br.com.conductor.templateAutomation.enums.ModeloDeCartao;
import br.com.conductor.templateAutomation.enums.ModeloDePedido;
import br.com.conductor.templateAutomation.enums.SegmentoEmpresaEnum;
import br.com.conductor.templateAutomation.enums.TipoContratoEnum;
import br.com.conductor.templateAutomation.enums.TipoEntregaCartao;
import br.com.conductor.templateAutomation.enums.TipoFaturamentoEnum;
import br.com.conductor.templateAutomation.enums.TipoPagamento;
import br.com.conductor.templateAutomation.enums.TipoPedido;
import br.com.conductor.templateAutomation.enums.TipoProduto;
import br.com.conductor.templateAutomation.enums.TransferenciaProdutos;
import br.com.conductor.templateAutomation.integrado.actions.GrupoEmpresaActions;
import br.com.conductor.templateAutomation.integrado.tests.BaseTest;

public class GrupoEmpresaTest extends BaseTest {

	@Autowired
	private GrupoEmpresaActions grupoEmpresaActions;
	
	private LocalDate dataAtual = LocalDate.now();
	private JSONArray tipoProduto = new JSONArray();

	@Test
	public void cadastroGrupoCentralizado() throws Exception {
		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "felipe.roque@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.CENTRALIZADO.name()); 
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); 
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.CENTRALIZADO.name());
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); 
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); 
		parametros.put("tipoEntrega", TipoEntregaCartao.RH.name()); 
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); 
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); 
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); 
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3)); 
		
		GrupoEmpresaResponse grupoEmpresaResponse = grupoEmpresaActions.cadastrarGrupoEmpresa(parametros);

		HierarquiaOrganizacionalGrupoResponse hierarquiaOrganizacionalGrupoResponse = grupoEmpresaActions
				.consultarHierarquiaOrganizalcionalGrupo(grupoEmpresaResponse.getId(),
						grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());

		grupoEmpresaActions.validarHierarquiaOrganizacional(hierarquiaOrganizacionalGrupoResponse,
				grupoEmpresaResponse.getId());

	}

	@Test
	public void cadastroGrupoDescentralizado() throws Throwable {
		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "felipe.roque@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.DESCENTRALIZADO.name()); 
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); 
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.DESCENTRALIZADO.name());
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); 
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); 
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); 
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); 
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); 
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); 
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3)); 
		
		GrupoEmpresaResponse grupoEmpresaResponse = grupoEmpresaActions.cadastrarGrupoEmpresa(parametros);

		SubgrupoEmpresaResponse SubgrupoEmpresaResponse = grupoEmpresaActions.cadastrarSubgrupoEmpresa(parametros,
				grupoEmpresaResponse.getId());

		HierarquiaOrganizacionalGrupoResponse hierarquiaOrganizacionalGrupoResponse = grupoEmpresaActions
				.consultarHierarquiaOrganizalcionalGrupo(grupoEmpresaResponse.getId(),
						grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());

		grupoEmpresaActions.validarHierarquiaOrganizacional(hierarquiaOrganizacionalGrupoResponse,
				grupoEmpresaResponse.getId());
	}

	@Test
	public void cadastroHierarquiaGrupoDoisSubgruposDuasEmpresas() throws Throwable {

		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "felipe.roque@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.DESCENTRALIZADO.name()); 
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); 
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.DESCENTRALIZADO.name());
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); 
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); 
		parametros.put("tipoEntrega", TipoEntregaCartao.PORTA_A_PORTA.name()); 
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); 
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); 
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); 
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3)); 
		
		GrupoEmpresaResponse grupoEmpresaResponse = grupoEmpresaActions.cadastrarGrupoEmpresa(parametros);

		grupoEmpresaActions.cadastrarSubgrupoEmpresa(parametros, grupoEmpresaResponse.getId());

		HierarquiaOrganizacionalGrupoResponse hierarquiaOrganizacionalGrupoResponse = grupoEmpresaActions
				.consultarHierarquiaOrganizalcionalGrupo(grupoEmpresaResponse.getId(),
						grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());

		grupoEmpresaActions.validarHierarquiaOrganizacional(hierarquiaOrganizacionalGrupoResponse,
				grupoEmpresaResponse.getId());

	}

	@Test
	public void cadastroHierarquiaGrupoDoisSubgruposTresEmpresas() throws Throwable {

		JSONObject parametros = new JSONObject();
		
		parametros.put("emailNotaFiscal", "felipe.roque@conductor.com.br");
		parametros.put("tipoFaturamento", TipoFaturamentoEnum.DESCENTRALIZADO.name()); 
		parametros.put("tipoContrato", TipoContratoEnum.POS.name()); 
		parametros.put("prazoPagamento", 60);
		parametros.put("limiteCreditoDisponivel", 1000.00);
		parametros.put("segmentoEmpresa", SegmentoEmpresaEnum.E1_DIGITAL.name());
		parametros.put("tipoPedido", TipoPedido.DESCENTRALIZADO.name());
		parametros.put("modeloCartao", ModeloDeCartao.COM_NOME.name()); 
		parametros.put("modeloPedido", ModeloDePedido.WEB.name()); 
		parametros.put("tipoEntrega", TipoEntregaCartao.RH.name()); 
		parametros.put("transferencia", TransferenciaProdutos.HABILITADA.name()); 
		parametros.put("tipoPagamento", TipoPagamento.BOLETO.name()); 
		parametros.put("tipoProduto", tipoProduto.put(TipoProduto.VA.name())); 
		parametros.put("dataVigenciaLimite", dataAtual.plusDays(3)); 
		
		GrupoEmpresaResponse grupoEmpresaResponse = grupoEmpresaActions.cadastrarGrupoEmpresa(parametros);

		SubgrupoEmpresaResponse subgrupoEmpresaResponse = grupoEmpresaActions.cadastrarSubgrupoEmpresa(parametros,
				grupoEmpresaResponse.getId());

		grupoEmpresaActions.cadastrarEmpresa(subgrupoEmpresaResponse.getId());

		HierarquiaOrganizacionalGrupoResponse hierarquiaOrganizacionalGrupoResponse = grupoEmpresaActions
				.consultarHierarquiaOrganizalcionalGrupo(grupoEmpresaResponse.getId(),
						grupoEmpresaResponse.getUsuarios().get(0).getIdUsuario());

		grupoEmpresaActions.validarHierarquiaOrganizacional(hierarquiaOrganizacionalGrupoResponse,
				grupoEmpresaResponse.getId());

	}
}
