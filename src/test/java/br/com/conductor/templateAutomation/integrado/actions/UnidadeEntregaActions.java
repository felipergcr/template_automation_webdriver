package br.com.conductor.templateAutomation.integrado.actions;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.conductor.templateAutomation.config.Request;
import br.com.conductor.templateAutomation.domain.custom.ArquivoUnidadeEntrega;
import br.com.conductor.templateAutomation.domain.custom.ArquivoUnidadeEntregaDetalhe;
import br.com.conductor.templateAutomation.domain.custom.UnidadeEntrega;
import br.com.conductor.templateAutomation.domain.response.UnidadeEntregaResponse;
import br.com.conductor.templateAutomation.enums.StatusArquivoUnidadeEntregaEnum;
import br.com.conductor.templateAutomation.integrado.report.LogReport;
import br.com.conductor.templateAutomation.service.ArquivoUnidadeEntregaDetalheService;
import br.com.conductor.templateAutomation.service.ArquivoUnidadeEntregaService;
import br.com.conductor.templateAutomation.service.UnidadeEntregaService;
import br.com.conductor.templateAutomation.utils.ConstantesPaths;
import br.com.conductor.templateAutomation.utils.GeradorMassas;
import br.com.conductor.templateAutomation.utils.LimparDiretorio;

@Component
public class UnidadeEntregaActions {

	@Autowired
	ArquivoUnidadeEntregaService arquivoUnidadeEntregaService;

	@Autowired
	ArquivoUnidadeEntregaDetalheService arquivoUnidadeEntregaDetalheService;

	@Autowired
	UnidadeEntregaService unidadeEntregaService;
//	UnidadeEntregaRepository unidadeEntregaRepository;

	private GeradorMassas gerar = new GeradorMassas();

	public XSSFWorkbook getPlanilhaDefaultUnidadesEntregas() throws IOException {

		File planilhaUnidadesEntregas = new File(ConstantesPaths.PATH_PLANILHA_DEFAULT_UNIDADES_ENTREGA);

		FileInputStream fileInputStream = new FileInputStream(planilhaUnidadesEntregas);

		XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);

		return workbook;
	}

	public MockMultipartFile gerarNovaPlanilhaUnidadesEntregas(XSSFWorkbook workbook) throws IOException {

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

		workbook.write(outputStream);

		outputStream.close();

		workbook.close();

		File file = gravarNovoArquivoUnidadesEntregas(outputStream,
				"carga_APIROQUE_AutomacaoUnidadesEntregaRoque.xlsx");

		InputStream fromoutputstream = new ByteArrayInputStream(outputStream.toByteArray());

		return new MockMultipartFile(file.getName(), "carga_APIROQUE_AutomacaoUnidadesEntregaRoque.xlsx",
				"multipart/form-data", fromoutputstream);
	}

	public File gravarNovoArquivoUnidadesEntregas(ByteArrayOutputStream bytes, String nomeArquivo) {

		File arquivo = null;

		try {

			LimparDiretorio.limparDiretorio("/tmp");

			String temp = "/tmp/";

			byte[] b = bytes.toByteArray();
			Path path = Paths.get(temp + nomeArquivo);
			Files.write(path, b);

			arquivo = new File(temp + nomeArquivo);

		} catch (Exception e) {

			LogReport.fail("ERRO AO GRAVAR PLANILHA EXCEL: <br/>" + e.getMessage());
		}

		return arquivo;
	}

	public void setUnidadesEntregasPlanilhaDefault(ArrayList<JSONObject> unidadesEntregas, XSSFWorkbook workbook) {

		XSSFSheet sheet = workbook.getSheet("BEN _ Beneficiários");

		XSSFRow row = sheet.createRow(6);

		XSSFCell cell;

		for (int i = 0; i < unidadesEntregas.size(); i++) {

			row = sheet.createRow(6 + i);

			try {

				cell = row.createCell(0);
				cell.setCellValue(unidadesEntregas.get(i).getString("codigoUnidadeEntrega"));

				cell = row.createCell(1);
				cell.setCellValue(unidadesEntregas.get(i).getString("logradouro"));

				cell = row.createCell(2);
				cell.setCellValue(unidadesEntregas.get(i).getString("endereco"));

				cell = row.createCell(3);
				cell.setCellValue(unidadesEntregas.get(i).getString("enderecoNumero"));

				cell = row.createCell(4);
				cell.setCellValue(unidadesEntregas.get(i).getString("enderecoComplemento"));

				cell = row.createCell(5);
				cell.setCellValue(unidadesEntregas.get(i).getString("bairro"));

				cell = row.createCell(6);
				cell.setCellValue(unidadesEntregas.get(i).getString("cidade"));

				cell = row.createCell(7);
				cell.setCellValue(unidadesEntregas.get(i).getString("uf"));

				cell = row.createCell(8);
				cell.setCellValue(unidadesEntregas.get(i).getString("cep"));

				cell = row.createCell(9);
				cell.setCellValue(unidadesEntregas.get(i).getString("nomeResponsavel1"));

				cell = row.createCell(10);
				cell.setCellValue(unidadesEntregas.get(i).getString("tipoDocumentoResponsavel1"));

				cell = row.createCell(11);
				cell.setCellValue(unidadesEntregas.get(i).getString("documentoResponsavel1"));

				cell = row.createCell(12);
				cell.setCellValue(unidadesEntregas.get(i).getString("telefoneResponsavel1"));

				cell = row.createCell(13);
				cell.setCellValue(unidadesEntregas.get(i).getString("nomeResponsavel2"));

				cell = row.createCell(14);
				cell.setCellValue(unidadesEntregas.get(i).getString("tipoDocumentoResponsavel2"));

				cell = row.createCell(15);
				cell.setCellValue(unidadesEntregas.get(i).getString("documentoResponsavel2"));

				cell = row.createCell(16);
				cell.setCellValue(unidadesEntregas.get(i).getString("telefoneResponsavel2"));

			} catch (Exception e) {

				LogReport.fail("ERRO AO PREENCHER A PLANILHA EXCEL: <br/>" + e.getMessage());
			}
		}
	}

	public ArrayList<JSONObject> gerarUnidadesEntregas(Long id_GrupoEmpresa, Integer quantidade, String codigoUnidade)
			throws JSONException {

		ArrayList<JSONObject> unidadesEntregas = new ArrayList<JSONObject>();

		for (int i = 0; i < quantidade; i++) {

			unidadesEntregas.add(gerar.gerarUnidadeEntregaGenericaRhUtils(id_GrupoEmpresa, codigoUnidade));
		}

		return unidadesEntregas;
	}

	public UnidadeEntregaResponse cadastrarUnidadesEntrega(Long id_GrupoEmpresa, Long id_Usuario,
			MultipartFile xlsUnidadesEntregas)
			throws JsonParseException, JsonMappingException, IOException, JSONException {

		UnidadeEntregaResponse unidadeEntregaResponse = new UnidadeEntregaResponse();

		try {

			String temp = "/tmp/";

			File file = new File(temp + xlsUnidadesEntregas.getName());

			MultiValueMap<String, String> params = new LinkedMultiValueMap<>();

			params.add("idGrupoEmpresa", id_GrupoEmpresa.toString());
			params.add("idUsuario", id_Usuario.toString());

			Request request = new Request();

			ResponseEntity<?> t = request.postFileApi(
					ConstantesPaths.PATH_UNIDADES_ENTREGA + ConstantesPaths.PATH_UPLOAD, params, file,
					"ARQUIVO UNIDADES ENTREGA");

			if (HttpStatus.CREATED.equals(t.getStatusCode())) {

				ObjectMapper mapper = new ObjectMapper();

				unidadeEntregaResponse = mapper.readValue(t.getBody().toString(), UnidadeEntregaResponse.class);
			}

		} catch (Exception e) {

			LogReport.fail("ERRO AO CADASTRAR UNIDADES ENTREGA: <br/>" + e.getMessage());
		}

		return unidadeEntregaResponse;
	}

	public void validarProcessamentoArquivoUnidadeEntrega(Integer status, Long idArquivoUnidadeEntrega)
			throws InterruptedException {

		Optional<ArquivoUnidadeEntrega> arquivoUnidadeEntrega = arquivoUnidadeEntregaService
				.obterPorId(idArquivoUnidadeEntrega);

		while (!arquivoUnidadeEntrega.get().getStatus().equals(status) && (arquivoUnidadeEntrega.get().getStatus()
				.equals(StatusArquivoUnidadeEntregaEnum.RECEBIDO.status)
				|| arquivoUnidadeEntrega.get().getStatus().equals(StatusArquivoUnidadeEntregaEnum.IMPORTADO.status))) {

			arquivoUnidadeEntrega = arquivoUnidadeEntregaService
					.obterPorId(idArquivoUnidadeEntrega);
		}

		LogReport.passFail(arquivoUnidadeEntrega.get().getStatus().equals(status),
				"STATUS ARQUIVO UNIDADE ENTREGA ESPERADO: " + status
						+ "<br/> STATUS ARQUIVO UNIDADE ENTREGA RETORNADO: " + arquivoUnidadeEntrega.get().getStatus());
	}

	public ArrayList<JSONObject> atualizarUnidadesEntregas(Long id_GrupoEmpresa, Long idArquivoUnidadeEntrega)
			throws JSONException, IOException {

		ArrayList<JSONObject> unidadesAtualizadas = new ArrayList<JSONObject>();

		Optional<List<ArquivoUnidadeEntregaDetalhe>> detalhesArquivo = arquivoUnidadeEntregaDetalheService
				.obterPorIdArquivoUE(idArquivoUnidadeEntrega);

		for (ArquivoUnidadeEntregaDetalhe detalhe : detalhesArquivo.get()) {

			unidadesAtualizadas
					.add(gerar.gerarUnidadeEntregaGenericaRhUtils(id_GrupoEmpresa, detalhe.getCodigoUnidadeEntrega()));
		}

		return unidadesAtualizadas;
	}

	public void validarDetalhesArquivoUnidadeEntregas(Long idArquivoUnidadeEntrega,
			ArrayList<JSONObject> unidadesEntregas) throws JSONException {

		Optional<List<ArquivoUnidadeEntregaDetalhe>> detalhesArquivo = arquivoUnidadeEntregaDetalheService
				.obterPorIdArquivoUE(idArquivoUnidadeEntrega);

		for (JSONObject unidade : unidadesEntregas) {

			for (ArquivoUnidadeEntregaDetalhe detalhe : detalhesArquivo.get()) {

				if (detalhe.getCodigoUnidadeEntrega().equals(unidade.getString("codigoUnidadeEntrega"))) {

					LogReport.passFail(detalhe.getBairro().equals(unidade.getString("bairro").toUpperCase()),
							"BAIRRO ESPERADO: " + unidade.getString("bairro").toUpperCase() + "<br/> BAIRRO RETORNADO: "
									+ detalhe.getBairro());
					LogReport.passFail(detalhe.getCep().equals(unidade.getString("cep")),
							"CEP ESPERADO: " + unidade.getString("cep") + "<br/> CEP RETORNADO: " + detalhe.getCep());
					LogReport.passFail(detalhe.getCidade().equals(unidade.getString("cidade").toUpperCase()),
							"CIDADE ESPERADO: " + unidade.getString("cidade").toUpperCase() + "<br/> CIDADE RETORNADO: "
									+ detalhe.getCidade());
					LogReport.passFail(
							detalhe.getDocumentoResponsavel1().equals(unidade.getString("documentoResponsavel1")),
							"DOCUMENTO RESPONSAVEL 1 ESPERADO: " + unidade.getString("documentoResponsavel1")
									+ "<br/> DOCUMENTO RESPONSAVEL 1 RETORNADO: " + detalhe.getDocumentoResponsavel1());
					LogReport.passFail(
							detalhe.getDocumentoResponsavel2().equals(unidade.getString("documentoResponsavel2")),
							"DOCUMENTO RESPONSAVEL 2 ESPERADO: " + unidade.getString("documentoResponsavel2")
									+ "<br/> DOCUMENTO RESPONSAVEL 2 RETORNADO: " + detalhe.getDocumentoResponsavel2());
					LogReport.passFail(detalhe.getEndereco().equals(unidade.getString("endereco").toUpperCase()),
							"ENDERECO ESPERADO: " + unidade.getString("endereco").toUpperCase()
									+ "<br/> ENDERECO RETORNADO: " + detalhe.getEndereco());
					LogReport.passFail(
							detalhe.getEnderecoComplemento()
									.equals(unidade.getString("enderecoComplemento").toUpperCase()),
							"ENDERECO COMPLEMENTO ESPERADO: " + unidade.getString("enderecoComplemento").toUpperCase()
									+ "<br/> ENDERECO COMPLEMENTO RETORNADO: " + detalhe.getEnderecoComplemento());
					LogReport.passFail(detalhe.getEnderecoNumero().equals(unidade.getString("enderecoNumero")),
							"ENDERECO NUMERO ESPERADO: " + unidade.getString("enderecoNumero")
									+ "<br/> ENDERECO NUMERO RETORNADO: " + detalhe.getEnderecoNumero());
					LogReport.passFail(detalhe.getLogradouro().equals(unidade.getString("logradouro").toUpperCase()),
							"LOGRADOURO ESPERADO: " + unidade.getString("logradouro").toUpperCase()
									+ "<br/> LOGRADOURO RETORNADO: " + detalhe.getLogradouro());
					LogReport.passFail(
							detalhe.getNomeResponsavel1().equals(unidade.getString("nomeResponsavel1").toUpperCase()),
							"NOME RESPONSAVEL 1 ESPERADO: " + unidade.getString("nomeResponsavel1").toUpperCase()
									+ "<br/> NOME RESPONSAVEL 1 RETORNADO: " + detalhe.getNomeResponsavel1());
					LogReport.passFail(
							detalhe.getNomeResponsavel2().equals(unidade.getString("nomeResponsavel2").toUpperCase()),
							"NOME RESPONSAVEL 2 ESPERADO: " + unidade.getString("nomeResponsavel2").toUpperCase()
									+ "<br/> NOME RESPONSAVEL 2 RETORNADO: " + detalhe.getNomeResponsavel2());
					LogReport.passFail(detalhe.getStatus().toString().equals(unidade.getString("status")),
							"STATUS ESPERADO: " + unidade.getString("status") + "<br/> STATUS RETORNADO: "
									+ detalhe.getStatus().toString());
					LogReport.passFail(
							detalhe.getTelefoneResponsavel1().equals(unidade.getString("telefoneResponsavel1")),
							"TELEFONE RESPONSAVEL 1 ESPERADO: " + unidade.getString("telefoneResponsavel1")
									+ "<br/> TELEFONE RESPONSAVEL 1 RETORNADO: " + detalhe.getTelefoneResponsavel1());
					LogReport.passFail(
							detalhe.getTelefoneResponsavel2().equals(unidade.getString("telefoneResponsavel2")),
							"TELEFONE RESPONSAVEL 2 ESPERADO: " + unidade.getString("telefoneResponsavel2")
									+ "<br/> TELEFONE RESPONSAVEL 2 RETORNADO: " + detalhe.getTelefoneResponsavel2());
					LogReport.passFail(
							detalhe.getTipoDocumentoResponsavel1()
									.equals(unidade.getString("tipoDocumentoResponsavel1").toUpperCase()),
							"TIPO DOCUMENTO RESPONSAVEL 1 ESPERADO: "
									+ unidade.getString("tipoDocumentoResponsavel1").toUpperCase()
									+ "<br/> TIPO DOCUMENTO RESPONSAVEL 1 ESPERADO: "
									+ detalhe.getTipoDocumentoResponsavel1());
					LogReport.passFail(
							detalhe.getTipoDocumentoResponsavel2()
									.equals(unidade.getString("tipoDocumentoResponsavel2").toUpperCase()),
							"TIPO DOCUMENTO RESPONSAVEL 2 ESPERADO: "
									+ unidade.getString("tipoDocumentoResponsavel2").toUpperCase()
									+ "<br/> TIPO DOCUMENTO RESPONSAVEL 2 RETORNADO: "
									+ detalhe.getTipoDocumentoResponsavel2());
					LogReport.passFail(detalhe.getUf().equals(unidade.getString("uf").toUpperCase()), "UF ESPERADO: "
							+ unidade.getString("uf").toUpperCase() + "<br/> UF RETORNADO: " + detalhe.getUf());
				}
			}
		}
	}

	public void validarQuantidadeUnidadesEntregasGrupoEStatus(Long idGrupoEmpresa, Integer quantidade, Integer status) {

		Optional<List<UnidadeEntrega>> unidades = unidadeEntregaService
				.obterPorIdGrupoEmpresaEStatus(idGrupoEmpresa, status);

		LogReport.passFail(quantidade.equals(unidades.get().size()),
				"QUANTIDADE DE UNIDADES COM STATUS " + status + "ESPERADO: " + quantidade
						+ "<br/> QUANTIDADE DE UNIDADES COM STATUS " + status + "RETORNADO: " + unidades.get().size());
	}
}
