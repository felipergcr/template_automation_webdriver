package br.com.conductor.templateAutomation.integrado.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import br.com.conductor.templateAutomation.config.Webdriver;

public class HomePage extends Webdriver {

	// *********Web Elements*********

	private By HOME = By.id("ud_dropdown");
	private By MEU_PERFIL = By.id("nav_link_userData");
	private By MEU_PERFIL_SAIR = By.id("logout_dropdown_button");

	public HomePage(WebDriver driver) {
		super(driver);
	}

	public void waitHome() {
		waitVisibility(HOME);
	}
	
	public void logout() {
		click(MEU_PERFIL);
		click(MEU_PERFIL_SAIR);
	}

}
