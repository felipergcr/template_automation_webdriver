package br.com.conductor.templateAutomation.integrado.tests.IT;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.conductor.templateAutomation.integrado.tests.BaseTest;

@RunWith(Suite.class)

@Suite.SuiteClasses(
		
		{ 	GrupoEmpresaTest.class, 
			PedidosTest.class, 
			UnidadeEntregasTest.class, 
		})


public class SuitesTest extends BaseTest {
}
