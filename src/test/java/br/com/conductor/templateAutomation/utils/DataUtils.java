
package br.com.conductor.templateAutomation.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import br.com.twsoftware.alfred.object.Objeto;

public class DataUtils {
     
     public static final DateTimeFormatter FORMATO_YYYY_MM_DD = DateTimeFormatter.ofPattern("yyyy-MM-dd");
     
     private static String converteLocalDateParaString(LocalDate localDate, DateTimeFormatter format) {
          
          return localDate.format(format);
     }
     
     public static LocalDate converteStringParaLocalDate(String data) {
          
          if(Objeto.isBlank(data)) 
               return null;

          return LocalDate.parse(data);
     }
     
     public static String localDateParaStringyyyyMMdd(LocalDate localDate) {
          
          return converteLocalDateParaString(localDate, FORMATO_YYYY_MM_DD);
     }

}
