package br.com.conductor.templateAutomation.utils;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import br.com.conductor.templateAutomation.domain.response.GrupoEmpresaResponse;
import br.com.conductor.templateAutomation.integrado.actions.GrupoEmpresaActions;

@Component
public class CadastroGenerico {
	
	@Autowired
	private GrupoEmpresaActions grupoEmpresaActions;
	
	public GrupoEmpresaResponse grupoCentralizadoUtils(JSONObject parametros) throws JsonParseException, JsonMappingException, IOException, JSONException {
		
		return grupoEmpresaActions.cadastrarGrupoEmpresa(parametros);
	}

}
