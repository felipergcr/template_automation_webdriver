package br.com.conductor.templateAutomation.utils;

import org.apache.commons.lang3.RandomStringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.conductor.templateAutomation.integrado.report.LogReport;

public class ConvertGenericUtil {

	public static String convertObjectToString(Object objeto) {
		
		ObjectMapper mapper = new ObjectMapper();
		String body = null;
		
		try {
			body = mapper.writeValueAsString(objeto);
		} catch (JsonProcessingException e) {
			
			LogReport.fail("ERRO AO CONVERTER OBJETO: " + e.getMessage());
		} 
		return body;
	}

	public static String converterTextoSemCaracteres(String text) {

		return text.replaceAll("[ãâàáä]", "a").replaceAll("[êèéë]", "e").replaceAll("[îìíï]", "i")
				.replaceAll("[õôòóö]", "o").replaceAll("[ûúùü]", "u").replaceAll("[ÃÂÀÁÄ]", "A")
				.replaceAll("[ÊÈÉË]", "E").replaceAll("[ÎÌÍÏ]", "I").replaceAll("[ÕÔÒÓÖ]", "O")
				.replaceAll("[ÛÙÚÜ]", "U").replace('ç', 'c').replace('Ç', 'C').replace('ñ', 'n').replace('Ñ', 'N');
	}

	public static String getAlphabetic(Integer size) {

		return RandomStringUtils.randomAlphabetic(size);

	}
}
