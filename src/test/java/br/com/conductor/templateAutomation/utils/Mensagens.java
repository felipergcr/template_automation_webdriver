package br.com.conductor.templateAutomation.utils;

public class Mensagens {

	public static final String LIMITE_DISPONIVEL_PEDIDO = "seu limite disponível para pedidos é:";
	
	public static final String LIMITE_UTILIZADO_DO_LIMITE_TOTAL = "Você está utilizando %s do seu limite total de %s";
	
	public static final String MENSAGEM_CANCELAMENTO_PEDIDO_PARCIAL = "Tem certeza que deseja cancelar parte do pedido? Esta ação não poderá ser desfeita.";
	
	public static final String MENSAGEM_ALERTA_HORARIO_CANCELAMENTO_PEDIDO_PARCIAL = "(Importante: Se você realizar o cancelamento após às 23h - Horário de Brasília, será cobrado o valor dos cartões solicitados)";
	
	public static final String MENSAGEM_PEDIDO_CANCELADO = "(pedido cancelado)";
	
	public static final String MENSAGEM_PEDIDO_NEGATIVO = "entre em contato com comercial@benvisavale.com.br";
	
	public static final String MENSAGEM_CANCELAMENTO_PEDIDO_CENTRALIZADO = "Tem certeza que deseja cancelar o pedido que está sendo processado?";
	
	public static final String MENSAGEM_ESCOLHER_GRUPO_DESEJA_ACESSAR = "Escolha o grupo que deseja acessar:";
	
}
