package br.com.conductor.templateAutomation.utils;

public class ConstantesPaths {

	public static final String PATH_GRUPO_EMPRESAS = "/grupos-empresas";
	
	public static final String PATH_USUARIOS = "/usuarios";
	
	public static final String PATH_ACESSOS = "/acessos";

	public static final String PATH_HIERARQUIA_ORGANIZACIONAL = "/hierarquia-organizacional";

	public static final String PATH_SUBGRUPO_EMPRESAS = "/subgrupos-empresas";

	public static final String PATH_EMPRESAS = "/empresas";

	public static final String PATH_UPLOAD = "/upload";

	public static final String PATH_UNIDADES_ENTREGA = "/unidades-entrega";
	
    public static final String PATH_PEDIDOS = "/pedidos";
    
    public static final String PATH_CANCELAR = "/cancelar";
    
    public static final String PATH_LIMITE_CREDITO = "/limite-credito";
    
    public static final String PATH_PERMISSOES = "/permissoes";
    
    public static final String PATH_HIERARQUIA_ACESSOS = "/hierarquia-acessos";

	// Planilhas
    public static final String PATH_PLANILHA_DEFAULT_PEDIDO_PORTA_PORTA = "src/test/resources/arquivos/Planilha_Default_Pedido_Porta_Porta.xlsx";
    public static final String PATH_PLANILHA_DEFAULT_PEDIDO_LOTE = "src/test/resources/arquivos/Planilha_Default_Pedido_Lote.xlsx";
    public static final String PATH_PLANILHA_DEFAULT_UNIDADES_ENTREGA = "src/test/resources/arquivos/Planilha_Default_Unidades_Entregas.xlsx";
}
