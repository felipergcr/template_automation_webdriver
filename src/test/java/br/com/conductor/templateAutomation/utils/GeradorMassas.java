package br.com.conductor.templateAutomation.utils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Objects;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.github.javafaker.Faker;

import br.com.twsoftware.alfred.cnpj.CNPJ;
import br.com.twsoftware.alfred.cpf.CPF;

public class GeradorMassas {

	private static Faker faker = new Faker();

	private LocalDateTime dataAtual = LocalDateTime.now();

	public JSONObject condicoesGrupoEmpresa() throws JSONException {

		JSONObject condicoes = new JSONObject();

		condicoes.put("valorMinimoVA", BigDecimal.valueOf(20.01));
		condicoes.put("valorMinimoVR", BigDecimal.valueOf(20.01));
		condicoes.put("taxaAdministracao", BigDecimal.valueOf(20.01));
		condicoes.put("taxaEntrega", BigDecimal.valueOf(20.01));
		condicoes.put("taxaDisponibilizacaoCredito", BigDecimal.valueOf(20.01));
		condicoes.put("taxaEmissaoCartao", BigDecimal.valueOf(20.01));
		condicoes.put("taxaReemissaoCartao", BigDecimal.valueOf(20.01));

		return condicoes;
	}

	public JSONObject empresaMatrizGrupo() throws JSONException {

		JSONObject empresaMatriz = new JSONObject();
		GeradorMassas gerar = new GeradorMassas();

		empresaMatriz.put("descricao", "Ramo Alimenticio");
		empresaMatriz.put("cnpj", CNPJ.gerar());
		empresaMatriz.put("razaoSocial", faker.name().lastName() + " LTDA");
		empresaMatriz.put("dataContrato", LocalDate.now().toString());
		empresaMatriz.put("emailContato", "samuel.severino.ext@conductor.com.br");
		empresaMatriz.put("dataNascimentoContato", LocalDate.now().toString());
		empresaMatriz.put("nomeExibicao", faker.name().firstName() + " Exibicao");
		empresaMatriz.put("nomeFantasia", faker.name().firstName() + " Fantasia");
		empresaMatriz.put("banco", new Long(3));
		empresaMatriz.put("agencia", "0001");
		empresaMatriz.put("contaCorrente", 0102);
		empresaMatriz.put("dvContaCorrente", "05");
		empresaMatriz.put("nomeContato", faker.name().firstName() + " Test");
		empresaMatriz.put("dddTelContato", 83);
		empresaMatriz.put("numTelContato", "991949559");
		empresaMatriz.put("telefone", telefone());
		empresaMatriz.put("endereco", endereco());
		empresaMatriz.put("contatos", gerar.contatosGrupo());

		return empresaMatriz;
	}
	
	public JSONArray contatosGrupo() throws JSONException {
		
		JSONArray contatos = new JSONArray();
		JSONObject contato = new JSONObject();
		
		contato.put("nome", faker.name().lastName() + "Contato 1");
		contato.put("cpf", CPF.gerar());
		contato.put("dataNascimento", LocalDate.now());
		contato.put("email", "felipe.roque@conductor.com.br");
		contato.put("dddTel", 83);
		contato.put("numTel", "953629457");
		contatos.put(contato);
		
		return contatos;
	}

	public JSONObject telefone() throws JSONException {

		JSONObject telefone = new JSONObject();

		telefone.put("ddd", 83);
		telefone.put("telefone", 36625026);
		telefone.put("ramal", 6564);

		return telefone;
	}

	public JSONObject endereco() throws JSONException {

		JSONObject endereco = new JSONObject();

		endereco.put("logradouro", "Rua Angatuba");
		endereco.put("numero", 15);
		endereco.put("complemento", RandomStringUtils.randomAlphabetic(6));
		endereco.put("cep", "58053012");
		endereco.put("bairro", faker.address().streetName());
		endereco.put("cidade", faker.address().cityName());
		endereco.put("uf", "SP");

		return endereco;
	}

	public JSONObject gerarUnidadeEntregaGenericaRhUtils(Long id_GrupoEmpresa, String codigoUnidadeEntrega)
			throws JSONException {

		JSONObject unidadeEntrega = new JSONObject();

		if (Objects.isNull(codigoUnidadeEntrega)) {

			unidadeEntrega.put("codigoUnidadeEntrega", "UE" + faker.number().numberBetween(1, 999999999));

		} else {

			unidadeEntrega.put("codigoUnidadeEntrega", codigoUnidadeEntrega);
		}

		unidadeEntrega.put("idGrupoEmpresa", id_GrupoEmpresa);

		unidadeEntrega.put("dataStatus", dataAtual);

		unidadeEntrega.put("dataRegistro", dataAtual);

		unidadeEntrega.put("status", 1);

		unidadeEntrega.put("logradouro", "RUA");

		unidadeEntrega.put("endereco", ConvertGenericUtil.getAlphabetic(15));

		unidadeEntrega.put("enderecoNumero", RandomStringUtils.randomNumeric(4));

		unidadeEntrega.put("enderecoComplemento", ConvertGenericUtil.getAlphabetic(6));

		unidadeEntrega.put("bairro", ConvertGenericUtil.getAlphabetic(15));

		unidadeEntrega.put("cidade", ConvertGenericUtil.getAlphabetic(15));

		unidadeEntrega.put("uf", "PB");

		unidadeEntrega.put("cep", String.valueOf(EntityGenericUtil.getLong(8)));

		unidadeEntrega.put("nomeResponsavel1", ConvertGenericUtil.getAlphabetic(5) + " Responsavel Um");

		unidadeEntrega.put("tipoDocumentoResponsavel1", "RG");

		unidadeEntrega.put("documentoResponsavel1", String.valueOf(faker.number().numberBetween(0000001, 999999999)));

		unidadeEntrega.put("telefoneResponsavel1",
				"219" + String.valueOf(faker.number().numberBetween(1000000, 99999999)));

		unidadeEntrega.put("nomeResponsavel2", ConvertGenericUtil.getAlphabetic(5) + " Responsavel Dois");

		unidadeEntrega.put("tipoDocumentoResponsavel2", "CPF");

		unidadeEntrega.put("documentoResponsavel2", CPF.gerar());

		unidadeEntrega.put("telefoneResponsavel2",
				"839" + String.valueOf(faker.number().numberBetween(1000000, 99999999)));

		return unidadeEntrega;
	}

}
