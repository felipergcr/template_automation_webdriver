package br.com.conductor.templateAutomation.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Ignore;

import br.com.twsoftware.alfred.object.Objeto;

@Ignore
public class EntityGenericUtil {

	public static Integer getInteger() {
		return getInteger(0, 999999);
	}

	public static Integer getInteger(Integer start, Integer end) {
		return RandomUtils.nextInt(start, end);
	}

	public static Long getLong() {
		return RandomUtils.nextLong(0, 999999);
	}

	public static Long getLong(int length) {
		long value = RandomUtils.nextLong(1111111111111111111L, 9219999999999999999L);
		return Long.parseLong(String.valueOf(value).substring(0, length));
	}

	public static Float getFloat() {
		return RandomUtils.nextFloat(0F, 999999F);
	}

	public static String getString() {
		return UUID.randomUUID().toString().replace("-", "");
	}

	public static String getString(Integer size) {
		return RandomStringUtils.randomAlphanumeric(size);
	}

	public static BigInteger getBigInteger() {
		return new BigInteger(getInteger().toString());
	}

	public static Date getDate() {
		return new Date();
	}

	public static LocalDateTime getDateTime() {
		return LocalDateTime.now();
	}

	public static BigDecimal getBigDecimal() {
		return new BigDecimal(String.valueOf(RandomUtils.nextDouble(0, 999999)));
	}

	public static Double getDouble() {
		return new Double(String.valueOf(RandomUtils.nextDouble(0, 999999)));
	}

	/**
	 * Método gerador de BigDecimal de acordo com precisão e escala. Se a precisão
	 * não for informada, a escala é ignorada e é retornado um BigDecimal qualquer
	 * Se a precisão for informada, mas a escala não for um valor menor que a
	 * precisão, o valor da escala será a precisão menos uma unidade.
	 * 
	 * @param precision
	 *            Quantidade de algarismos do número
	 * @param scale
	 *            Quantidade de algarismos após separador decimal do número
	 * @return O número BigDecimal gerado
	 */
	public static BigDecimal getBigDecimal(Integer precision, Integer scale) {
		if (Objeto.isBlank(precision) || precision < 1) {
			return getBigDecimal();
		}

		if (Objeto.notBlank(scale) && scale >= precision) {
			scale = precision - 1;
		}

		String randomNumeric = RandomStringUtils.randomNumeric(precision);
		return new BigDecimal(randomNumeric).divide(BigDecimal.TEN.pow(scale));
	}

	public static LocalDate getLocalDate() {
		return LocalDate.now();
	}

	public static LocalDateTime getLocalDateTime() {
		return LocalDateTime.now();
	}

	public static Boolean getBoolean() {
		return new Random().nextBoolean();
	}

	public static <T> T getEnum(Class<T> clazz) {
		int length = clazz.getEnumConstants().length;
		int nextInt = new Random().nextInt(length);
		return clazz.getEnumConstants()[nextInt];
	}

	public static Object getByType(Class<?> type) {
		if (type == Integer.class || type == int.class) {
			return getInteger();
		} else if (type == BigDecimal.class) {
			return getBigDecimal();
		} else if (type == BigInteger.class) {
			return getBigInteger();
		} else if (type == Date.class) {
			return getDate();
		} else if (type == LocalDateTime.class) {
			return getDateTime();
		} else if (type == LocalDate.class) {
			return getDateTime().toLocalDate();
		} else if (type == LocalTime.class) {
			return getDateTime().toLocalTime();
		} else if (type == Long.class) {
			return getLong();
		} else if (type == String.class) {
			return getString();
		}
		return null;
	}

	public static Integer gerarDdd() {
		List<Integer> dddList = new ArrayList<>();
		Integer dddSelecionado = null;

		dddList.addAll(Arrays.asList(11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22, 24, 27, 28, 31, 32, 33, 34, 35, 37, 38,
				41, 42, 43, 44, 45, 46, 47, 48, 49, 51, 53, 54, 55, 61, 62, 63, 64, 65, 66, 67, 68, 69, 71, 73, 74, 75,
				77, 79, 81, 82, 83, 84, 85, 86, 87, 88, 89, 91, 92, 93, 94, 95, 96, 97, 98, 99));

		dddSelecionado = dddList.stream().skip((int) (dddList.size() * Math.random())).findFirst().get();

		return dddSelecionado;
	}

	public static String prefixo() {
		List<Integer> prefixos = new ArrayList<>();

		prefixos.addAll(Arrays.asList(67, 71, 72, 95, 96, 97, 98, 99, 68, 73, 74, 75, 76, 91, 92, 93, 94, 69, 79, 80,
				81, 82, 83, 84, 85, 86, 87, 88, 89));

		return String.valueOf(prefixos.stream().skip((int) (prefixos.size() * Math.random())).findFirst().get());
	}

	// Gera os primeiros Numeros
	private static String nPrefixo() {
		String digitos = "";
		Random r = new Random();
		for (int i = 0; i < 2; i++)
			digitos += r.nextInt(9);
		return digitos;
	}

	// Gera a Numeracao final baseando-se em numeros aleatorios
	private static String nFinal() {
		String digitos = "";
		Random r = new Random();
		for (int i = 0; i < 4; i++)
			digitos += r.nextInt(9);
		return digitos;
	}

	public static String numeroCelularValido() {
		return ("9" + prefixo() + nPrefixo() + nFinal()).substring(0, 9);
	}

	public static String numeroCelularInvalido() {
		return ("0" + prefixo() + nPrefixo() + nFinal()).substring(0, 9);
	}

	public static String numerotelefoneValido() {
		return (prefixo() + nPrefixo() + nFinal()).substring(0, 8);
	}

	public static String getSexo() {
		List<String> sexo = new ArrayList<>();
		sexo.add("M");
		sexo.add("F");

		return String.valueOf(sexo.stream().skip((int) (sexo.size() * Math.random())).findFirst().get());
	}

	public static String converter(String text) {
		return text.replaceAll("[ãâàáä]", "a").replaceAll("[êèéë&]", "e").replaceAll("[îìíï]", "i")
				.replaceAll("[õôòóö]", "o").replaceAll("[ûúùü]", "u").replaceAll("[ÃÂÀÁÄ]", "A")
				.replaceAll("[ÊÈÉË]", "E").replaceAll("[ÎÌÍÏ]", "I").replaceAll("[ÕÔÒÓÖ]", "O")
				.replaceAll("[ÛÙÚÜ]", "U").replace('ç', 'c').replace('Ç', 'C').replace('ñ', 'n').replace('Ñ', 'N')
				.replaceAll("[^a-zA-Z]", " ");
	}

}