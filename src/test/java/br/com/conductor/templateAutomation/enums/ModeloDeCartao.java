
package br.com.conductor.templateAutomation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ModeloDeCartao {

	COM_NOME("COM_NOME"), SEM_NOME("SEM_NOME");

	private String modelo;
}
