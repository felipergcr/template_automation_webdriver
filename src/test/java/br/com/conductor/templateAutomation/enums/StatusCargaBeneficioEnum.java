
package br.com.conductor.templateAutomation.enums;

import org.junit.Ignore;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Ignore
@Getter
@AllArgsConstructor
public enum StatusCargaBeneficioEnum {

	CARGA_EFETUADOA(0), AGUARDANDO_PAGAMENTO(1), PEDIDO_CONFIRMADO(2), PEDIDO_CANCELADO(3), PEDIDO_PROCESSADO(
			4), CARGA_PROCESSANDO(5), PENDENCIA_LIMITE(6);

	private Integer status;
}
