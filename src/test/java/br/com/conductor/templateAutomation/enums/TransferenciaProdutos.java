
package br.com.conductor.templateAutomation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TransferenciaProdutos {

	HABILITADA("HABILITADA"), DESABILITADA("DESABILITADA");

	private String transferencia;
}
