package br.com.conductor.templateAutomation.enums;

import org.junit.Ignore;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Ignore
@Getter
@AllArgsConstructor
public enum StatusPagamentoEnum {

	AGUARDANDO_PAGAMENTO(0), CONFIRMADO(1), PAGO_PARCIALMENTE(2), NAO_EFETUADO(3), 
	LIMITE_CREDITO_VENCIDO(4), LIMITE_CREDITO_INSUFICIENTE(5);

	private Integer status;
}
