
package br.com.conductor.templateAutomation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SegmentoEmpresaEnum {

	E1_DIGITAL("E1_DIGITAL"),

	E1("E1"),

	E2("E2"),

	E2_POLO("E2_POLO"),

	E3("E3"),

	CORPORATE("CORPORATE"),

	GCB("GCB");

	private String segmento;
}
