
package br.com.conductor.templateAutomation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoFaturamentoEnum {

	CENTRALIZADO(1), DESCENTRALIZADO(0);

	Integer valor;
}
