package br.com.conductor.templateAutomation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UnidadeEntregaEnum {

	ATIVA(1), DESATIVADA(2);

	public Integer status;
}
