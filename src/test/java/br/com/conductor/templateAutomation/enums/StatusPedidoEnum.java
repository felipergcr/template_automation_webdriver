package br.com.conductor.templateAutomation.enums;

import org.junit.Ignore;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Ignore
@Getter
@AllArgsConstructor
public enum StatusPedidoEnum {

	EFETUADO("Efetuado"), PARCIALMENTE_CANCELADO("Parcialmente Cancelado"), CANCELADO("Cancelado");

	private String descricao;
}
