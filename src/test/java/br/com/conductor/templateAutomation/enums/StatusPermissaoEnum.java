package br.com.conductor.templateAutomation.enums;

import org.junit.Ignore;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Ignore
@Getter
@AllArgsConstructor
public enum StatusPermissaoEnum {

     ATIVO("ATIVO"), INATIVO("INATIVO"), BLOQUEADO("BLOQUEADO");
     
     @Getter
     private String descricao;

}
