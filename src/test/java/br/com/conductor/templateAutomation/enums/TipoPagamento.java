
package br.com.conductor.templateAutomation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoPagamento {

	BOLETO("BOLETO", "B"), DEBITO("DEBITO", "D"), TED("TED", "T");

	private String tipo;

	private String valor;
}
