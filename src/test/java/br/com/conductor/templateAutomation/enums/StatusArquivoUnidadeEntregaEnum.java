package br.com.conductor.templateAutomation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusArquivoUnidadeEntregaEnum {

	RECEBIDO(0), IMPORTADO(1), INVALIDADO(2), PROCESSAMENTO_CONCLUIDO(3);

	public Integer status;

}
