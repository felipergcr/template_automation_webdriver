
package br.com.conductor.templateAutomation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoEntregaCartao {

	RH("RH"), PORTA_A_PORTA("PO");

	private String valor;
}
