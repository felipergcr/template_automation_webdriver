package br.com.conductor.templateAutomation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CoresGraficoLimitesEnum {

	VERDE("#2AD178"), CINZA("#979797"), LARANJA("#FF9C00"), VERMELHO("#FF0000");

	private final String cor;
}
