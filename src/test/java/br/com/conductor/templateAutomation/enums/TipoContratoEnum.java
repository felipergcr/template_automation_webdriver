
package br.com.conductor.templateAutomation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoContratoEnum {

	PRE("Pré-Pagamento", 7), POS("Pós-Pagamento", 3);

	private final String descricao;

	private final Integer diasUteis;
}
