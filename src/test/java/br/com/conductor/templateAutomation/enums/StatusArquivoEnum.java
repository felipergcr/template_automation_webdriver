package br.com.conductor.templateAutomation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusArquivoEnum {

	RECEBIDO(0), IMPORTADO(1), INVALIDADO(2), PROCESSAMENTO_CONCLUIDO(3), VALIDADO_PARCIAL(4), CANCELADO(
			5), CANCELADO_PARCIAL(6);

	private Integer status;
}
