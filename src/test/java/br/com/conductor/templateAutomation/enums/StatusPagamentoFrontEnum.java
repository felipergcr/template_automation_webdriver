package br.com.conductor.templateAutomation.enums;

import org.junit.Ignore;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Ignore
@Getter
@AllArgsConstructor
public enum StatusPagamentoFrontEnum{

	AGUARDANDO_PAGAMENTO("Aguardando"), CONFIRMADO("Confirmado"), PAGO_PARCIALMENTE("Pago Parcialmente"), NAO_EFETUADO(
			"Nao Efetuado"), LIMITE_CREDITO_VENCIDO("Limite de Crédito Vencido"), LIMITE_CREDITO_INSUFICIENTE("Limite de Crédito Insuficiente");

	private String descricao;
}
