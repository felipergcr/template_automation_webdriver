
package br.com.conductor.templateAutomation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum FormasPagamento {

	TED_DOC("TED/DOC RH", "Ted ou Doc");

	private final String chave;

	private final String descricao;

}
