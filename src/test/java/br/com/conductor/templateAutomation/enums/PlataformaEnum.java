package br.com.conductor.templateAutomation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PlataformaEnum {

	RH("RH");
	
	private String tipo;
}
