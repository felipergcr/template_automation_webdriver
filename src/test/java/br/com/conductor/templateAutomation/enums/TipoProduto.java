
package br.com.conductor.templateAutomation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TipoProduto {

	VR("VR"), VA("VA");

	private String tipo;
}
