package br.com.conductor.templateAutomation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OrigemComercialEnum {

	Alimentacao(1l, "PAT - Alimentação"), Refeicao(2l, "PAT - Refeição"), RhGerencial(3l, "RH Gerencial");

	@Getter
	private final Long id;

	@Getter
	private final String descricao;
}
