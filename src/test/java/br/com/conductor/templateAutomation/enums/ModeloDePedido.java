package br.com.conductor.templateAutomation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ModeloDePedido {
	WEB("WEB"), TRANSFERENCIA_ARQUIVO("TRANSFERENCIA_ARQUIVO");

	private String modelo;
}
