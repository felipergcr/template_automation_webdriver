package br.com.conductor.templateAutomation.controleAcesso.repository.jpa;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.conductor.templateAutomation.controleAcesso.domain.Usuarios;

@Repository
public interface UsuarioControleAcessoRepository extends JpaRepository<Usuarios, Long> {

	Optional<Usuarios> findById(Long id);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE Usuarios SET senha = PWDENCRYPT('Teste@123') WHERE Id_Usuario = :id", nativeQuery = true)
	void updateSenha(@Param("id") Long id);
}
