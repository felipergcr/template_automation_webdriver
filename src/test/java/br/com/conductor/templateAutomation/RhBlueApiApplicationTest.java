package br.com.conductor.templateAutomation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RhBlueApiApplicationTest {

	public static void main(String[] args) {
		SpringApplication.run(RhBlueApiApplicationTest.class, args).close();
	}

}