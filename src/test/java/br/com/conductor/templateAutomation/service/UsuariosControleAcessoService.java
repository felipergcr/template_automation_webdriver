package br.com.conductor.templateAutomation.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.templateAutomation.controleAcesso.domain.Usuarios;
import br.com.conductor.templateAutomation.controleAcesso.repository.jpa.UsuarioControleAcessoRepository;

@Service
public class UsuariosControleAcessoService {

	@Autowired
	private UsuarioControleAcessoRepository usuarioControleAcessoRepository;
	
	public Optional<Usuarios> obterPorId(final Long id) {

		return usuarioControleAcessoRepository.findById(id);
	}
	
	@Transactional
	public void updateSenha(Long id) {
		
		usuarioControleAcessoRepository.updateSenha(id);
	}
}
