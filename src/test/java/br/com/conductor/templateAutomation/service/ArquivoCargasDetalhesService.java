package br.com.conductor.templateAutomation.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.templateAutomation.domain.custom.ArquivosCargasDetalhes;
import br.com.conductor.templateAutomation.repository.jpa.ArquivoCargasDetalhesRepository;

@Service
public class ArquivoCargasDetalhesService {
	
	@Autowired
	ArquivoCargasDetalhesRepository arquivoCargasDetalhesRepository;
	
	
	public Optional<List<ArquivosCargasDetalhes>> obterPorId(final Long id) {

		return arquivoCargasDetalhesRepository.findByIdArquivoCargas(id);
	}
}
