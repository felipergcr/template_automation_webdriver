
package br.com.conductor.templateAutomation.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.templateAutomation.domain.custom.GrupoEmpresa;
import br.com.conductor.templateAutomation.domain.custom.HierarquiaOrganizacionalGrupo;
import br.com.conductor.templateAutomation.repository.custom.GrupoEmpresaRepositoryImpl;
import br.com.conductor.templateAutomation.repository.jpa.GrupoEmpresaRepository;

@Service
public class GrupoEmpresaService {

	@Autowired
	GrupoEmpresaRepository grupoEmpresaRepository;

	@Autowired
	GrupoEmpresaRepositoryImpl grupoEmpresaRepositoryImpl;

	public Optional<GrupoEmpresa> obterPorId(final Long id) {

		return grupoEmpresaRepository.findById(id);
	}

	public List<HierarquiaOrganizacionalGrupo> consultaHierarquiaOrganizacional(Long id) {

		return grupoEmpresaRepositoryImpl.consultaHierarquiaOrganizacional(id);
	}
	
	public String consultarCNPJ(Integer idPessoa) {
		
		return grupoEmpresaRepositoryImpl.consultarCNPJ(idPessoa);
	}

}
