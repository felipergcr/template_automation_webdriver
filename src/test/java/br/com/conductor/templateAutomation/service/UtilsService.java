package br.com.conductor.templateAutomation.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.templateAutomation.repository.custom.UtilsRepositoryImpl;

@Service
public class UtilsService {
	
	@Autowired
	UtilsRepositoryImpl utilsRepositoryImpl;

	public void inserirEventoExternosPagamentos(BigDecimal valor) {
		
		utilsRepositoryImpl.inserirEventoExternosPagamentos(valor);
	}
	
	public Long getUltimoEventosExternosPagamentos() {
		
		return utilsRepositoryImpl.getUltimoEventosExternosPagamentos();
	}
	
	public void pagamentoBoletoEmitidos(Long id_boleto, Long id_eventopagamento) {
		
		utilsRepositoryImpl.pagamentoBoletoEmitidos(id_boleto, id_eventopagamento);
	}
	
	public void inserirExcecoesLimiteDisponivel(Long idGrupoEmpresa, Long idUsuario) {
		
		utilsRepositoryImpl.inserirExcecoesLimiteDisponivel(idGrupoEmpresa, idUsuario);
	}
	
	public void inserirDataVigenciaVencida(Long idGrupoEmpresa, Long dias) {
		
		utilsRepositoryImpl.inserirDataVigenciaVencida(idGrupoEmpresa, dias);
	}
}
