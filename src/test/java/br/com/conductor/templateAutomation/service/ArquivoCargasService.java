package br.com.conductor.templateAutomation.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.templateAutomation.domain.custom.ArquivoCargas;
import br.com.conductor.templateAutomation.repository.jpa.ArquivoCargasRepository;

@Service
public class ArquivoCargasService {

	@Autowired
	ArquivoCargasRepository arquivoCargasRepository;
	
	public Optional<ArquivoCargas> obterPorId(final Long id) {

		return arquivoCargasRepository.findById(id);
	}
}
