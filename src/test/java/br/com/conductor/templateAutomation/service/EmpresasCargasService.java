package br.com.conductor.templateAutomation.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.templateAutomation.domain.custom.EmpresasCargas;
import br.com.conductor.templateAutomation.repository.jpa.EmpresasCargasRepository;

@Service
public class EmpresasCargasService {

	@Autowired
	EmpresasCargasRepository empresasCargasRepository;
	
	public Optional<EmpresasCargas> obterPorIdEmpresa(final Long idEmpresa) {

		return empresasCargasRepository.findByIdEmpresa(idEmpresa);
	}
	
	public Optional<EmpresasCargas> obterPorIdCargaBeneficio(final Long idCargaBeneficio) {
		
		return empresasCargasRepository.findByIdCargaBeneficio(idCargaBeneficio);
	}
}
