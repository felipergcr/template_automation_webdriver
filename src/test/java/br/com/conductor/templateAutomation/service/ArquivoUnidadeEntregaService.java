package br.com.conductor.templateAutomation.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.templateAutomation.domain.custom.ArquivoUnidadeEntrega;
import br.com.conductor.templateAutomation.repository.jpa.ArquivoUnidadeEntregaRepository;

@Service
public class ArquivoUnidadeEntregaService {
	
	@Autowired
	ArquivoUnidadeEntregaRepository arquivoUnidadeEntregaRepository;
	
	public Optional<ArquivoUnidadeEntrega> obterPorId(final Long id) {

		return arquivoUnidadeEntregaRepository.findById(id);
	}

}
