package br.com.conductor.templateAutomation.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.templateAutomation.domain.custom.UnidadeEntrega;
import br.com.conductor.templateAutomation.repository.jpa.UnidadeEntregaRepository;

@Service
public class UnidadeEntregaService {
	
	@Autowired
	UnidadeEntregaRepository unidadeEntregaRepository;
	
	public Optional<List<UnidadeEntrega>> obterPorIdGrupoEmpresaEStatus(Long idGrupoEmpresa, Integer status) {

		return unidadeEntregaRepository.findByIdGrupoEmpresaAndStatus(idGrupoEmpresa, status);
	}

}
