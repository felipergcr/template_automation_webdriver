package br.com.conductor.templateAutomation.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.templateAutomation.domain.custom.CargaBeneficio;
import br.com.conductor.templateAutomation.repository.jpa.CargaBeneficioRepository;

@Service
public class CargaBeneficioService {
	
	@Autowired
	CargaBeneficioRepository cargaBeneficioRepository;
	
	public List<CargaBeneficio> obterPorIdArquivoCargas(final Long idArquivoCargas) {

		return cargaBeneficioRepository.findByArquivoCargasId(idArquivoCargas);
	}

}
