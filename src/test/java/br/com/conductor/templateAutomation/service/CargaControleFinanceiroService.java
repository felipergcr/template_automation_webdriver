package br.com.conductor.templateAutomation.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.templateAutomation.domain.custom.CargaControleFinanceiro;
import br.com.conductor.templateAutomation.repository.jpa.CargaControleFinanceiroRepository;

@Service
public class CargaControleFinanceiroService {
	
	@Autowired
	CargaControleFinanceiroRepository cargaControleFinanceiroRepository;
	
	public Optional<CargaControleFinanceiro> obterPorId(final Long id) {

		return cargaControleFinanceiroRepository.findById(id);
	}
}
