package br.com.conductor.templateAutomation.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conductor.templateAutomation.domain.custom.ArquivoUnidadeEntregaDetalhe;
import br.com.conductor.templateAutomation.repository.jpa.ArquivoUnidadeEntregaDetalheRepository;

@Service
public class ArquivoUnidadeEntregaDetalheService {

	@Autowired
	ArquivoUnidadeEntregaDetalheRepository arquivoUnidadeEntregaDetalheRepository;
	
	public Optional<List<ArquivoUnidadeEntregaDetalhe>> obterPorIdArquivoUE(final Long idArquivoUE) {

		return arquivoUnidadeEntregaDetalheRepository.findByIdArquivoUE(idArquivoUE);
	}
}
