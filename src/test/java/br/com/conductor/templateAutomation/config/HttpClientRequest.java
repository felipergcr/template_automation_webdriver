package br.com.conductor.templateAutomation.config;

import static java.util.Collections.singletonList;
import static org.springframework.http.CacheControl.noCache;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class HttpClientRequest {

	private final String API = Property.API_BACK;

	public HttpHeaders incializaHeadersAutenticacao() {

		final HttpHeaders headers = new HttpHeaders();
		headers.setAccept(singletonList(APPLICATION_JSON_UTF8));
		headers.setContentType(APPLICATION_JSON_UTF8);
		headers.setCacheControl(noCache());
		return headers;
	}

	public HttpHeaders incializaHeadersArquivoAutenticacao() {
		HttpHeaders headers = incializaHeadersAutenticacao();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		headers.add(HttpHeaders.CONTENT_TYPE, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		headers.add("Content-Disposition", "attachment; filename=report.xlsx");
		return headers;
	}

	public StringBuilder inicializaUri() {

		final StringBuilder sb = new StringBuilder();
		sb.append(API);
		return sb;

	}

	public String uriResource(String path) {

		return this.inicializaUri().append(path).toString();
	}
}
