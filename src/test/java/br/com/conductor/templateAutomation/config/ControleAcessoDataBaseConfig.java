
package br.com.conductor.templateAutomation.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "controleAcessoEntityManagerFactory", transactionManagerRef = "controleAcessoTransactionManager", 
     basePackages = { "br.com.conductor.templateAutomation.controleAcesso.repository" })

public class ControleAcessoDataBaseConfig {

     @Bean(name = "controleAcessoDataSource")
     @ConfigurationProperties(prefix = "spring.controleacesso.datasource")
     public DataSource controleAcessoDataSource() {

          return DataSourceBuilder.create().build();
     }

     @Bean(name = "controleAcessoEntityManagerFactory")
     public LocalContainerEntityManagerFactoryBean controleAcessoEntityManagerFactory(EntityManagerFactoryBuilder builder, @Qualifier("controleAcessoDataSource") DataSource dataSource) {

          return builder.dataSource(dataSource).packages("br.com.conductor.templateAutomation.controleAcesso.domain").persistenceUnit("controleAcesso").build();
     }

     @Bean(name = "controleAcessoTransactionManager")
     public PlatformTransactionManager controleAcessoTransactionManager(@Qualifier("controleAcessoEntityManagerFactory") EntityManagerFactory controleAcessoentityManagerFactory) {

          return new JpaTransactionManager(controleAcessoentityManagerFactory);
     }
}
