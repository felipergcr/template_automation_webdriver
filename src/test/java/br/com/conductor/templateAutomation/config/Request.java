package br.com.conductor.templateAutomation.config;

import java.io.File;
import java.net.URI;
import java.util.Objects;

import org.json.JSONObject;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.conductor.templateAutomation.integrado.report.LogReport;

public class Request {

	private static ResponseEntity<?> response;

	private TestRestTemplate template = new TestRestTemplate();

	private HttpClientRequest httpClientRequest = new HttpClientRequest();

	public ResponseEntity<?> getApi(String url, MultiValueMap<String, String> params, String msgLog) {
		try {

			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(httpClientRequest.uriResource(url))
					.queryParams(params);

			HttpHeaders headers = httpClientRequest.incializaHeadersAutenticacao();

			HttpEntity<?> entity = new HttpEntity<>(headers);

			response = template.exchange(new URI(builder.toUriString()), HttpMethod.GET, entity, String.class);

			LogReport.extentInfo("GET " + msgLog + ":" + response);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return response;
	}

	public ResponseEntity<?> postApi(String url,  MultiValueMap<String, String> params, JSONObject body, String msgLog) {
		try {
			HttpEntity<String> entity;
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(httpClientRequest.uriResource(url));

			if (!Objects.isNull(params)) 
				builder.queryParams(params);
			
			HttpHeaders headers = httpClientRequest.incializaHeadersAutenticacao();
			
			if (!Objects.isNull(body)) {
				 entity = new HttpEntity<>(body.toString(), headers);
			} else {
				 entity = new HttpEntity<>(headers);
			}

			LogReport.extentInfo(msgLog + ":" + body);

			response = template.exchange(new URI(builder.toUriString()), HttpMethod.POST, entity, String.class);

			LogReport.extentInfo("POST " + msgLog + ":" + response);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return response;
	}

	public ResponseEntity<?> postFileApi(String url, MultiValueMap<String, String> params, File file, String msgLog) {
		try {

			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(httpClientRequest.uriResource(url))
					.queryParams(params);

			HttpHeaders headers = httpClientRequest.incializaHeadersArquivoAutenticacao();

			MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();

			body.add("file", new FileSystemResource(file));

			HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(body, headers);

			LogReport.extentInfo(msgLog + ":" + file.getName());

			response = template.exchange(new URI(builder.toUriString()), HttpMethod.POST, entity, String.class);

			LogReport.extentInfo("POST ARQUIVO " + msgLog + ":" + response);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return response;
	}
	
	public ResponseEntity<?> putApi(String url, MultiValueMap<String, String> params, JSONObject body, String msgLog) {
		
		try {
			
			HttpEntity<String> entity;
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(httpClientRequest.uriResource(url));

			if (!Objects.isNull(params)) 
				builder.queryParams(params);
			
			HttpHeaders headers = httpClientRequest.incializaHeadersAutenticacao();
			
			if (!Objects.isNull(body)) {
				 entity = new HttpEntity<>(body.toString(), headers);
			} else {
				 entity = new HttpEntity<>(headers);
			}

			response = template.exchange(new URI(builder.toUriString()), HttpMethod.PUT, entity, String.class);

			LogReport.extentInfo("PUT " + msgLog + ":" + response);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return response;
	}
	
	public ResponseEntity<?> patchApi(String url, MultiValueMap<String, String> params, JSONObject body, String msgLog) {
		try {
			
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(httpClientRequest.uriResource(url));

			if (!Objects.isNull(params)) 
				builder.queryParams(params);
			
			HttpHeaders headers = httpClientRequest.incializaHeadersAutenticacao();
			
			HttpEntity<String> entity = new HttpEntity<>(body.toString(), headers);
			
			LogReport.extentInfo(msgLog + ":" + body);
			
			response = template.exchange(new URI(builder.toUriString()), HttpMethod.PATCH, entity, String.class);
			
			LogReport.extentInfo("PATCH " + msgLog + ":" + response);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return response;
	}
}
