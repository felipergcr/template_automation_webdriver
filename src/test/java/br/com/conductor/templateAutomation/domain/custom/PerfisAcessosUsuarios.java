package br.com.conductor.templateAutomation.domain.custom;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "PERFISACESSOSUSUARIOS")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PerfisAcessosUsuarios {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PERFILACESSOUSUARIO")
	private Long id;

	@NotNull
	@Column(name = "DESCRICAO")
	private String descricao;
}
