package br.com.conductor.templateAutomation.domain.response;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.Email;

import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import br.com.conductor.templateAutomation.domain.custom.PerfisAcessosUsuarios;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonInclude(value = Include.NON_NULL)
public class UsuarioResponse {

	private Long id;

	private Long idPermissao;

	private Long idNivelPermissaoAcesso;

	@CPF
	private String cpf;

	@Email
	private String email;

	private String nome;

	private String login;

	private Integer status;

	private Boolean bloquearAcesso;

	private Integer tentativasIncorretas;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private LocalDateTime dataModificacao;

	private Integer idPlataforma;

	private List<PerfisAcessosUsuarios> idsPerfis;
}
