
package br.com.conductor.templateAutomation.domain.custom;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = { "id", "dataStatus", "dataRegistro", "status" })
@Table(name = "UNIDADESENTREGA")
public class UnidadeEntrega {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_UNIDADEENTREGA")
	private Long id;

	@Column(name = "ID_GRUPOEMPRESA")
	private Long idGrupoEmpresa;

	@Column(name = "CODIGOUNIDADEENTREGA")
	private String codigoUnidadeEntrega;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	@Column(name = "DATASTATUS")
	private LocalDateTime dataStatus;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	@Column(name = "DATAREGISTRO")
	private LocalDateTime dataRegistro;

	@Column(name = "STATUS")
	private Integer status;

	@Column(name = "LOGRADOURO")
	private String logradouro;

	@Column(name = "ENDERECO")
	private String endereco;

	@Column(name = "ENDERECONUMERO")
	private String enderecoNumero;

	@Column(name = "ENDERECOCOMPLEMENTO")
	private String enderecoComplemento;

	@Column(name = "BAIRRO")
	private String bairro;

	@Column(name = "CIDADE")
	private String cidade;

	@Column(name = "UF")
	private String uf;

	@Column(name = "CEP")
	private String cep;

	@Column(name = "NOMERESPONSAVEL1")
	private String nomeResponsavel1;

	@Column(name = "TIPODOCUMENTORESPONSAVEL1")
	private String tipoDocumentoResponsavel1;

	@Column(name = "DOCUMENTORESPONSAVEL1")
	private String documentoResponsavel1;

	@Column(name = "TELEFONERESPONSAVEL1")
	private String telefoneResponsavel1;

	@Column(name = "NOMERESPONSAVEL2")
	private String nomeResponsavel2;

	@Column(name = "TIPODOCUMENTORESPONSAVEL2")
	private String tipoDocumentoResponsavel2;

	@Column(name = "DOCUMENTORESPONSAVEL2")
	private String documentoResponsavel2;

	@Column(name = "TELEFONERESPONSAVEL2")
	private String telefoneResponsavel2;

}