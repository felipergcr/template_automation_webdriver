package br.com.conductor.templateAutomation.domain.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonInclude(value = Include.NON_NULL)
public class GrupoEmpresaResponse {

	private Long id;

	private Long idUsuario;

	private Long idPermissao;

	private Long idEmpresa;

	private Long idPessoa;

	private Long idSubGrupo;
	
	List<UsuarioPermissaoResponse> usuarios;
}
