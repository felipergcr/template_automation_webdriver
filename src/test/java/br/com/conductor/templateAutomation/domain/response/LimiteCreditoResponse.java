
package br.com.conductor.templateAutomation.domain.response;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonInclude(value = Include.NON_NULL)
public class LimiteCreditoResponse {

     private boolean validaLimite;

     private BigDecimal valorLimiteDisponivel;

     private BigInteger porcentagemUtilizado;

     private BigDecimal valorDisponivel;

     private BigDecimal valorUtilizado;
}