
package br.com.conductor.templateAutomation.domain.response;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonInclude(value = Include.NON_NULL)
public class HierarquiaOrganizacionalGrupoResponse {

	private Long idGrupoEmpresa;

	private String nome;

	private List<HierarquiaOrganizacionalSubGrupoResponse> subGrupos;
}
