package br.com.conductor.templateAutomation.domain.custom;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CARGACONTROLEFINANCEIRO")
public class CargaControleFinanceiro {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id_CargaControleFinanceiro")
	private Long id;

	@NotNull
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	@Column(name = "DATAEMISSAORPS")
	private LocalDateTime dataEmissao;

	@NotNull
	@Column(name = "ID_EMPRESA")
	private Long idEmpresa;

	@NotNull
	@Column(name = "ID_BOLETO")
	private Long idBoleto;

	@NotNull
	@Column(name = "NUMERONF")
	private String numeroNF;

	@NotNull
	@Column(name = "CODIGOAUTENTICIDADENF")
	private String codigoAutenticidadeNF;

	@NotNull
	@Column(name = "STATUS")
	private Long status;

}
