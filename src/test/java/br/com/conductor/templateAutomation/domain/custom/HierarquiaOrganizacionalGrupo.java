package br.com.conductor.templateAutomation.domain.custom;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonInclude(value = Include.NON_NULL)
public class HierarquiaOrganizacionalGrupo {

	@Id
	@Column(name = "ID")
	@JsonIgnore
	private BigInteger id;

	@Column(name = "ID_GRUPOEMPRESA")
	private Long idGrupoEmpresa;

	@Column(name = "NOME_GRUPOEMPRESA")
	private String nomeGrupoEmpresa;

	@Column(name = "ID_SUBGRUPOEMPRESA")
	private Long idSubGrupoEmpresa;

	@Column(name = "NOME_SUBGRUPOEMPRESA")
	private String nomeSubGrupoEmpresa;

	@Column(name = "ID_EMPRESA")
	private Long idEmpresa;

	@Column(name = "NOME_EMPRESA")
	private String nomeEmpresa;

	@Column(name = "CNPJ")
	private String cnpj;

}
