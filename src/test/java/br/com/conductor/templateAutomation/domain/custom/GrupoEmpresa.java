
package br.com.conductor.templateAutomation.domain.custom;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "GRUPOSEMPRESAS")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GrupoEmpresa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_GRUPOEMPRESA")
	private Long id;

	@Column(name = "NOME")
	private String nomeGrupo;

	@Column(name = "ID_EMPRESAPRINCIPAL")
	private Long idEmpresaPrincipal;

	@Column(name = "CODIGOEXTERNO")
	private String codigoExterno;

	@Column(name = "DATACADASTRO")
	private LocalDateTime dataCadastro;

	@Column(name = "ID_GRUPOEMPRESAPAI")
	private Long idGrupoEmpresaPai;

}
