package br.com.conductor.templateAutomation.domain.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.conductor.templateAutomation.domain.custom.GrupoEmpresa;
import br.com.conductor.templateAutomation.enums.StatusPermissaoEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PermissoesRhResponse{

     private Long id;
     
     private Long idUsuario;
     
     private String nomeGrupoEmpresa;
     
     private StatusPermissaoEnum statusDescricao;
     
     private Long idUsuarioRegistro;
     
     private GrupoEmpresa grupoEmpresa;
}

